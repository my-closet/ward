
#ifndef wardrobe_Defines_h
#define wardrobe_Defines_h

typedef NS_ENUM(NSInteger, kSocialType) {
    kSocialTypeFacebook,
    kSocialTypeTwitter,
    kSocialTypeVk,
};
typedef NS_ENUM(NSInteger,kShareOption){
    kShareOptionNotAllowed, // USER did not connect social network
    kShareOptionDisabled, // USER did connect social network, but disabled sharing
    kShareOptionEnabled, // USER did conenct social network and enabled sharing
};

#define WEBSITE_URL @"http://mycloset.ru"
#define APPSTORE_LINK @"https://itunes.apple.com/ru/app/moj-garderob/id964554879?mt=8"

#define FACEBOOK_APP_ID @"1391937637769796"
#define VK_APP_ID @"5123763" // 4697334 - garderob

#define TWITTER_APP_CONSUMER_KEY @"h57UH3iGxUr2xRiq5MXo3oWwC"
#define TWITTER_APP_CONSUMER_SECRET @"UaErfWdKg6np921h0P1y8ZuuCOA3Tg2HrWnCIEjaL0C0uKV0OJ" // h57UH3iGxUr2xRiq5MXo3oWwC - is for garderob twitter account

// SOCIAL ACCOUNTS USER DATA
#define kFacebookLastUserData @"kFacebookLastUserData"
#define kTwitterLastUserData @"kTwitterLastUserData"
#define kVKLastUserData @"kVKLastUserData"
#define kGoogleLastUserData @"kGoogleLastUserData"
#define kOKLastUserData @"kOKLastUserData"
#define kLastFMLastUserData @"kLastFMLastUserData"

// SOCIAL ACCOUNTS SHARE OPTIONS
#define kFacebookShareOptionKey @"kFacebookShareOptionKey"
#define kTwitterShareOptionKey @"kTwitterShareOptionKey"
#define kVKShareOptionKey @"kVKShareOptionKey"
#define kGoogleShareOptionKey @"kGoogleShareOptionKey"
#define kOKShareOptionKey @"kOKShareOptionKey"
#define kLastFMShareOptionKey @"kLastFMShareOptionKey"




#define kShareString @"Cоздано в приложении Мой Гардероб: mycloset.ru"






#endif
