
#import "CameraHelper.h"
#import "PECropViewController.h"
#import "ItemEditController.h"
#import "MainViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
@interface CameraHelper()<UIImagePickerControllerDelegate, UINavigationControllerDelegate, PECropViewControllerDelegate>
@property (nonatomic, copy) ObjectCallback photoBlock;
@property (nonatomic, strong) Cat *targetCategory;

@end
@implementation CameraHelper
+ (CameraHelper*)sharedHelper {
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (void)launchPhotoCaptureForCategory:(Cat*)category isLibrary:(BOOL)library
{
    UIImagePickerController *c = [[UIImagePickerController alloc] init];
    UIImagePickerControllerSourceType type;
    if (library){
       type = UIImagePickerControllerSourceTypePhotoLibrary;
    } else {
        type = UIImagePickerControllerSourceTypeCamera;
    }
    c.delegate = self;
    
    if (![UIImagePickerController isSourceTypeAvailable:type]){
        [[[UIAlertView alloc] initWithTitle:@"unavailable" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        return;
    }
    
    c.sourceType = type;
    
    UINavigationController *root = (UINavigationController*)ApplicationDelegate.window.rootViewController;
    
    [root presentViewController:c animated:YES completion:nil];
    
    [AppHelper setupNavigationController:c];
    
    self.targetCategory = category;
    
    
    MainViewController *vController = [[root viewControllers] firstObject];
    
    if ([vController isKindOfClass:[MainViewController class]] && !category)
    {
        self.photoBlock = [vController itemAddToCanvasBlock];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    __block UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    if (!image){
        ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
        [library assetForURL:info[UIImagePickerControllerReferenceURL] resultBlock:^(ALAsset *asset) {
            image = [UIImage imageWithCGImage:[[asset defaultRepresentation] fullResolutionImage]];
            if (image)
                [self continueWithImage:image forPicker:picker];
        } failureBlock:^(NSError *error) {
            NSLog(@"error : %@", error);
        }];
    } else {
        [self continueWithImage:image forPicker:picker];
    }
}

- (void)continueWithImage:(UIImage*)image forPicker:(UIImagePickerController*)picker{
    PECropViewController *controller = [[PECropViewController alloc] init];
    controller.delegate = self;
    controller.image = image;
    controller.title = @"Выберите область";
    controller.toolbarHidden = YES;
    dispatch_after_short(0.2, ^{
        [picker pushViewController:controller animated:YES];
        [picker setNavigationBarHidden:NO animated:YES];
    });
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    self.photoBlock = nil;
}


#pragma mark - Crop delegate
- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage
{
    
    if (self.targetCategory == nil){
        UIStoryboard *s = [UIStoryboard storyboardWithName:@"main" bundle:nil];
        
        ItemEditController *c = [s instantiateViewControllerWithIdentifier:NSStringFromClass([ItemEditController class])];
        c.image = croppedImage;
        c.savedBlock = self.photoBlock;
        c.targetCategory = self.targetCategory;
        
        [controller.navigationController pushViewController:c animated:YES];
        
    } else { // just save item, not itemedit controller
        
        Item *item = [Item MR_createEntity];
        item.uidValue = [Item freeUIDValue];
        item.name = [Item freeItemNameForCategory:self.targetCategory.name];
        NSString *imageURL = [AppHelper saveImage:croppedImage];
        item.image_url = imageURL;
        item.category = self.targetCategory;
        
        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
        BLOCK_SAFE_RUN(self.photoBlock, item);
        [controller dismissViewControllerAnimated:YES completion:nil];

    }
}
- (void)cropViewControllerDidCancel:(PECropViewController *)controller
{
    [controller.navigationController dismissViewControllerAnimated:YES completion:nil];
    self.photoBlock = nil;
}


@end
