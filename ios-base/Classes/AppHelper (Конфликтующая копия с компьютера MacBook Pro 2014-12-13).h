
#import <Foundation/Foundation.h>

@interface AppHelper : NSObject
+ (AppHelper*)sharedHelper;
+ (UIColor*) defaultRedColor;
- (UILabel*)titleViewWithString:(NSString*)string;
@end
