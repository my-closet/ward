
#import "AppHelper.h"

@implementation AppHelper
+ (AppHelper*)sharedHelper {
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}
- (id) init{
    self = [super init];
    if (self){
    }
    return self;
}
+ (UIColor*) defaultRedColor{
    return RGB (236, 46, 46);
}
+ (UIFont *)fontBoldWithSize:(CGFloat)size{
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:size];
    if (font)
        return font;
    else
        return [UIFont boldSystemFontOfSize:size];
}
+ (UIFont *)fontRegularWithSize:(CGFloat)size{
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:size];
    if (font)
        return font;
    else
        return [UIFont systemFontOfSize:size];
}

- (UILabel*)titleViewWithString:(NSString*)string
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    label.font = [AppHelper fontBoldWithSize:20.0f];
    label.text = string;
    label.textColor = [UIColor whiteColor];
    [label resizeHorizontallyToFit];
    if (label.width > 200)
        label.width = 200;
    label.backgroundColor = [UIColor clearColor];
    label.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin;
    [label setTranslatesAutoresizingMaskIntoConstraints:YES];
    return label;
}

@end
