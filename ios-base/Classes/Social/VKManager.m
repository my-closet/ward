
#import "VKManager.h"
#import "VKSdk.h"

@interface VKManager()<VKSdkDelegate>
@property (nonatomic, copy) DictionaryCallback block;
@property (nonatomic, copy) Callback failure;
@end
@implementation VKManager
+ (instancetype)sharedManager {
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}
+ (kSocialType)socialType
{
    return kSocialTypeVk;
}
- (instancetype) init{
    self = [super init];
    [VKSdk initializeWithDelegate:self andAppId:VK_APP_ID];
    [VKSdk wakeUpSession];
    return self;
}
-(void)setShareOption:(kShareOption)shareOption
{
    if (shareOption == kShareOptionEnabled)
        [UserDefaults setBool:YES forKey:kVKShareOptionKey];
    else if (shareOption == kShareOptionDisabled)
        [UserDefaults setBool:NO forKey:kVKShareOptionKey];
    [UserDefaults synchronize];
}
- (kShareOption)shareOption{
    if ([UserDefaults objectForKey:kVKShareOptionKey]){
        if ([UserDefaults boolForKey:kVKShareOptionKey])
            return kShareOptionEnabled;
        else
            return kShareOptionDisabled;
    } else
        return kShareOptionNotAllowed;
}
-(NSString *)username{
    NSDictionary *options = [UserDefaults objectForKey:kVKLastUserData];
    if (options){
        NSString *username = [options objectForKeyOrNil:@"username"];
        if (username == nil || [self usernameIsIdFormat:username])
            return [options objectForKeyOrNil:@"full_name"];
        else
            return username;

    }
    return nil;
}
- (BOOL) usernameIsIdFormat:(NSString*)username{
    
    if (username.length > 2){
        NSString *first = [[username substringToIndex:2] lowercaseString];
        NSString *last = [username substringFromIndex:2];
        
        return [first isEqualToString:@"id"] && [last longLongValue] > 0;
    }
    
    return NO;
}
- (BOOL)isConnected{
    return [VKSdk isLoggedIn];
}
-(void)disconnect{
    [VKSdk forceLogout];
    [UserDefaults removeObjectForKey:kVKLastUserData];
    [UserDefaults removeObjectForKey:kVKShareOptionKey];
    [UserDefaults synchronize];

}
- (void)connectWithCompletionHandler:(DictionaryCallback)aBlock failure:(Callback)failure{
    self.block = aBlock;
    self.failure = failure;
    if ([VKSdk isLoggedIn]){
        [self fetchCurrentUser];
    } else {
        [VKSdk authorize:@[VK_PER_FRIENDS, VK_PER_WALL, VK_PER_PHOTOS] revokeAccess:YES];
    }
}
- (void) fetchCurrentUser{
    
    NSDictionary *params = @{ @"fields": VK_USER_FIELDS };
    VKRequest *userInfoReq = [[VKApi users] get:params];
    
    [userInfoReq executeWithResultBlock:^(VKResponse * response) {
        
        NSDictionary *userDict = [[response json] firstObject];
        
        NSString *uid = [userDict objectForKeyOrEmptyString:@"id"];
        NSString *firstName = [userDict objectForKeyOrEmptyString:@"first_name"];
        NSString *lastName = [userDict objectForKeyOrEmptyString:@"last_name"];
        NSString *fullName = [firstName stringByAppendingString:[NSString stringWithFormat:@" %@",lastName]];
        NSString *username = [userDict objectForKeyOrNil:@"screen_name"];
        NSString *photoMax = [userDict objectForKeyOrNil:@"photo_max"];
        
        NSMutableDictionary *options = [NSMutableDictionary dictionary];
        [options setObjectIfNotNil:photoMax forKey:@"avatar"];
        [options setObjectIfNotNil:fullName forKey:@"full_name"];
        [options setObjectIfNotNil:firstName forKey:@"first_name"];
        [options setObjectIfNotNil:lastName forKey:@"last_name"];
        [options setObjectIfNotNil:username forKey:@"username"];
        [options setObjectIfNotNil:uid forKey:@"uid"];
        
        if ([UserDefaults objectForKey:kVKShareOptionKey] == nil){
            [UserDefaults setBool:YES forKey:kVKShareOptionKey];
        }
        
        [UserDefaults setObject:options forKey:kVKLastUserData];
        [UserDefaults synchronize];
        
        if (uid)
            BLOCK_SAFE_RUN(self.block, options);
        else
            BLOCK_SAFE_RUN(self.failure);
        
        self.block = nil;
        self.failure = nil;
        
    } errorBlock:^(NSError * error) {
        BLOCK_SAFE_RUN(self.failure);
        
        self.block = nil;
        self.failure = nil;
    }];
    
}

- (void)getAppFriendsWithSuccess:(ArrayCallback)uids failure:(Callback)failure{
    NSDictionary *params = @{ @"fields": VK_USER_FIELDS };
    VKRequest *userInfoReq = [VKRequest requestWithMethod:@"friends.getAppUsers" andParameters:params andHttpMethod:@"GET"];
    [userInfoReq executeWithResultBlock:^(VKResponse *response) {
        NSArray *array = response.json;
        BLOCK_SAFE_RUN(uids,array);
    } errorBlock:^(NSError *error) {
        failure();
    }];
}


- (void)shareImage:(UIImage*)image success:(Callback)success failure:(Callback)failure{
    
   // NSString *publishText = kShareString;
    
    NSString *publishText = @"Оцените мой новый образ:";
    
    if (image == nil) {
        BLOCK_SAFE_RUN(failure);
        return;
    }
    
    NSDictionary *options = [UserDefaults objectForKey:kVKLastUserData];
    NSInteger uid = [[options objectForKeyOrNil:@"uid"] integerValue];
    
    VKImageParameters *params = [[VKImageParameters alloc] init];
    params.imageType = VKImageTypeJpg;
    params.jpegQuality = 0.8f;
    
    VKRequest *upload = [VKApi uploadWallPhotoRequest:image parameters:params userId:uid groupId:0];
    [upload executeWithResultBlock:^(VKResponse *response) {
        
        NSString *attachment = [[response.json firstObject] objectForKey:@"id"];
        attachment = [NSString stringWithFormat:@"photo%ld_%@",(long)uid,attachment];
        
        attachment = [attachment stringByAppendingString:[NSString stringWithFormat:@",%@",APPSTORE_LINK]];
        
        VKRequest *request = [[VKApi wall] post:@{@"message"        : publishText,
                                                  @"attachments"    : attachment
                                                  }];
        [request executeWithResultBlock:^(VKResponse *response) {
            NSLog(@"successfully posted to VK: %@",publishText);
            BLOCK_SAFE_RUN(success);
        } errorBlock:^(NSError *error) {
            NSLog(@"failed to post to VK: %@",error);
            BLOCK_SAFE_RUN(failure);
        }];
        
    } errorBlock:^(NSError *error) {
        NSLog(@"failed to post to VK:%@",error);
        BLOCK_SAFE_RUN(failure);
    }];
    
}



- (void)vkSdkNeedCaptchaEnter:(VKError *)captchaError
{
    NSLog(@"vkSdkNeedCaptchaEnter");
    VKCaptchaViewController * vc = [VKCaptchaViewController captchaControllerWithError:captchaError];
    [vc presentIn:ApplicationDelegate.window.rootViewController];
}
- (void)vkSdkTokenHasExpired:(VKAccessToken *)expiredToken
{
    NSLog(@"vkSdkTokenHasExpired");
}
- (void)vkSdkUserDeniedAccess:(VKError *)authorizationError
{
    NSLog(@"vkSdkUserDeniedAccess");
}
- (void)vkSdkShouldPresentViewController:(UIViewController *)controller
{
    NSLog(@"vkSdkShouldPresentViewController");
    UIViewController *c = ApplicationDelegate.window.rootViewController;
    if (c.presentedViewController != nil)
        [c.presentedViewController presentViewController:controller animated:YES completion:nil];
    else
        [c presentViewController:controller animated:YES completion:nil];
    [SVProgressHUD dismiss];
}
- (void)vkSdkReceivedNewToken:(VKAccessToken *)newToken
{
    NSLog(@"vkSdkReceivedNewToken");
    [self fetchCurrentUser];
}
- (void)vkSdkAcceptedUserToken:(VKAccessToken *)token
{
    NSLog(@"vkSdkAcceptedUserToken");
}
- (void)vkSdkRenewedToken:(VKAccessToken *)newToken
{
    NSLog(@"vkSdkReceivedNewToken");
    [self fetchCurrentUser];
}

@end
