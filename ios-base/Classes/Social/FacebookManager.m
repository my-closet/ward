
#import "FacebookManager.h"
#import <Accounts/Accounts.h>
#import <Social/Social.h>

#define facebook_read_permissions @[@"public_profile"]

#define facebook_user_params @{@"fields":@"gender,name,first_name,last_name"}

#import <objc/runtime.h>
@interface ACAccount(Ext)
- (void)logAllProperties;
@end
@implementation ACAccount(Ext)

- (void)logAllProperties {
    unsigned int count;
    Ivar *ivars = class_copyIvarList([self class], &count);
    for (unsigned int i = 0; i < count; i++) {
        Ivar ivar = ivars[i];
        
        const char *name = ivar_getName(ivar);
        const char *type = ivar_getTypeEncoding(ivar);
        ptrdiff_t offset = ivar_getOffset(ivar);
        
        if (strncmp(type, "i", 1) == 0) {
            int intValue = *(int*)((uintptr_t)self + offset);
            NSLog(@"%s = %i", name, intValue);
        } else if (strncmp(type, "f", 1) == 0) {
            float floatValue = *(float*)((uintptr_t)self + offset);
            NSLog(@"%s = %f", name, floatValue);
        } else if (strncmp(type, "@", 1) == 0) {
            id value = object_getIvar(self, ivar);
            NSLog(@"%s = %@", name, value);
        }
        // And the rest for other type encodings
    }
    free(ivars);
}

@end

@interface FacebookManager()
@property (nonatomic, copy) DictionaryCallback block;
@property (nonatomic, copy) Callback failure;
@end

@implementation FacebookManager
+ (instancetype)sharedManager {
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}
+ (kSocialType)socialType
{
    return kSocialTypeFacebook;
}

- (instancetype) init{
    self = [super init];
    // Whenever a person opens the app, check for a cached session
    [self signInWithFacebookSDKSilently:YES];
    return self;
}
-(void)setShareOption:(kShareOption)shareOption{
    if (shareOption == kShareOptionEnabled)
        [UserDefaults setBool:YES forKey:kFacebookShareOptionKey];
    else if (shareOption == kShareOptionDisabled)
        [UserDefaults setBool:NO forKey:kFacebookShareOptionKey];
    [UserDefaults synchronize];
}
- (kShareOption)shareOption{
    if ([UserDefaults objectForKey:kFacebookShareOptionKey]){
        if ([UserDefaults boolForKey:kFacebookShareOptionKey])
            return kShareOptionEnabled;
        else
            return kShareOptionDisabled;
    } else
        return kShareOptionNotAllowed;
}

-(NSString *)username{
    NSDictionary *options = [UserDefaults objectForKey:kFacebookLastUserData];
    if (options){
        NSString *username = [options objectForKeyOrNil:@"username"];
        if (username == nil)
            return [options objectForKeyOrNil:@"full_name"];
        else
            return username;
        
    }
    return @"";
}
- (NSString*)userID{
    NSDictionary *options = [UserDefaults objectForKey:kFacebookLastUserData];
    if (options){
        return [options objectForKeyOrNil:@"uid"];
    }
    return nil;
}
- (BOOL)isConnected{
//    
//    if (FBSession.activeSession.state == FBSessionStateOpen || FBSession.activeSession.state == FBSessionStateOpenTokenExtended){
//        return YES;
//    }
    
    ACAccountStore *account = [[ACAccountStore alloc] init];
    ACAccountType *accountType = [account accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
    NSArray *arrayOfAccounts = [account
                                accountsWithAccountType:accountType];
    if (arrayOfAccounts.count > 0 && [UserDefaults objectForKey:kFacebookLastUserData])
        return YES;
    else
        return NO;
}

-(void)disconnect{
    [UserDefaults removeObjectForKey:kFacebookLastUserData];
    [UserDefaults removeObjectForKey:kFacebookShareOptionKey];
    [UserDefaults synchronize];
    
//    if (FBSession.activeSession.state == FBSessionStateOpen
//        || FBSession.activeSession.state == FBSessionStateOpenTokenExtended) {
//        
//        // Close the session and remove the access token from the cache
//        // The session state handler (in the app delegate) will be called automatically
//        [FBSession.activeSession closeAndClearTokenInformation];
//        [FBSession setActiveSession:nil];
//        // If the session state is not any of the two "open" states when the button is clicked
//    }
}
- (void)connectWithCompletionHandler:(DictionaryCallback)aBlock failure:(Callback)failure
{
    self.block = aBlock;
    self.failure = failure;
    
    [self signInWithFacebookSDKSilently:NO];
    
}
- (void)getUserBirthdayWithCallback:(DictionaryCallback)block
{
    ACAccountStore *account = [[ACAccountStore alloc] init];
    ACAccountType *accountType = [account accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
    ACAccount *acc = [[account accountsWithAccountType:accountType] lastObject];
    
    
    NSURL *requestURL = [NSURL URLWithString:@"https://graph.facebook.com/me"];
    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeFacebook
                                            requestMethod:SLRequestMethodGET
                                                      URL:requestURL
                                               parameters:nil];
    request.account = acc;
    [request performRequestWithHandler:^(NSData *data,
                                         NSHTTPURLResponse *response,
                                         NSError *error) {
        
        if(!error){
            NSDictionary *list =[NSJSONSerialization JSONObjectWithData:data
                                                                options:kNilOptions error:&error];
            NSLog(@"Dictionary contains: %@", list );
            block(list);
        }
        else{
            //handle error gracefully
            block(nil);
        }
        
    }];
}


#pragma mark - Parse USER DIct

- (void)getUserInfoWithAcAccountOrFacebookSDK:(BOOL)useFacebookSDK
{
    
    DictionaryCallback block = ^(NSDictionary *FBuser){
        
        NSString *userName = [FBuser objectForKey:@"name"];
        NSString *uid = [FBuser objectForKey:@"id"];
        
        NSString *birthday = nil;
        NSDictionary *ageRangeDict = [FBuser objectForKeyOrNil:@"age_range"];
        
        if ([ageRangeDict objectForKeyOrNil:@"min"])
        {
            NSInteger years = [[ageRangeDict objectForKeyOrNil:@"min"] integerValue];
            if (years >= 0)
            {
                birthday = @"06/11/1990";
            }
        }
        NSString *avatar = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?width=%i&height=%i",
                            uid, (int)500, (int)500];
        
        NSString *email = [FBuser objectForKeyOrNil:@"email"];
        NSString *firstName = [FBuser objectForKeyOrNil:@"first_name"];
        NSString *lastName = [FBuser objectForKeyOrNil:@"last_name"];
        
        NSString *fullName = firstName;
        
        
        if (lastName){
            fullName = [fullName stringByAppendingString:@" "];
            fullName = [fullName stringByAppendingString:lastName];
        }
        
        NSMutableDictionary *options = [NSMutableDictionary dictionary];
        [options setObjectIfNotNil:fullName forKey:@"full_name"];
        [options setObjectIfNotNil:firstName forKey:@"first_name"];
        [options setObjectIfNotNil:lastName forKey:@"last_name"];
        [options setObjectIfNotNil:userName forKey:@"username"];
        [options setObjectIfNotNil:email forKey:@"email"];
        [options setObjectIfNotNil:uid forKey:@"uid"];
        [options setObjectIfNotNil:avatar forKey:@"avatar"];
        [options setObjectIfNotNil:birthday forKey:@"birthday"];
        
        if ([UserDefaults objectForKey:kFacebookShareOptionKey] == nil){
            [UserDefaults setBool:YES forKey:kFacebookShareOptionKey];
        }
        
        [UserDefaults setObject:options forKey:kFacebookLastUserData];
        [UserDefaults synchronize];
        
        if (uid)
            BLOCK_SAFE_RUN(self.block, options);
        else
            BLOCK_SAFE_RUN(self.failure);
        
        self.block = nil;
        self.failure = nil;
        
        // notify friends that current user registered in Heychat
    };
    
    
    if (useFacebookSDK)
    {
//        FBRequest *request =[FBRequest requestWithGraphPath:@"me" parameters:facebook_user_params HTTPMethod:@"GET"];
//        
//        [request
//         startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
//             NSDictionary<FBGraphUser> *FBuser = (NSDictionary<FBGraphUser> *) result;
//             if (error)
//             {
//                 BLOCK_SAFE_RUN(self.failure);
//             }
//             else
//             {
//                 block(FBuser);
//             }
//         }];
    }
    else
    {
        ACAccountStore *account = [[ACAccountStore alloc] init];
        ACAccountType *accountType = [account accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
        ACAccount *acc = [[account accountsWithAccountType:accountType] lastObject];
        
        NSURL *requestURL = [NSURL URLWithString:@"https://graph.facebook.com/me"];
        
        SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeFacebook
                                                requestMethod:SLRequestMethodGET
                                                          URL:requestURL
                                                   parameters:facebook_user_params];
        request.account = acc;
        
        [request performRequestWithHandler:^(NSData *data, NSHTTPURLResponse *response, NSError *error) {
            
            if (error)
            {
                BLOCK_SAFE_RUN(self.failure);
            }
            else
            {
                NSDictionary *list =[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                if ([list objectForKeyOrNil:@"error"] == nil)
                {
                    block(list);
                }
                else
                {
                    [self signInWithFacebookSDKSilently:NO];
                }
            }
        }];
    }
}

#pragma mark - FACEBOOK SDK

- (void)signInWithFacebookSDKSilently:(BOOL)silently{
    
    // If the session state is any of the two "open" states when the button is clicked
//    if (FBSession.activeSession.state == FBSessionStateOpen
//        || FBSession.activeSession.state == FBSessionStateOpenTokenExtended) {
//        
//        if (!silently){
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [self getUserInfoWithAcAccountOrFacebookSDK:YES];
//            });
//        }
//        else
//        {
//            // session is open, do nothing
//        }
//        
//        // If the session state is not any of the two "open" states when the button is clicked
//    } else {
//        // Open a session showing the user the login UI
//        // You must ALWAYS ask for public_profile permissions when opening a session
//        [FBSession openActiveSessionWithReadPermissions:facebook_read_permissions
//                                           allowLoginUI:!silently
//                                      completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
//                                          
//                                          dispatch_async(dispatch_get_main_queue(), ^{
//                                              [self sessionStateChanged:session
//                                                                  state:session.state
//                                                                  error:error
//                                                                 silent:silently];
//                                          });
//                                          
//                                          //this is called even from the reauthorizeWithPublishPermissions
//                                          if (state == FBSessionStateOpen && !error && !silently)
//                                          {
//                                              // do nothing
//                                          }
//                                          else if (state == FBSessionStateClosedLoginFailed)
//                                          {
//                                              [FBSession.activeSession closeAndClearTokenInformation];
//                                          }
//                                      }];
//    }
}

- (BOOL)hasPublishPermission
{
    BOOL hasPublishPermissions = NO;
//    for (NSString *string in [[FBSession activeSession] permissions])
//    {
//        if ([string isEqualToString:@"publish_actions"])
//            hasPublishPermissions = YES;
//    }
    return hasPublishPermissions;
}


// This method will handle ALL the session state changes in the app
//- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState) state error:(NSError *)error silent:(BOOL)silent
//{
//    
//    // If the session was opened successfully
//    if (!error && (state == FBSessionStateOpen || state == FBSessionStateOpenTokenExtended)){
//        NSLog(@"Session opened");
//        // Show the user the logged-in UI
//        if (!silent){
//            [self getUserInfoWithAcAccountOrFacebookSDK:YES];
//        }
//    }
//    if (state == FBSessionStateClosed || state == FBSessionStateClosedLoginFailed){
//        // If the session is closed
//        NSLog(@"Session closed");
//        // Show the user the logged-out UI
//        [self disconnect];
//    }
//    
//    // Handle errors
//    if (error){
//        NSLog(@"Error");
//        NSString *alertText;
//        NSString *alertTitle;
//        // If the error requires people using an app to make an action outside of the app in order to recover
//        if ([FBErrorUtility shouldNotifyUserForError:error] == YES){
//            alertTitle = @"Something went wrong";
//            alertText = [FBErrorUtility userMessageForError:error];
//        } else {
//            
//            // If the user cancelled login, do nothing
//            if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled) {
//                NSLog(@"User cancelled login");
//                
//                // Handle session closures that happen outside of the app
//            } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession){
//                alertTitle = @"Session Error";
//                alertText = @"Your current session is no longer valid. Please log in again.";
//                
//                
//                // Here we will handle all other errors with a generic error message.
//                // We recommend you check our Handling Errors guide for more information
//                // https://developers.facebook.com/docs/ios/errors/
//            } else {
//                //Get more error information from the error
//                NSDictionary *errorInformation = [[[error.userInfo objectForKey:@"com.facebook.sdk:ParsedJSONResponseKey"] objectForKey:@"body"] objectForKey:@"error"];
//                
//                // Show the user an error message
//                alertTitle = @"Something went wrong";
//                alertText = [NSString stringWithFormat:@"Please retry. \n\n If the problem persists contact us and mention this error code: %@", [errorInformation objectForKey:@"message"]];
//                
//            }
//        }
//        [self disconnect];
//    }
//}

- (void)getAppFriendsWithSuccess:(ArrayCallback)uids failure:(Callback)failure{
    
    ACAccountStore *account = [[ACAccountStore alloc] init];
    ACAccountType *accountType = [account accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
    ACAccount *acc = [[account accountsWithAccountType:accountType] lastObject];
    
    if (acc)
    {
        NSURL *requestURL = [NSURL URLWithString:@"https://graph.facebook.com/me/friends"];
        SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeFacebook
                                                requestMethod:SLRequestMethodGET
                                                          URL:requestURL
                                                   parameters:nil];
        request.account = acc;
        [request performRequestWithHandler:^(NSData *data,
                                             NSHTTPURLResponse *response,
                                             NSError *error) {
            
            NSDictionary *list =[NSJSONSerialization JSONObjectWithData:data
                                                                options:kNilOptions error:&error];
            if (error == nil){
                NSArray* friends = [list objectForKey:@"data"];
                NSMutableArray *uidsArray = [NSMutableArray array];
                for (NSDictionary *dict in friends){
                    [uidsArray addObject:dict[@"id"]];
                }
                BLOCK_SAFE_RUN(uids, uidsArray);
            } else {
                BLOCK_SAFE_RUN(failure);
            }
        }];
    }
    else
    {
//        [FBRequestConnection startWithGraphPath:@"me/friends"
//                                     parameters:nil
//                                     HTTPMethod:@"GET"
//                              completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
//                                  if (error == nil){
//                                      NSArray* friends = [result objectForKey:@"data"];
//                                      NSMutableArray *uidsArray = [NSMutableArray array];
//                                      for (NSDictionary *dict in friends){
//                                          [uidsArray addObject:dict[@"id"]];
//                                      }
//                                      BLOCK_SAFE_RUN(uids, uidsArray);
//                                  } else {
//                                      BLOCK_SAFE_RUN(failure);
//                                  }
//                              }];
    }
}
- (void)notifyFriendsAboutMe
{
    
    return;
    
    [self getAppFriendsWithSuccess:^(NSArray *resultArray) {
        if (resultArray.count > 0)
        {
            
        }
    } failure:^{
        
    }];
}
- (void)shareImage:(UIImage *)image success:(Callback)success failure:(Callback)failure
{
    if ([image isKindOfClass:[UIImage class]] == NO){
        BLOCK_SAFE_RUN(failure);
        return;
    }
    
//    // Check if the Facebook app is installed and we can present the share dialog
//    FBLinkShareParams *params = [[FBLinkShareParams alloc] init];
//    params.link = [NSURL URLWithString:@"https://developers.facebook.com/docs/ios/share/"];
//    
//    // If the Facebook app is installed and we can present the share dialog
//    if ([FBDialogs canPresentShareDialogWithPhotos]) {
//        // Present the share dialog
//        // Present share dialog
//        
//        FBPhotoParams *params = [FBPhotoParams new];
//        params.photos = @[image];
//        
//        [FBDialogs presentShareDialogWithPhotoParams:params clientState:nil handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
//            dispatch_after_short(0, ^{
//                if(error) {
//                    // An error occurred, we need to handle the error
//                    // See: https://developers.facebook.com/docs/ios/errors
//                    NSLog(@"Error publishing story: %@", error.description);
//                    BLOCK_SAFE_RUN(failure);
//                } else {
//                    // Success
//                    NSLog(@"result %@", results);
//                    BLOCK_SAFE_RUN(success);
//                }
//
//            });
//        }];
//    } else {
//        
//        [[FBRequest requestForUploadPhoto:image] startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
//            if ([result objectForKey:@"post_id"]){
//                NSLog(@"result:%@",result);
//                BLOCK_SAFE_RUN(success);
//            } else {
//                BLOCK_SAFE_RUN(failure);
//            }
//        }];
//    }
}

- (void)uploadWithWebDialogImageID:(NSString*)imageId success:(Callback)success failure:(Callback)failure{
    
    // Present the feed dialog
    // Put together the dialog parameters
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   @"Мой новый образ", @"name",
                                   @"Оцените мой новый образ, созданный в приложении 'Мой Гардероб'", @"caption",
                                   APPSTORE_LINK, @"link",
                                   imageId, @"picture",
                                   nil];
    // Show the feed dialog
    
////    [FBWebDialogs presentFeedDialogModallyWithSession:nil parameters:params handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
//        dispatch_after_short(0, ^{
//            if (error) {
//                // An error occurred, we need to handle the error
//                // See: https://developers.facebook.com/docs/ios/errors
//                NSLog(@"Error publishing story: %@", error.description);
//                BLOCK_SAFE_RUN(failure);
//            } else {
//                if (result == FBWebDialogResultDialogNotCompleted) {
//                    // User cancelled.
//                    NSLog(@"User cancelled.");
//                    BLOCK_SAFE_RUN(failure);
//                } else {
//                    // Handle the publish feed callback
//                    NSDictionary *urlParams = [self parseURLParams:[resultURL query]];
//                    
//                    if (![urlParams valueForKey:@"post_id"]) {
//                        // User cancelled.
//                        NSLog(@"User cancelled.");
//                        BLOCK_SAFE_RUN(failure);
//                        
//                    } else {
//                        // User clicked the Share button
//                        NSString *result = [NSString stringWithFormat: @"Posted story, id: %@", [urlParams valueForKey:@"post_id"]];
//                        NSLog(@"result %@", result);
//                        BLOCK_SAFE_RUN(success);
//                    }
//                }
//            }
//        });
//        
//    }];

}

// A function for parsing URL parameters returned by the Feed Dialog.
- (NSDictionary*)parseURLParams:(NSString *)query {
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [kv[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        params[kv[0]] = val;
    }
    return params;
}

@end
