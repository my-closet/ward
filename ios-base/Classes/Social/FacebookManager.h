
#import <Foundation/Foundation.h>

#define USE_ONLY_FACEBOOK_SDK 1 // not working, TODO

@interface FacebookManager : NSObject
+ (instancetype)sharedManager;
+ (kSocialType)socialType;
- (void)connectWithCompletionHandler:(DictionaryCallback)aBlock failure:(Callback)failure;

- (BOOL)isConnected;
- (void)disconnect;

-(void)downloadAvatarWithCompletionHandler:(Callback)aBlock failure:(Callback)failure progressBlock:(ProgressFloatCallback)progressBlock;

- (NSString*)username; // returns last signed in username

- (void)getAppFriendsWithSuccess:(ArrayCallback)uids failure:(Callback)failure;

- (void)shareImage:(UIImage*)image success:(Callback)success failure:(Callback)failure;

@property (nonatomic, assign) kShareOption shareOption;

@end
