
#import <Foundation/Foundation.h>

#define VK_USER_FIELDS @"photo_max,screen_name,sex"

@interface VKManager : NSObject
+ (instancetype)sharedManager;
+ (kSocialType)socialType;
- (void)connectWithCompletionHandler:(DictionaryCallback)aBlock failure:(Callback)failure;

- (BOOL)isConnected;
- (void)disconnect;

- (void)downloadAvatarWithCompletionHandler:(Callback)aBlock failure:(Callback)failure progressBlock:(ProgressFloatCallback)progressBlock;

- (NSString*)username; // returns last signed in username

- (void)getAppFriendsWithSuccess:(ArrayCallback)uids failure:(Callback)failure;

@property (nonatomic, assign) kShareOption shareOption;

- (void)shareImage:(UIImage*)image success:(Callback)success failure:(Callback)failure;
@end
