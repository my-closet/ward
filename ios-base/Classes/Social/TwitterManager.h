
#import <Foundation/Foundation.h>

@interface TwitterManager : NSObject
+ (instancetype)sharedManager;
+ (kSocialType)socialType;
- (void)connectWithCompletionHandler:(DictionaryCallback)aBlock failure:(Callback)failure;

- (BOOL)isConnected;
- (void)disconnect;
- (void)setupConnectedStatus;

- (void)downloadAvatarWithCompletionHandler:(Callback)aBlock failure:(Callback)failure progressBlock:(ProgressFloatCallback)progressBlock;

- (NSString*)username; // returns last signed in username

@property (nonatomic, assign) kShareOption shareOption;

- (void)getAppFriendsWithSuccess:(ArrayCallback)uids failure:(Callback)failure;

- (void)setOAuthToken:(NSString *)token oauthVerifier:(NSString *)verifier;

- (void)shareImage:(UIImage*)image success:(Callback)success failure:(Callback)failure;


@end
