
#import "TwitterManager.h"
#import <Accounts/Accounts.h>
#import <Social/Social.h>
#import "STTwitter.h"

#define kOauthTokenKey @"oauthToken"
#define kOauthTokenSecretKey @"oauthTokenSecret"

@interface TwitterManager() <UIActionSheetDelegate>
@property (nonatomic, copy) DictionaryCallback block;
@property (nonatomic, copy) Callback failure;
@property (nonatomic, strong) STTwitterAPI *twitterAPI;
@property (nonatomic, assign) BOOL isLoggedIn;
@end

@implementation TwitterManager

+ (instancetype)sharedManager {
    static dispatch_once_t once;
    static TwitterManager *sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
        [sharedInstance setupConnectedStatus];
    });
    return sharedInstance;
}
+ (kSocialType)socialType
{
    return kSocialTypeTwitter;
}
-(void)setShareOption:(kShareOption)shareOption{
    if (shareOption == kShareOptionEnabled)
        [UserDefaults setObject:@(YES) forKey:kTwitterShareOptionKey];
    else if (shareOption == kShareOptionDisabled)
        [UserDefaults setObject:@(NO) forKey:kTwitterShareOptionKey];
    [UserDefaults synchronize];
}
- (kShareOption)shareOption{
    if ([UserDefaults objectForKey:kTwitterShareOptionKey]){
        if ([UserDefaults boolForKey:kTwitterShareOptionKey])
            return kShareOptionEnabled;
        else
            return kShareOptionDisabled;
    } else
        return kShareOptionNotAllowed;
}

- (void)setupConnectedStatus {
    ACAccountStore *account = [[ACAccountStore alloc] init];
    ACAccountType *accountType = [account accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    NSArray *arrayOfAccounts = [account accountsWithAccountType:accountType];
    
    NSDictionary *options = [UserDefaults objectForKey:kTwitterLastUserData];
    NSString *oauthToken = [options objectForKeyOrNil:kOauthTokenKey];
    NSString *oauthTokenSecret = [options objectForKeyOrNil:kOauthTokenSecretKey];
    
    if (arrayOfAccounts.count > 0 && options) {
        self.isLoggedIn = YES;
    } else if (oauthToken && oauthTokenSecret) {
        self.twitterAPI = [STTwitterAPI twitterAPIWithOAuthConsumerKey:TWITTER_APP_CONSUMER_KEY consumerSecret:TWITTER_APP_CONSUMER_SECRET oauthToken:oauthToken oauthTokenSecret:oauthTokenSecret];
        [self.twitterAPI getAccountVerifyCredentialsWithSuccessBlock:^(NSDictionary *account) {
            [self parseAccountFromDictionary:account];
            self.isLoggedIn = YES;
        } errorBlock:^(NSError *error) {
            if ([UserDefaults objectForKey:kTwitterLastUserData])
                [self disconnect];
            self.isLoggedIn = NO;
        }];
    } else {
        if ([UserDefaults objectForKey:kTwitterLastUserData])
            [self disconnect];
        self.isLoggedIn = NO;
    }
}

- (void)setIsLoggedIn:(BOOL)isLoggedIn {
    if (_isLoggedIn != isLoggedIn) {
    }
    _isLoggedIn = isLoggedIn;
}

- (BOOL)isConnected {
    return self.isLoggedIn;
}

- (void)disconnect{
    [UserDefaults removeObjectForKey:kTwitterLastUserData];
    [UserDefaults removeObjectForKey:kTwitterShareOptionKey];
    [UserDefaults synchronize];
    self.twitterAPI = nil;
    self.isLoggedIn = NO;
}
-(NSString *)username{
    NSDictionary *options = [UserDefaults objectForKey:kTwitterLastUserData];
    if (options){
        NSString *username = [options objectForKeyOrNil:@"username"];
        if (username == nil)
            return [options objectForKeyOrNil:@"full_name"];
        else
            return username;
    }
    return @"";
}
- (void)connectWithCompletionHandler:(DictionaryCallback)aBlock failure:(Callback)failure{
    self.block = aBlock;
    self.failure = failure;
    
    ACAccountStore *account = [[ACAccountStore alloc] init];
    ACAccountType *accountType = [account accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    
    [account requestAccessToAccountsWithType:accountType
                                     options:nil completion:^(BOOL granted, NSError *error)
     {
         NSArray *arrayOfAccounts = [account accountsWithAccountType:accountType];
         if (granted == YES && [arrayOfAccounts count] > 0)
         {
             if (arrayOfAccounts.count > 1){
                 UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Choose Twitter Account",nil)
                                                                          delegate:self
                                                                 cancelButtonTitle:nil
                                                            destructiveButtonTitle:nil
                                                                 otherButtonTitles:nil];
                 for (ACAccount *account in arrayOfAccounts)
                     [actionSheet addButtonWithTitle:account.username];
                 [actionSheet addButtonWithTitle:NSLocalizedString(@"Cancel", nil)];
                 [actionSheet setCancelButtonIndex:actionSheet.numberOfButtons - 1];
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [actionSheet showInView:ApplicationDelegate.window.rootViewController.view];
                 });
             } else {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     self.twitterAPI = [STTwitterAPI twitterAPIOSWithAccount:[arrayOfAccounts firstObject]];
                     [self.twitterAPI getAccountVerifyCredentialsWithSuccessBlock:^(NSDictionary *account) {
                         [self parseAccountFromDictionary:account];
                     } errorBlock:^(NSError *error) {
                         self.isLoggedIn = NO;
                         BLOCK_SAFE_RUN(self.failure);
                     }];
                 });
             }
         } else {
             dispatch_async(dispatch_get_main_queue(), ^{
                 [self authentificateWithSTTwitter];
             });
         }
     }];
}


-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex != actionSheet.cancelButtonIndex) {
        ACAccountStore *accountStore = [[ACAccountStore alloc] init];
        ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
        NSArray *twitterAccounts = [accountStore accountsWithAccountType:accountType];
        ACAccount *account = twitterAccounts[buttonIndex];
        self.twitterAPI = [STTwitterAPI twitterAPIOSWithAccount:account];
        [self.twitterAPI getAccountVerifyCredentialsWithSuccessBlock:^(NSDictionary *account) {
            [self parseAccountFromDictionary:account];
        } errorBlock:^(NSError *error) {
            self.isLoggedIn = NO;
        }];
    }
}


- (void)getAppFriendsWithSuccess:(ArrayCallback)uids failure:(Callback)failure{
    DLog(@"Twitter friends search not supported");
    BLOCK_SAFE_RUN(failure);
}

#pragma mark - STTwitter Login

- (void)authentificateWithSTTwitter {
    
    
    self.twitterAPI = [STTwitterAPI twitterAPIWithOAuthConsumerKey:TWITTER_APP_CONSUMER_KEY consumerSecret:TWITTER_APP_CONSUMER_SECRET];

    [self.twitterAPI postTokenRequest:^(NSURL *url, NSString *oauthToken) {
        [[UIApplication sharedApplication] openURL:url];
    } authenticateInsteadOfAuthorize:NO forceLogin:@(YES) screenName:nil oauthCallback:@"garderob://twitter_access_tokens/"  errorBlock:^(NSError *error) {
        self.isLoggedIn = NO;
        NSLog(@"error:%@",error);
        BLOCK_SAFE_RUN(self.failure);
    }];
}

- (void)setOAuthToken:(NSString *)token oauthVerifier:(NSString *)verifier {
    [self.twitterAPI postAccessTokenRequestWithPIN:verifier successBlock:^(NSString *oauthToken, NSString *oauthTokenSecret, NSString *userID, NSString *screenName) {

        NSMutableDictionary *options = [[UserDefaults objectForKey:kTwitterLastUserData] mutableCopy];
        if (!options) {
            options = [NSMutableDictionary dictionary];
        }
        [options setObjectIfNotNil:oauthToken forKey:kOauthTokenKey];
        [options setObjectIfNotNil:oauthTokenSecret forKey:kOauthTokenSecretKey];
        [UserDefaults setObject:options forKey:kTwitterLastUserData];
        [UserDefaults synchronize];
        
        [self.twitterAPI getAccountVerifyCredentialsWithSuccessBlock:^(NSDictionary *account) {
            [self parseAccountFromDictionary:account];
        } errorBlock:^(NSError *error) {
            self.isLoggedIn = NO;
        }];
    } errorBlock:^(NSError *error) {
        NSLog(@"-- %@", [error localizedDescription]);
    }];
}

- (void)parseAccountFromDictionary:(NSDictionary *)account {
    self.isLoggedIn = YES;
    
    NSString *username = account[@"screen_name"];
    NSNumber *uid = account[@"id"];
    NSString *fullName = account[@"name"];
    NSString *avatarURL = [account objectForKeyOrNil:@"profile_image_url_https"];
    avatarURL = [avatarURL stringByReplacingOccurrencesOfString:@"_normal" withString:@""];
    avatarURL = [avatarURL stringByReplacingOccurrencesOfString:@"_bigger" withString:@""];
    avatarURL = [avatarURL stringByReplacingOccurrencesOfString:@"_mini" withString:@""];
    
    NSMutableDictionary *options = [[UserDefaults objectForKey:kTwitterLastUserData] mutableCopy];
    if (!options) {
        options = [NSMutableDictionary dictionary];
    }
    [options setObjectIfNotNil:fullName forKey:@"full_name"];
    [options setObjectIfNotNil:username forKey:@"username"];
    [options setObjectIfNotNil:uid forKey:@"uid"];
    [options setObjectIfNotNil:avatarURL forKey:@"avatar"];
    
    if ([UserDefaults objectForKey:kTwitterShareOptionKey] == nil){
        [UserDefaults setBool:YES forKey:kTwitterShareOptionKey];
    }
    [UserDefaults setObject:options forKey:kTwitterLastUserData];
    [UserDefaults synchronize];
    dispatch_async(dispatch_get_main_queue(), ^{
        if (uid)
            BLOCK_SAFE_RUN(self.block, options);
        else
            BLOCK_SAFE_RUN(self.failure);
        self.block = nil;
        self.failure = nil;
    });
}


- (void)shareImage:(UIImage *)image success:(Callback)success failure:(Callback)failure{

    if (!image) {
        BLOCK_SAFE_RUN(failure);
        return;
    }
    NSString *publishText = [NSString stringWithFormat:@"%@ %@",kShareString, APPSTORE_LINK];
    
    Callback block = ^{
        [self.twitterAPI postStatusUpdate:publishText mediaDataArray:@[UIImageJPEGRepresentation(image, 0.7f)] possiblySensitive:@NO inReplyToStatusID:nil latitude:nil longitude:nil placeID:nil displayCoordinates:@NO uploadProgressBlock:nil successBlock:^(NSDictionary *status) {
            dispatch_async(dispatch_get_main_queue(), ^{
                BLOCK_SAFE_RUN(success);
            });
        } errorBlock:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                BLOCK_SAFE_RUN(failure);
            });
        }];
    };
    
    if (!self.twitterAPI) {
        [self connectWithCompletionHandler:^(NSDictionary *resultDict) {
            block();
        } failure:failure];
    } else {
        block();
    }
}


@end
