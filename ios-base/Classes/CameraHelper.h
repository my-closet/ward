
#import <Foundation/Foundation.h>

@interface CameraHelper : NSObject
+ (instancetype)sharedHelper;
- (void)launchPhotoCaptureForCategory:(Cat*)category isLibrary:(BOOL)library;
@end
