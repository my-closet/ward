#import "APIManager.h"
#import "AFNetworking.h"
#import "Shop.h"
#import "ShopCategory.h"
//Get info about this API on http://timezonedb.com/api

#define API_KEY @"PF85Q4OP7VRG"

@implementation APIManager
{
    AFHTTPRequestOperationManager *_manager;
}
+ (APIManager*)sharedManager {
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
        [sharedInstance setup];
    });
    return sharedInstance;
}

- (void)setup{
    _manager = [AFHTTPRequestOperationManager manager];
    _manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [_manager.responseSerializer setAcceptableContentTypes:[NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/plain", nil]];
}

- (void)getShopsWithCompleteBlock:(ArrayCallback)block
{
    NSString *fullURL = [NSString stringWithFormat:@"%@/%@",BASE_URL,@"shops"];
    
   // NSDictionary *params = @{ @"action": @"getShops"};
    
    _manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [_manager GET:fullURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [MagicalRecord saveWithArrayBlock:^NSArray *(NSManagedObjectContext *localContext) {
            NSMutableDictionary *dJSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
            NSArray *array = [dJSON valueForKey:@"result"];
            NSMutableArray *objectsArray = [NSMutableArray array];
            for (NSMutableDictionary *dict in array){
                Shop *shop = [Shop objectWithDict:dict inContext:localContext];
                [objectsArray addObject:shop];
            }
            return objectsArray;
            
        } completion:^(NSArray *objectsArray, NSError *error) {
            BLOCK_SAFE_RUN(block, objectsArray);
        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        BLOCK_SAFE_RUN(block, nil);
    }];
}

- (void)getCategoriesWithShop:(Shop*)shop completeBlock:(ArrayCallback)block
{
    NSString *fullURL = [NSString stringWithFormat:@"%@/shop/%d/categories",BASE_URL,shop.uidValue];
    
  //  NSDictionary *params = @{ @"action": @"getCategoriesByShopId", @"id" : @(shop.uidValue),};
    
    _manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [_manager GET:fullURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [MagicalRecord saveWithArrayBlock:^NSArray *(NSManagedObjectContext *localContext) {
            
            NSMutableDictionary *dJSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
            NSArray *array = [dJSON objectForKey:@"result"];
            NSMutableArray *objectsArray = [NSMutableArray array];
            for (NSDictionary *dict in array){
                ShopCategory *shopCat = [ShopCategory objectWithDict:dict inContext:localContext];
                [objectsArray addObject:shopCat];
            }
            return objectsArray;
            
        } completion:^(NSArray *objectsArray, NSError *error) {
            BLOCK_SAFE_RUN(block, objectsArray);
        }];

        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        BLOCK_SAFE_RUN(block, nil);
    }];
}

- (void)getClothesWithShop:(Shop*)shop shopCategory:(NSString*)shopCategory     completeBlock:(ArrayCallback)block
{
    NSString *fullURL = [NSString stringWithFormat:@"%@/category/%@/products",BASE_URL,shopCategory];
    
  //  NSDictionary *params = @{ @"action": @"getProductsByCategoryIdAndShopId",
//                              @"category_id" : @(shopCategory.uidValue),
//                              @"shop_id" : @(shop.uidValue),
//                              };
    
    _manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [_manager GET:fullURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [MagicalRecord saveWithArrayBlock:^NSArray *(NSManagedObjectContext *localContext) {
            
            NSMutableDictionary *dJSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
            NSArray *array = [dJSON objectForKey:@"result"];
            NSMutableArray *objectsArray = [NSMutableArray array];
            
            Shop *localShop = [shop MR_inContext:localContext];
            
            for (NSDictionary *dict in array){
                Item *item = [Item objectWithDict:dict inContext:localContext];
                item.shop_name = localShop.name;
                item.shop_uidValue = localShop.uidValue;
                item.vendor_code = [dict valueForKey:@"vendor_code"];
               // item.productURL = [dict valueForKey:@"url"];
                [objectsArray addObject:item];
            }
            return objectsArray;
            
        } completion:^(NSArray *objectsArray, NSError *error) {
            BLOCK_SAFE_RUN(block, objectsArray);
        }];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        BLOCK_SAFE_RUN(block, nil);
    }];
}

///// filters geting close

- (void)getClothesWithShop:(Shop*)shop shopCategory:(NSString*)shopCategory andFilters:(NSString*)filters completeBlock:(ArrayCallback)block
{
    NSString *fullURL = [NSString stringWithFormat:@"%@/category/%@/products?%@",BASE_URL,shopCategory,filters];
    
    NSString *encoded = [fullURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
   // fullURL = [fullURL stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
    _manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [_manager GET:encoded parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [MagicalRecord saveWithArrayBlock:^NSArray *(NSManagedObjectContext *localContext) {
            
            NSMutableDictionary *dJSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
            NSArray *array = [dJSON objectForKey:@"result"];
            NSMutableArray *objectsArray = [NSMutableArray array];
            
            Shop *localShop = [shop MR_inContext:localContext];
            
            for (NSDictionary *dict in array){
                Item *item = [Item objectWithDict:dict inContext:localContext];
                item.shop_name = localShop.name;
                item.shop_uidValue = localShop.uidValue;
                // item.productURL = [dict valueForKey:@"url"];
                [objectsArray addObject:item];
            }
            return objectsArray;
            
        } completion:^(NSArray *objectsArray, NSError *error) {
            BLOCK_SAFE_RUN(block, objectsArray);
        }];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        BLOCK_SAFE_RUN(block, nil);
    }];
}

//////

- (void)getShopFiltersWithCompleteBlockWithShopID:(NSString*)shopID andHandler:(void(^)(id errorMsg, id response))handler
{
    NSString *fullURL = [NSString stringWithFormat:@"%@/shop/%@/filters",BASE_URL, shopID];
    
    _manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [_manager GET:fullURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        handler(nil, responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        handler(error.description, nil);
    }];

}

///// / // / //  // / GET CLOTHE FILTERS ////////////////////////////

- (void)getCloseFiltersWithCompleteBlockAndHandler:(void(^)(id errorMsg, id response))handler
{
    NSString *fullURL = [NSString stringWithFormat:@"%@/ctypes",BASE_URL];
    
    _manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [_manager GET:fullURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        handler(nil, responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        handler(error.description, nil);
    }];
}

- (void)getAllFiltersWithType:(NSString *)type andCompleteBlockAndHandler:(void(^)(id errorMsg, id response))handler
{
    NSString *fullURL = [NSString stringWithFormat:@"%@/filters?type=%@",BASE_URL,type];
    
    _manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [_manager GET:fullURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        handler(nil, responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        handler(error.description, nil);
    }];
}


- (void)getCloseFiltersWithShopID:(NSString *)shopID andType:(NSString*)type andHandler:(void(^)(id errorMsg, id response))handler
{
    NSString *fullURL = [NSString stringWithFormat:@"%@/shop/%@/categories?type=%@",BASE_URL,shopID,type];
    
    _manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [_manager GET:fullURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        handler(nil, responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        handler(error.description, nil);
    }];
}

- (void)getAllFiltersWithCompleteBlockAndHandler:(void(^)(id errorMsg, id response))handler
{
    NSString *fullURL = [NSString stringWithFormat:@"%@/filters",BASE_URL];
    
    _manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [_manager GET:fullURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        handler(nil, responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        handler(error.description, nil);
    }];
}

////


//- (void)getsClothesWithShop:(Shop*)shop shopCategory:(ShopCategory*)shopCategory     completeBlock:(ArrayCallback)block
//{
//    NSString *fullURL = [NSString stringWithFormat:@"%@",BASE_URL];
//    
//    NSDictionary *params = @{ @"action": @"getProductsByCategoryIdAndShopId",
//                              @"category_id" : @(shopCategory.uidValue),
//                              @"shop_id" : @(shop.uidValue),
//                              };
//    
//    [_manager GET:fullURL parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        
//        [MagicalRecord saveWithArrayBlock:^NSArray *(NSManagedObjectContext *localContext) {
//            
//            NSArray *array = [responseObject objectForKey:@"results"];
//            NSMutableArray *objectsArray = [NSMutableArray array];
//            
//            Shop *localShop = [shop MR_inContext:localContext];
//            
//            for (NSDictionary *dict in array){
//                Item *item = [Item objectWithDict:dict inContext:localContext];
//                item.shop_name = localShop.name;
//                item.shop_uidValue = localShop.uidValue;
//                // item.productURL = [dict valueForKey:@"url"];
//            }
//            [objectsArray addObjectsFromArray:array];
//            return objectsArray;
//            
//        } completion:^(NSArray *objectsArray, NSError *error) {
//            BLOCK_SAFE_RUN(block, objectsArray);
//        }];
//        
//        
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        BLOCK_SAFE_RUN(block, nil);
//    }];
//}


//- (void)getURL:(Shop*)shop shopCategory:(ShopCategory*)shopCategory completeBlock:(ArrayCallback)block{
//    NSString *fullURL = [NSString stringWithFormat:@"%@",BASE_URL];
//    
//    NSDictionary *params = @{ @"action": @"getProductsByCategoryIdAndShopId",
//                              @"category_id" : @(shopCategory.uidValue),
//                              @"shop_id" : @(shop.uidValue),
//                              };
//    
//    [_manager GET:fullURL parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        
//        NSArray *array = [responseObject objectForKey:@"results"];
//        NSMutableArray *objectsArray = [NSMutableArray array];
//        
//        for (NSDictionary *dict in array){
//            [objectsArray addObject:[dict valueForKey:@"name"]];
//            if (![[dict valueForKey:@"url"] isEqual:[NSNull null]]) {
//                [objectsArray addObject:[dict valueForKey:@"url"]];
//            }
//        }
//        return objectsArray;
//        
//    } completion:^(NSArray *objectsArray, NSError *error) {
//        // BLOCK_SAFE_RUN(block, objectsArray);
//    }];

    
    
//        [MagicalRecord saveWithArrayBlock:^NSArray *(NSManagedObjectContext *localContext) {
//            
//            NSArray *array = [responseObject objectForKey:@"results"];
//            NSMutableArray *objectsArray = [NSMutableArray array];
//            
//            for (NSDictionary *dict in array){
//                [objectsArray addObject:[dict valueForKey:@"name"]];
//                if (![[dict valueForKey:@"url"] isEqual:[NSNull null]]) {
//                    [objectsArray addObject:[dict valueForKey:@"url"]];
//                }
//            }
//            return objectsArray;
//            
//        } completion:^(NSArray *objectsArray, NSError *error) {
//           // BLOCK_SAFE_RUN(block, objectsArray);
//        }];
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//      //  BLOCK_SAFE_RUN(block, nil);
//    }];
//}

- (void)sendPostToServer:(NSMutableDictionary *)params andImage:(UIImage *)image andItem:(NSMutableArray *)item andHandler:(void(^)(id errorMsg, id response))handler
{
    NSString *fullURL = [NSString stringWithFormat:@"%@/createShape",BASE_URL];
   _manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSMutableDictionary *parameters = [NSMutableDictionary new];
    [parameters setObject:params forKey:@"data"];
    [parameters setObject:[[[UIDevice currentDevice] identifierForVendor] UUIDString] forKey:@"imei"];
    

    NSString *newStr = [item componentsJoinedByString:@"|"];
    [parameters setObject:newStr forKey:@"item"];
    
    [_manager POST:fullURL parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        if (image != nil) {
            [formData appendPartWithFileData:UIImageJPEGRepresentation(image, 1) name:@"post" fileName:@"post" mimeType:@"image/jpg"];
        }
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSMutableDictionary *dJSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
        NSLog(@"%@",dJSON);
        handler(nil, dJSON);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        handler(error.localizedDescription, nil);
    }];
}

- (void)setLiktToPost:(NSString *)uid andHandler:(void(^)(id errorMsg, id response))handler
{
    NSString *fullURL = [NSString stringWithFormat:@"%@/likeShape",BASE_URL];
    _manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    NSMutableDictionary *parameters = [NSMutableDictionary new];
    [parameters setObject:uid forKey:@"uid"];
    [parameters setObject:[[[UIDevice currentDevice] identifierForVendor] UUIDString] forKey:@"imei"];
    [_manager POST:fullURL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSMutableDictionary *dJSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
        NSArray *array = dJSON;
        handler(nil, array);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        handler(error.localizedDescription, nil);
    }];
}


- (void)getFavoriteWithHandler:(void(^)(id errorMsg, id response))handler
{
    NSString *fullURL = [NSString stringWithFormat:@"%@/shape/popular?limit=100",BASE_URL];
    _manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [_manager GET:fullURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSMutableDictionary *dJSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
        NSArray *array = [dJSON objectForKey:@"result"];
         handler(nil, array);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        handler(error.localizedDescription, nil);
    }];
}

- (void)getUIDArrayandHandler:(void(^)(id errorMsg, id response))handler
{
  //  NSString *fullURL = [NSString stringWithFormat:@"%@/likedShape/%@",BASE_URL,[[[UIDevice currentDevice] identifierForVendor] UUIDString]];
    
    NSString *fullURL = @"http://ishop.mycloset.ru/api/likedShape/48E6C0C8-00D3-4C94-981F-C6DB0B04DB83";
    
    _manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [_manager GET:fullURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSMutableDictionary *dJSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
        NSArray *array = [dJSON objectForKey:@"result"];
        handler(nil, array);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        handler(error.localizedDescription, nil);
    }];
}


- (void)getPopularPage:(NSString *)page WithCompleteBlockAndHandler:(void(^)(id errorMsg, id response))handler
{
    NSString *fullURL = [NSString stringWithFormat:@"%@/popular?limit=50&page=%@",BASE_URL,page];
    _manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [_manager GET:fullURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [MagicalRecord saveWithArrayBlock:^NSArray *(NSManagedObjectContext *localContext) {
            
            NSMutableDictionary *dJSON = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
            NSArray *array = [dJSON objectForKey:@"result"];
            NSMutableArray *objectsArray = [NSMutableArray array];
            
            for (NSDictionary *dict in array){
                Item *item = [Item objectWithDict:dict inContext:localContext];
                item.shop_name = [dict valueForKey:@"shop_name"];
                item.shop_uidValue = [[dict valueForKey:@"shop_id"] integerValue];
                item.vendor_code = [dict valueForKey:@"vendor_code"];
                //item.pro = [dict valueForKey:@"url"];
                [objectsArray addObject:item];
            }
            return objectsArray;
            
        } completion:^(NSArray *objectsArray, NSError *error) {
                handler(nil, objectsArray);
        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        handler(error.localizedDescription, nil);
    }];

}

@end
