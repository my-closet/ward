#import <Foundation/Foundation.h>

#define DOMAIN_URL @"http://ishop.mycloset.ru"


#define BASE_URL [NSString stringWithFormat:@"%@/api",DOMAIN_URL]

//#define BASE_URL @"http://ishop.mycloset.ru/api"


@class Shop, ShopCategory;

@interface APIManager : NSObject

+ (APIManager*)sharedManager;

- (void)getShopsWithCompleteBlock:(ArrayCallback)block;

- (void)getCategoriesWithShop:(Shop*)shop completeBlock:(ArrayCallback)block;

- (void)getClothesWithShop:(Shop*)shop shopCategory:(ShopCategory*)shopCategory     completeBlock:(ArrayCallback)block;

- (void)getsClothesWithShop:(Shop*)shop shopCategory:(ShopCategory*)shopCategory     completeBlock:(ArrayCallback)block;

- (void)getShopWithID:(NSString*)identifier andHandler:(void(^)(id errorMsg, id response))handler;

- (void)getShopFiltersWithCompleteBlockWithShopID:(NSString *)shopID andHandler:(void(^)(id errorMsg, id response))handler;

- (void)getCloseFiltersWithShopID:(NSString *)shopID andType:(NSString*)type andHandler:(void(^)(id errorMsg, id response))handler;

- (void)getClothesWithShop:(Shop*)shop shopCategory:(NSString*)shopCategory andFilters:(NSString*)filters completeBlock:(ArrayCallback)block;

- (void)sendPostToServer:(NSMutableDictionary *)params andImage:(UIImage *)image andItem:(NSMutableArray *)item andHandler:(void(^)(id errorMsg, id response))handler;

- (void)getFavoriteWithHandler:(void(^)(id errorMsg, id response))handler;

- (void)setLiktToPost:(NSString *)uid andHandler:(void(^)(id errorMsg, id response))handler;

- (void)getAllFiltersWithCompleteBlockAndHandler:(void(^)(id errorMsg, id response))handler;

- (void)getPopularPage:(NSString *)page WithCompleteBlockAndHandler:(void(^)(id errorMsg, id response))handler;

- (void)getAllFiltersWithType:(NSString *)type andCompleteBlockAndHandler:(void(^)(id errorMsg, id response))handler;

- (void)getUIDArrayandHandler:(void(^)(id errorMsg, id response))handler;


@end
