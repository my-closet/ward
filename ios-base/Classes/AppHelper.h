#import <Foundation/Foundation.h>

#define CANVAS_BG_COLOR RGB(230, 230, 230)

@interface AppHelper : NSObject
+ (AppHelper*)sharedHelper;
+ (UIColor*) defaultRedColor;
- (UILabel*)titleViewWithString:(NSString*)string;

- (NSString*)currencyStringFromString:(NSNumber*)number;

+ (void)regenerateDataBaseIfNeeded;

+ (NSString*)saveImage:(UIImage*)image;
+ (UIImage *)imageFromUUIDString:(NSString*)uuidString;
+ (void)deleteImageWithUUIDString:(NSString*)uuidString;
+ (NSString*)imageFilePathFromUUID:(NSString*)uuidString;


+ (void)setupNavigationController:(UINavigationController*)nc;
@end
