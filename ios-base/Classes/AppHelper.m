

#import "AppHelper.h"

@interface AppHelper()
@property (nonatomic, strong) NSNumberFormatter *numberFormatter;
@end

@implementation AppHelper
+ (AppHelper*)sharedHelper {
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}
- (id) init{
    self = [super init];
    if (self){
        [self setup];
    }
    return self;
}

- (void)setup{
    self.numberFormatter = [NSNumberFormatter new];
    [self.numberFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"ru_RU"]];
    self.numberFormatter.numberStyle = NSNumberFormatterCurrencyStyle;
    self.numberFormatter.zeroSymbol = @"- р.";
    self.numberFormatter.nilSymbol = @"- р.";
    self.numberFormatter.currencySymbol = @"р.";
    self.numberFormatter.allowsFloats = NO;
    self.numberFormatter.alwaysShowsDecimalSeparator = NO;
    self.numberFormatter.maximumFractionDigits = 0;
}

- (NSString*)currencyStringFromString:(NSNumber*)number{
    return [self.numberFormatter stringFromNumber:number];
}

+ (UIColor*) defaultRedColor{
    return RGB (236, 46, 46);
}
+ (UIFont *)fontBoldWithSize:(CGFloat)size{
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:size];
    if (font)
        return font;
    else
        return [UIFont boldSystemFontOfSize:size];
}
+ (UIFont *)fontRegularWithSize:(CGFloat)size{
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:size];
    if (font)
        return font;
    else
        return [UIFont systemFontOfSize:size];
}

- (UILabel*)titleViewWithString:(NSString*)string
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    label.font = [AppHelper fontBoldWithSize:20.0f];
    label.text = string;
    label.textColor = [UIColor whiteColor];
    [label resizeHorizontallyToFit];
    if (label.width > 200)
        label.width = 200;
    label.backgroundColor = [UIColor clearColor];
    label.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin;
    [label setTranslatesAutoresizingMaskIntoConstraints:YES];
    return label;
}

+ (void)regenerateDataBaseIfNeeded
{
    NSArray *categories = @[@"Верхняя одежда",
                            @"Нижняя одежда",
                            @"Избранное"];
    int count = (int)[[Cat MR_findAll] count];
    if (count == 0){
        [Item MR_truncateAll];
        
      //  [self saveElementWithImagesName:[NSArray arrayWithObjects:@"образ, 2-1.jpg",@"образ, 1-2 и 2-2.jpg",@"образ, 2.jpg", nil] andItemName:@"3"];
        
        Item *item1 = [Item MR_createEntity];
        item1.uidValue = [Item freeUIDValue];
        item1.image_url = @"образ, 2-1.jpg";
        item1.name = @"Рубашка 1";
        
        Item *item2 = [Item MR_createEntity];
        item2.uidValue = [Item freeUIDValue];
        item2.image_url = @"образ, 1-2 и 2-2.jpg";
        item2.name = @"Брюки 1";
        
        Item *item3 = [Item MR_createEntity];
        item3.uidValue = [Item freeUIDValue];
        item3.image_url = @"образ, 2.jpg";
        item3.name = @"1";
        
        CGFloat width = [[UIScreen mainScreen] applicationFrame].size.width;
        
        CGFloat koef = width / 320.0f;
        
        CGRect frameForTop = CGRectMake(0, 0, width, 205 * koef);
        CGRect frameForBottom = CGRectMake(0, 183 * koef, width, 219 * koef);
        
        item3.favorite_components = @[@{@"frame" : [NSValue valueWithCGRect:frameForBottom], @"item_id" : item2.uid},
                                      @{@"frame" : [NSValue valueWithCGRect:frameForTop], @"item_id" : item1.uid}];
        
        for (int i = 0; i < categories.count; i++){
            NSString *category = categories[i];
            Cat *cat = [Cat MR_createEntity];
            cat.name = category;
            if (i == 0)
                [cat.itemsSet addObject:item1];
            if (i == 1)
                [cat.itemsSet addObject:item2];
            if (i == 2){
                [cat.itemsSet addObject:item3];
                cat.favoriteValue = YES;
            }
        }
        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:nil];
    }
}

+ (NSString*)saveImage:(UIImage*)image
{
    NSString *uuidString = [[self class] generateString];
    
    NSString *docsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    docsDir = [docsDir stringByAppendingPathComponent:uuidString];
    docsDir = [docsDir stringByAppendingPathExtension:@"jpg"];
    [UIImageJPEGRepresentation(image, 1.0) writeToFile:docsDir atomically:YES];
    NSError *error;
    NSURL *url = [NSURL fileURLWithPath:docsDir];
    [url setResourceValue:[NSNumber numberWithBool:YES] forKey:NSURLIsExcludedFromBackupKey error:&error];
    
    return uuidString;
}
+ (void)deleteImageWithUUIDString:(NSString*)uuidString
{
    NSString *docsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    docsDir = [docsDir stringByAppendingPathComponent:uuidString];
    docsDir = [docsDir stringByAppendingPathExtension:@"jpg"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:docsDir])
        [[NSFileManager defaultManager] removeItemAtPath:docsDir error:nil];
}
+ (NSString*)generateString
{
    CFUUIDRef uuid = CFUUIDCreate(kCFAllocatorDefault);
    NSString *uuidString = (__bridge_transfer NSString *)CFUUIDCreateString(kCFAllocatorDefault, uuid);
    CFRelease(uuid);
    return uuidString;
}
+ (UIImage *)imageFromUUIDString:(NSString*)uuidString
{
    if ([[uuidString pathExtension] isEqualToString:@"jpg"] &&
        [UIImage imageNamed:uuidString]){
        return [UIImage imageNamed:uuidString];
    }
    
    NSString *docsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    docsDir = [docsDir stringByAppendingPathComponent:uuidString];
    docsDir = [docsDir stringByAppendingPathExtension:@"jpg"];
    
    UIImage *image = [UIImage imageWithContentsOfFile:docsDir];
    return image;
}
+ (void)setupNavigationController:(UINavigationController*)nc
{
    nc.navigationBar.tintColor = [UIColor whiteColor];
    nc.navigationBar.translucent = NO;
    nc.navigationBar.barStyle = UIBarStyleBlack;
    nc.navigationBar.barTintColor = RGB(236, 46, 46);
}

@end
