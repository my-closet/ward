//
//  CollectionShopItem.h
//  wardrobe
//
//  Created by Oleg Mytsouda on 18.01.16.
//  Copyright © 2016 Farcom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionShopItem : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (strong, nonatomic) IBOutlet UILabel *nameM;
@property (strong, nonatomic) IBOutlet UILabel *priceValue;

- (void)loadShopImageWithURL:(NSURL*)url;
- (void)loadLocalImageWithItem:(Item*)item;

@end
