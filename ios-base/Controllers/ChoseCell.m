//
//  ChoseCell.m
//  wardrobe
//
//  Created by Oleg Mytsouda on 08.03.16.
//  Copyright © 2016 Farcom. All rights reserved.
//

#import "ChoseCell.h"

@implementation ChoseCell

- (void)awakeFromNib
{
    self.checkImage.image = [UIImage imageNamed:@"unTap.png"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}


- (void)changeImageWithBool:(BOOL)select
{
    if (select) {
        self.isChek = NO;
        self.checkImage.image = [UIImage imageNamed:@"unTap.png"];
    } else {
        self.checkImage.image = [UIImage imageNamed:@"onTap.png"];
        self.isChek = YES;
    }
}

- (IBAction)selectedCell:(id)sender
{
    [self changeImageWithBool:self.isChek];
    [self.delegate selectedCell:self];
}

@end
