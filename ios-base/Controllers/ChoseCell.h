//
//  ChoseCell.h
//  wardrobe
//
//  Created by Oleg Mytsouda on 08.03.16.
//  Copyright © 2016 Farcom. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ChoseCell;

@protocol ChoseCellDelegate <NSObject>

- (void)selectedCell:(ChoseCell *)cell;

@end

@interface ChoseCell : UITableViewCell

@property (assign, nonatomic) id <ChoseCellDelegate>delegate;

@property (strong, nonatomic) IBOutlet UILabel *filtersLabel;
@property (strong, nonatomic) IBOutlet UIImageView *checkImage;

@property (assign, nonatomic) BOOL isChek;

- (IBAction)selectedCell:(id)sender;

- (void)changeImageWithBool:(BOOL)select;

@end
