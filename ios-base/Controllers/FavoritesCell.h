//
//  FavoritesCell.h
//  wardrobe
//
//  Created by Oleg Mytsouda on 26.11.15.
//  Copyright © 2015 Farcom. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FavoritesCell;

@protocol FavoritesDelegate <NSObject>

- (void)sendLikeToPostWithCell:(FavoritesCell *)cell;

@end

@interface FavoritesCell : UICollectionViewCell

@property (assign, nonatomic) id<FavoritesDelegate>delegate;

@property (strong, nonatomic) IBOutlet UIImageView *itemAvatar;
@property (strong, nonatomic) IBOutlet UILabel *dataLabel;
@property (strong, nonatomic) IBOutlet UIImageView *heartImage;
@property (strong, nonatomic) IBOutlet UILabel *likeLabel;
@property (strong, nonatomic) NSString *udid;

- (IBAction)likeButtonTapped:(id)sender;

@end
