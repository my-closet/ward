
//
//  FavoritesVC.m
//  wardrobe
//
//  Created by Oleg Mytsouda on 26.11.15.
//  Copyright © 2015 Farcom. All rights reserved.
//

#import "FavoritesVC.h"
#import "FavoritesCell.h"
#import "APIManager.h"
#import <UIImageView+AFNetworking.h>
#import "FakePreviewVC.h"
#import "Item.h"
#import "LikedItems.h"

@interface FavoritesVC () <UICollectionViewDataSource, UICollectionViewDelegate, FavoritesDelegate>

@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentControll;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;

- (IBAction)segmentControllAction:(id)sender;

@property (strong, nonatomic) NSMutableArray *collectionViewArray;
@property (strong, nonatomic) NSMutableArray *itemsArray;

@property (nonatomic, strong) NSMutableArray *sharedUID;

@property (nonatomic, strong) NSArray *value;

@end

@implementation FavoritesVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self getUIDValueArray];
    [self getFavorites];
    self.value = [NSArray new];
}

- (void)getFavorites
{
    [SVProgressHUD show];
    [[APIManager sharedManager] getFavoriteWithHandler:^(id errorMsg, id response) {
        if (errorMsg == nil) {
            [SVProgressHUD dismiss];
//            [NSThread detachNewThreadSelector: @selector(createNewItemWithFAvoriteAndSaveToBase:) toTarget:self withObject:response];
//            dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
//                [self createNewItemWithFAvoriteAndSaveToBase:response];
//            });
            self.collectionViewArray = [NSMutableArray arrayWithArray:response];
            [self.collectionView reloadData];
        }
    }];
}

- (void)getUIDValueArray
{
    self.sharedUID = [NSMutableArray new];
    NSArray *people = [LikedItems MR_findAll];
    
    for (LikedItems *liked in people) {
        [self.sharedUID addObject:liked.serverLikeID];
    }
}

- (void)createNewItemWithFAvoriteAndSaveToBase:(id)response
{
    NSLog(@"start");
    NSMutableDictionary *favouriteDict = [NSMutableDictionary new];
    NSMutableArray *itemsArray = [NSMutableArray new];
    
    Item *sendedItem;
    
    Item *findedValue = [Item MR_findByAttribute:@"name" withValue:[response valueForKey:@"id"]];
        
    NSArray *valueFinded = (NSArray *)findedValue;
    
    if (valueFinded.count <= 1) {
    
    NSManagedObjectContext *specContext = [NSManagedObjectContext MR_defaultContext];
    [specContext performBlockAndWait:^{
   // for (NSArray *elementItems in [response valueForKey:@"item"]) {
       // if (![[elementItems valueForKey:@"item"] isKindOfClass:[NSNull class]]) {
    
//    NSManagedObjectContext *specContext = [NSManagedObjectContext MR_defaultContext];
//    [specContext performBlockAndWait:^{

            NSArray *elementArray = [[response valueForKey:@"item"] componentsSeparatedByString:@"|"];
            for (NSString *element in elementArray) {
                NSArray *itemInform = [element componentsSeparatedByString:@";"];
                Item *newItem = [Item MR_createEntity];
                newItem.uidValue = [Item freeUIDValue];
                NSString *imageURL = [AppHelper saveImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[itemInform objectAtIndex:1]]]]]];
                newItem.image_url = imageURL;
                newItem.name = [NSString stringWithFormat:@"%@", [itemInform objectAtIndex:0]];
                [favouriteDict setObject:itemInform[2] forKey:@"frame"];
                [favouriteDict setObject:[NSString stringWithFormat:@"%d", newItem.uidValue] forKey:@"item_id"];
                [itemsArray addObject:favouriteDict];
                NSError *error;

                [newItem.managedObjectContext save:&error];

             //   [newItem.managedObjectContext MR_saveToPersistentStoreAndWait];
             //   [specContext MR_saveToPersistentStoreAndWait];
            }
       // }
        
//    }];
    
    
        Item *itemToFavorit = [Item MR_createEntity];
        itemToFavorit.uidValue = [Item freeUIDValue];
        NSString *imageFavoriteURL = [AppHelper saveImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://ishop.mycloset.ru%@",[response valueForKey:@"image"]]]]]];
        itemToFavorit.image_url = imageFavoriteURL;
        itemToFavorit.name = [NSString stringWithFormat:@"%d",itemToFavorit.uidValue ];
        
        itemToFavorit.favorite_components = itemsArray;
        
        NSError *error;

        [itemToFavorit.managedObjectContext save:&error];

//        [itemToFavorit.managedObjectContext MR_saveToPersistentStoreAndWait];
//        [specContext MR_saveToPersistentStoreAndWait];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self performSegueWithIdentifier:@"toPrev" sender:itemToFavorit];
        });
        
    }];
        
    }
    //}
    
    NSLog(@"finish");
}

- (IBAction)segmentControllAction:(id)sender
{
}

#pragma mark - collectionDelegate

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.collectionViewArray.count;
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"fav";
    
    FavoritesCell *cell = [[FavoritesCell alloc]init];
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    NSArray *newArray = self.collectionViewArray[indexPath.row];
    
    cell.delegate = self;
    
    if (![[newArray valueForKey:@"image"] isKindOfClass:[NSNull class]]) {
        [cell.itemAvatar setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://ishop.mycloset.ru%@",[newArray valueForKey:@"image"]]] placeholderImage:nil];
    } else {
        cell.itemAvatar.backgroundColor = [UIColor redColor];
    }
    cell.likeLabel.text = [newArray valueForKey:@"rating"];
    cell.dataLabel.text = [self setDataWithString:[newArray valueForKey:@"created"]];
    cell.udid = [newArray valueForKey:@"id"];
    
    cell.heartImage.image = [UIImage imageNamed:@"littleHeart"];
    
    if ([self.sharedUID containsObject:cell.udid]) {
        cell.likeLabel.textColor = [UIColor redColor];
    } else {
        cell.likeLabel.textColor = [UIColor colorWithRed:246/255.f green:170/255.f blue:170/255.f alpha:1.f];
    }
    
    [cell.layer setBorderColor:[UIColor colorWithRed:213.0/255.0f green:210.0/255.0f blue:199.0/255.0f alpha:0.6f].CGColor];
    [cell.layer setBorderWidth:0.9f];
    [cell.layer setShadowOffset:CGSizeMake(0, 1)];
    [cell.layer setShadowColor:[[UIColor lightGrayColor] CGColor]];
    [cell.layer setShadowRadius:10.0];
    [cell.layer setShadowOpacity:0.1];
    
    return cell;
}

- (void)sendLikeToPostWithCell:(FavoritesCell *)cell
{
    [[APIManager sharedManager] setLiktToPost:cell.udid andHandler:^(id errorMsg, id response) {
        if (errorMsg == nil) {
            LikedItems *likeItem = [LikedItems MR_createEntity];
            likeItem.myLikeID = [NSString stringWithFormat:@"%@",cell.udid];
            likeItem.serverLikeID = [response valueForKey:@"id"];
            [likeItem.managedObjectContext MR_saveToPersistentStoreAndWait];
            [self getUIDValueArray];
            [self getFavorites];
        }
    }];
}

- (NSString *)setDataWithString:(NSString *)str
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *output = [dateFormatter dateFromString:str];
    NSDateFormatter *outputDateFormat = [[NSDateFormatter alloc] init];
    outputDateFormat.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [outputDateFormat setDateFormat:@"dd.MM.yyyy"];
    NSString *outputSTR = [outputDateFormat stringFromDate:output];
    return outputSTR;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    [SVProgressHUD show];
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0),
                   ^{
                        [NSThread detachNewThreadSelector: @selector(createNewItemWithFAvoriteAndSaveToBase:) toTarget:self withObject:self.collectionViewArray[indexPath.row]];
                       self.value = self.collectionViewArray[indexPath.row];
            });
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"toPrev"]) {
        FakePreviewVC *vc = (FakePreviewVC*)segue.destinationViewController;
        vc.getItem = sender;
        vc.value = self.value;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
