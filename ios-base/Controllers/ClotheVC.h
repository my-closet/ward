//
//  ClotheVC.h
//  wardrobe
//
//  Created by Oleg Mytsouda on 26.02.16.
//  Copyright © 2016 Farcom. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Shop.h"

@interface ClotheVC : UIViewController

@property (strong, nonatomic) Shop *shop;

@end
