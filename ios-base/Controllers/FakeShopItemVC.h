//
//  FakeShopItemVC.h
//  wardrobe
//
//  Created by Oleg Mytsouda on 03.05.16.
//  Copyright © 2016 Farcom. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Shop.h"
#import "ShopCategory.h"

@interface FakeShopItemVC : UIViewController

@property(strong, nonatomic) NSString *navBarTitle;

@property (nonatomic, strong) Shop *shop;
@property (nonatomic, strong) ShopCategory *shopCategory;

@end
