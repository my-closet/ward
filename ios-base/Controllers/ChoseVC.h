//
//  ChoseVC.h
//  wardrobe
//
//  Created by Oleg Mytsouda on 08.03.16.
//  Copyright © 2016 Farcom. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ChoseDelegate <NSObject>

- (void)returnValueArray:(NSArray *)valuesArray;

@end

@interface ChoseVC : UIViewController

@property (strong, nonatomic) NSString* navTitle;
@property (strong, nonatomic) NSArray* valuesArray;

@property (strong, nonatomic) NSMutableArray *selectedItems;

@property (assign, nonatomic) id <ChoseDelegate>delegate;

@end
