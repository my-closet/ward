//
//  FilterCell.h
//  wardrobe
//
//  Created by Oleg Mytsouda on 07.03.16.
//  Copyright © 2016 Farcom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilterCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *filtersLabel;
@property (strong, nonatomic) IBOutlet UILabel *filterValue;

@property (strong, nonatomic) NSDictionary *element;

@end
