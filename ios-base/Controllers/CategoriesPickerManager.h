
#import <Foundation/Foundation.h>

@interface CategoriesPickerManager : NSObject
- (void)setupWithItem:(Item*)item controller:(UIViewController*)controller;
- (void)dismiss;
@end
