//
//  ScanerView.m
//  wardrobe
//
//  Created by Oleg Mytsouda on 27.10.15.
//  Copyright © 2015 Farcom. All rights reserved.
//

#import "ScanerView.h"
#import "RSCodeView.h"
#import "RSCodeGen.h"

@interface ScanerView ()

@property (nonatomic, weak) IBOutlet RSCodeView *codeView;

@end

@implementation ScanerView

- (void)__applicationDidEnterBackground:(NSNotification *)notification
{
    self.navigationController.title = @"";
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        __weak typeof(self) weakSelf = self;
        self.barcodesHandler = ^(NSArray *barcodeObjects) {
            if (barcodeObjects.count > 0) {
                NSMutableString *text = [[NSMutableString alloc] init];
                [barcodeObjects enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    [text appendString:[NSString stringWithFormat:@"%@: %@", [(AVMetadataObject *)obj type], [obj stringValue]]];
                    if (idx != (barcodeObjects.count - 1)) {
                        [text appendString:@"\n"];
                    }
                }];
                dispatch_async(dispatch_get_main_queue(), ^{
                    weakSelf.navigationController.title = text;
                });
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    weakSelf.navigationController.title = @"";
                });
            }
        };
        
        self.tapGestureHandler = ^(CGPoint tapPoint) {
        };
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidEnterBackgroundNotification
                                                  object:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.codeView.code = [CodeGen genCodeWithContents:@"AAABBBCCCDDDEEE1" machineReadableCodeObjectType:AVMetadataObjectTypeCode128Code];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(__applicationDidEnterBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.view bringSubviewToFront:self.codeView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
