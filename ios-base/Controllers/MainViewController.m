
#import "MainViewController.h"
#import "APIManager.h"
#import "PECropViewController.h"
#import "TabButton.h"
#import "UIBarButtonItem+Extensions.h"
#import "WardrobeController.h"
#import "ItemEditController.h"
#import "CanvasView.h"
#import "ClothesTableController.h"
#import "FacebookManager.h"
#import "VKManager.h"
#import "TwitterManager.h"
#import "CameraHelper.h"
#import "PopupView.h"
#import "TutorialView.h"
#import "PureLayout.h"
#import "BuyFormVC.h"
#import "CanvasItem.h"
#import "LikedItems.h"

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)

@interface MainViewController ()<UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate, PopupViewDelegate, UIDocumentInteractionControllerDelegate, SegueWithItemDelegate>
@property (nonatomic, weak) IBOutlet TabButton *cameraButton;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *tabBarBottomConstraint;
@property (nonatomic, weak) IBOutlet UIImageView *userImageView;
@property (nonatomic, weak) IBOutlet UIButton *shareButton;
@property (nonatomic, weak) IBOutlet UIButton *favButton;
@property (nonatomic, weak) IBOutlet UIButton *shopButton;
@property (nonatomic, strong) NSMutableArray *itemsArray;
//@property (nonatomic, assign) BOOL *swipe;
//
//@property (nonatomic, assign) int index;

// heart_off.png
// heart_on.png

@property (nonatomic, strong) NSMutableArray *sharedUID;

@property (nonatomic, retain) UIDocumentInteractionController *documentController;

- (IBAction)saveTapped;

- (IBAction)shopButtonAction:(id)sender;

@end

@implementation MainViewController
{
    BOOL _stateEditing;
    Item *_favoriteItem;
    
    TutorialView *_tutorialView;
}

- (void)viewDidLoad
{
    [self getUIDValueArray];
    [super viewDidLoad];
    [self updateItems];
    self.cameraButton.isLargeButton = YES;
    [self.cameraButton setNeedsLayout];
    [self.cameraButton addTarget:self action:@selector(cameraButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    [self setupNavbar];
    [self setupShareButton];
    
    if (IS_IPHONE_6) {
        self.canvasView = [[CanvasView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, self.view.height - 74 - self.shopButton.height)];
    } else if (IS_IPHONE_6P) {
        self.canvasView = [[CanvasView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, self.view.height - 84 - self.shopButton.height)];
    } else {
        self.canvasView = [[CanvasView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, self.view.height - 64 - self.shopButton.height)];
    }
    
    self.canvasView.delegate = self;
    
    [self.view insertSubview:self.canvasView atIndex:0];
    
    [self.canvasView autoSetDimensionsToSize:self.canvasView.frame.size];
    [self.canvasView autoPinEdgeToSuperviewEdge:ALEdgeLeading];
    [self.canvasView autoPinEdgeToSuperviewEdge:ALEdgeTop];
    
    [self.canvasView setup];
    [self implementBArScaner];
    
    dispatch_after_short(0.0, ^{
        [self setupTutorialIfNeeded];
    });
    
    if ([self.sharedUID containsObject:_favoriteItem.uid]) {
        [self.shareButton setImage:[UIImage imageNamed:@"heart_on.png"] forState:UIControlStateNormal];
    } else {
        [self.shareButton setImage:[UIImage imageNamed:@"heart_off.png"] forState:UIControlStateNormal];
    }
}

- (void)getUIDValueArray
{
    self.sharedUID = [NSMutableArray new];
    NSArray *people = [LikedItems MR_findAll];

    for (LikedItems *liked in people) {
        [self.sharedUID addObject:liked.myLikeID];
    }
    
//    [[APIManager sharedManager] getUIDArrayandHandler:^(id errorMsg, id response) {
//        if (errorMsg == nil) {
//            for (NSDictionary *uidDictionary in response) {
//                [self.sharedUID addObject:[uidDictionary valueForKey:@"id"]];
//            }
//        }
//    }];
}


- (void)implementBArScaner
{
    self.scanner = [[RSScannerViewController alloc] initWithCornerView:YES controlView:YES
    barcodesHandler:^(NSArray *barcodeObjects) {
        if (barcodeObjects.count > 0) {
            [barcodeObjects enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    AVMetadataMachineReadableCodeObject *code = obj;
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Barcode found"
                    message:code.stringValue delegate:self cancelButtonTitle:@"OK"
                    otherButtonTitles:nil];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.scanner dismissViewControllerAnimated:true completion:nil];
                        [alert show];
                    });
                });
            }];
        }
    }
                    
    preferredCameraPosition:AVCaptureDevicePositionBack];
    [self.scanner setIsButtonBordersVisible:YES];
    [self.scanner setStopOnFirst:YES];
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.swipe = NO;
    [self setupView];
}


- (void)updateItems{
    Cat *favoriteCategory = [Cat favoriteCategory];
    Cat *cat = [favoriteCategory MR_inContext:[NSManagedObjectContext MR_defaultContext]];
    self.itemsArray = [NSMutableArray arrayWithArray:[cat.items array]];
    self.index = self.itemsArray.count - 1;
    _favoriteItem = [self.itemsArray lastObject];
    if ([self.sharedUID containsObject:_favoriteItem.uid]) {
        [self.shareButton setImage:[UIImage imageNamed:@"heart_on.png"] forState:UIControlStateNormal];
    } else {
        [self.shareButton setImage:[UIImage imageNamed:@"heart_off.png"] forState:UIControlStateNormal];
    }
}


- (void)setupView{
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc]
                                           initWithTarget:self
                                           action:@selector(didSwipeLeft)];
    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeLeft];
    
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc]
                                            initWithTarget:self
                                            action:@selector(didSwipeRight)];
    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipeRight];
}


-(void)saveIfNeded{
    [_canvasView saveFavoriteItem];
    [self.canvasView clearCanvas];
    [self updateItems];
    [self addFavoriteItemToCanvas];
}


-(void)check{
    if ([self.navigationItem.rightBarButtonItem.title isEqualToString:@"Готово"]) {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Изменить" style:UIBarButtonItemStyleBordered target:self action:@selector(editTapped)];
        self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemWithImage:[UIImage imageNamed:@"wardrobe"] target:self action:@selector(wardrobeTapped)];
        [self.canvasView cancelEditing];
        [self.canvasView removeState];
        _stateEditing = NO;
    }
}


- (void)didSwipeLeft{
    if (self.itemsArray.count > 1 || self.swipe) {
        self.index += 1;
        if (self.index >= self.itemsArray.count) {
            self.index = 0;
        }
        [self check];
        [self.canvasView clearCanvas];
        [self.canvasView addItemsToCanvasFromFavoriteItem:[self.itemsArray objectAtIndex:self.index]];
        _favoriteItem = [self.itemsArray objectAtIndex:self.index];
        
        if ([self.sharedUID containsObject:_favoriteItem.uid]) {
            [self.shareButton setImage:[UIImage imageNamed:@"heart_on.png"] forState:UIControlStateNormal];
        } else {
            [self.shareButton setImage:[UIImage imageNamed:@"heart_off.png"] forState:UIControlStateNormal];
        }
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.3];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromRight];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [[self.canvasView layer] addAnimation:animation forKey:nil];
        self.swipe = NO;
    }
}


- (void)didSwipeRight{
    if (self.itemsArray.count > 1) {
        self.index -= 1;
        if (self.index <= -1) {
            self.index = (int)self.itemsArray.count - 1;
        }
        [self check];
        [self.canvasView clearCanvas];
        [self.canvasView addItemsToCanvasFromFavoriteItem:[self.itemsArray objectAtIndex:self.index]];
        _favoriteItem = [self.itemsArray objectAtIndex:self.index];
        
        if ([self.sharedUID containsObject:_favoriteItem.uid]) {
            [self.shareButton setImage:[UIImage imageNamed:@"heart_on.png"] forState:UIControlStateNormal];
        } else {
            [self.shareButton setImage:[UIImage imageNamed:@"heart_off.png"] forState:UIControlStateNormal];
        }
        
        CATransition *animation = [CATransition animation];
        [animation setDuration:0.3];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromLeft];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
        [[self.canvasView layer] addAnimation:animation forKey:nil];
    }
}


- (void)setupShareButton
{
    UIImage *image = [UIImage imageWithColor:RGBA(205, 205, 205,0.6f) radius:20 rect:CGRectMake(0, 0, 40, 40)];
    [self.shareButton setBackgroundImage:image forState:UIControlStateNormal];
    
    self.shareButton.backgroundColor = nil;
    
    [self.shareButton addTarget:self action:@selector(shareTapped) forControlEvents:UIControlEventTouchUpInside];
}

- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    [self.view sendSubviewToBack:self.canvasView];
}

- (void)shareTapped
{
    if (_canvasView.canvasItemsArray.count == 0)
    {
        [[[UIAlertView alloc] initWithTitle:nil
                                    message:@"Пустой екран"
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
        return;
    }
    
    PopupView *popup = [PopupView popupView];
    popup.delegate = self;
    [popup showInView:self.navigationController.view];
}


- (void)setupNavbar
{
    self.title = @"";
    
    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"moda.png"]];
    imgView.frame = CGRectMake(0,0,320,30);
    imgView.contentMode = UIViewContentModeScaleAspectFit;
    self.navigationItem.titleView = imgView;
    
    [AppHelper setupNavigationController:self.navigationController];
    
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemWithImage:[UIImage imageNamed:@"wardrobe"] target:self action:@selector(wardrobeTapped)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Изменить" style:UIBarButtonItemStyleBordered target:self action:@selector(editTapped)];
}
- (void)cancelTapped
{
//    [self.canvasView restoreState];
//    [self editTapped];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Изменить" style:UIBarButtonItemStyleBordered target:self action:@selector(editTapped)];
    
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemWithImage:[UIImage imageNamed:@"wardrobe"] target:self action:@selector(wardrobeTapped)];
    
    [self.canvasView cancelEditing];
    [self.canvasView removeState];
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)editTapped
{
    _stateEditing = !_stateEditing;
    CGFloat constant;
    if (_stateEditing){
        [self.cameraButton setTitle:@"" forState:UIControlStateNormal];
        [self.canvasView enableEditing];
        [self.canvasView saveState];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Готово" style:UIBarButtonItemStyleBordered target:self action:@selector(editTapped)];
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Отменить" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelTapped)];
    } else {
        constant = 0;
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Изменить" style:UIBarButtonItemStyleBordered target:self action:@selector(editTapped)];
        self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemWithImage:[UIImage imageNamed:@"wardrobe"] target:self action:@selector(wardrobeTapped)];
        NSArray *array = [NSArray arrayWithArray:_favoriteItem.favorite_components];
        if (array.count == 0 && self.itemsArray.count != 0) {
            array = [[self.itemsArray objectAtIndex:self.index] valueForKey:@"favorite_components"];
        }
        
        
        
        BOOL needToSave = NO;
        
        if (self.canvasView.canvasItemsArray.count > array.count || self.canvasView.canvasItemsArray.count < array.count) {
            if (self.canvasView.canvasItemsArray.count == 0) {
                [[NSNotificationCenter defaultCenter] postNotificationName:WillDeleteItemNotification object:_favoriteItem];
                [AppHelper deleteImageWithUUIDString:_favoriteItem.image_url];
                [self.itemsArray removeObject:_favoriteItem];
                [self.itemsArray addObject:@"0"];
                [_favoriteItem MR_deleteEntity];
                [_favoriteItem.managedObjectContext MR_saveToPersistentStoreAndWait];
                [self updateItems];
                needToSave = YES;
                if (self.itemsArray.count != 0) {
                    self.swipe = YES;
                    self.index -= 1;
                    [self didSwipeLeft];
                }
            } else {
                needToSave = YES;
            }
        } else if(self.canvasView.canvasItemsArray.count == array.count){
            NSMutableArray *firstSetArray = [NSMutableArray new];
            NSMutableArray *secondSetArray = [NSMutableArray new];
            for (NSArray*item in array) {
                [firstSetArray addObject:[item valueForKey:@"item_id"]];
            }
            for (CanvasView *it in self.canvasView.canvasItemsArray) {
                NSNumber *addToArray = [[it valueForKey:@"item"]valueForKey:@"uid"];
                [secondSetArray addObject:addToArray];
            }
            NSSet* set1 = [NSMutableSet setWithArray:firstSetArray];
            NSSet* set2 = [NSMutableSet setWithArray:secondSetArray];
            bool isSave = [set1 isSubsetOfSet:set2];
            if (isSave == false) {
                needToSave = YES;
            }
            
            for (CanvasItem *currentCanvas in self.canvasView.canvasItemsArray) {
                for (NSArray *currentFavItem in _favoriteItem.favorite_components) {
                    if ([currentCanvas.item.uid intValue] == [[currentFavItem valueForKey:@"item_id"] intValue]) {
                        if (CGRectEqualToRect(currentCanvas.frame,[[currentFavItem valueForKey:@"frame"] CGRectValue])) {
                            needToSave = YES;
                            break;
                        }
                    }
                }
                if (needToSave) {
                    break;
                }
            }            
        }
        
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"needToSave"] || needToSave) {
            [self saveIfNeded];
        }
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"needToSave"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self.canvasView cancelEditing];
        [self.canvasView removeState];
    }
}


- (ObjectCallback)selectionBlock
{
    __weak typeof(self) aSelf = self;
    
    ObjectCallback block = ^(Item*item){
        
        if (item.category.favoriteValue)
        {
            if (_favoriteItem)
            {
                [aSelf.canvasView clearCanvas];
            } else {
                [aSelf.canvasView saveFavoriteItemAsDraft:YES];
            }
            
            [aSelf.canvasView addItemsToCanvasFromFavoriteItem:item];
            
            _favoriteItem = item;
            
        }
        else
        {
            [aSelf.canvasView addItemToCanvas:item];
            if (!_stateEditing)
                dispatch_after_short(0, ^{
                    [aSelf editTapped];
                });
        }
        
        [aSelf dismissViewControllerAnimated:YES completion:nil];
        
    };
    
    return block;
}
- (ObjectCallback)itemAddToCanvasBlock
{
    __weak typeof(self) aSelf = self;
    
    ObjectCallback block = ^(Item* anItem){
        if (anItem)
            [aSelf.canvasView addItemToCanvas:anItem];
        if (!_stateEditing)
            [aSelf editTapped];
        if (aSelf.navigationController.topViewController != aSelf)
            [aSelf.navigationController popToRootViewControllerAnimated:NO];
    };

    return block;
}


- (void)wardrobeTapped
{
    WardrobeController *controller = [WardrobeController controller];
    controller.selectionBlock = [self selectionBlock];
    if (_stateEditing){
        UINavigationController *nc = [UINavigationController new];
        [AppHelper setupNavigationController:nc];
        [nc pushViewController:controller animated:NO];
        [self presentViewController:nc animated:YES
                         completion:nil];
    } else {
        [self.navigationController pushViewController:controller animated:YES];
    }
}

- (IBAction)gotoFavorites
{
    [self performSegueWithIdentifier:@"FavoritesVC" sender:nil];
//    Cat *cat = [Cat favoriteCategory];
//    if (cat){
//        UIStoryboard *s = [UIStoryboard storyboardWithName:@"main" bundle:nil];
//        ClothesTableController *controller = [s instantiateViewControllerWithIdentifier:NSStringFromClass([ClothesTableController class])];
//        [controller setupWithCategory:cat];
//        controller.selectionBlock = [self selectionBlock];
//        [self.navigationController pushViewController:controller animated:YES];
//    }
    
//    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"buyProduction"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//    [_canvasView saveFavoriteItem];
//    [self addFavoriteItemToCanvas];
}

#pragma mark - Save canvas
- (void)saveTapped
{
    if (_canvasView.canvasItemsArray.count == 0)
    {
        [[[UIAlertView alloc] initWithTitle:nil
                                    message:@"Нечего сохранять"
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
        return;
    }
    
    if (_stateEditing)
    {
        [self editTapped];
    }
    dispatch_after_short(0, ^{
        if (_favoriteItem){
            
            UIImage *image = [_canvasView getImage];
            NSString *imageURL = [AppHelper saveImage:image];
            _favoriteItem.image_url = imageURL;
            [_canvasView saveChangesForExistingFavoriteItem:_favoriteItem];
            [[[UIAlertView alloc] initWithTitle:nil
                                        message:@"Изменения сохранены"
                                       delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil] show];
            _favoriteItem = nil;
        }else
            [_canvasView saveFavoriteItem];
            [self addFavoriteItemToCanvas];
    });
}

#pragma mark - action

- (IBAction)shopButtonAction:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setInteger:self.index > 0 ? self.index : 0 forKey:@"mainItemIndex"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - Image Picker
-(void)cameraButtonTapped
{
    [self showCameraPopup];
}


- (void)showCameraPopup{
    PopupView *popup = [PopupView cameraView];
    popup.delegate = self;
    popup.tag = 999;
    [popup showInView:self.navigationController.view];
}

#pragma mark - Popup View

- (void)popupView:(PopupView*)popupView clickedButtonAtIndex:(int)index
{
    if (popupView.tag == 999){
        if (index == 5) {
            [self presentViewController:self.scanner animated:YES completion:nil];
        } else {
        [[CameraHelper sharedHelper] launchPhotoCaptureForCategory:nil isLibrary:index == 1];
        dispatch_after_short(0.4, ^{
            [self removeTutorialIfNeeded];
        });
        }
        return;
    }
    
    if (index == 0){
        [self manageConnectionWithSocialType:kSocialTypeVk];
    } else if (index == 1){
        [self manageConnectionWithSocialType:kSocialTypeFacebook];
    } else if (index == 2){
        [self manageConnectionWithSocialType:kSocialTypeTwitter];
    } else if (index == 10){
        [self shareToInstagram];
    }
    else if (index == 4){
       // [self saveTapped];
        
        Item *newItem = [Item MR_createEntity];
        newItem.uidValue = [Item freeUIDValue];
        newItem.name = @"newName";
        newItem.category = [Cat favoriteCategory];
        [newItem.managedObjectContext MR_saveToPersistentStoreAndWait];
   
        [self.canvasView addItemsToCanvasFromFavoriteItem:newItem];
        //[self updateItems];

        [self addFavoriteItemToCanvas];
        
    }
    else if (index == 5) {
        [self presentViewController:self.scanner animated:YES completion:nil];
        //[self performSegueWithIdentifier:@"scaner" sender:nil];
    } else if (index == 11) {
      //  create new item
        
//        __weak typeof(self) aSelf = self;
//        __block NSString *url = [_favoriteItem.image_url copy];
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            UIImage *image = [AppHelper imageFromUUIDString:_favoriteItem.image_url];
//            dispatch_after_short(0, ^{

        NSMutableDictionary *paramsDict = [NSMutableDictionary new];
        NSMutableArray *mDict = [NSMutableArray new];
        
        int number = 0;
        
        NSString * uidString = @"";

        
        for (NSArray *elementArray in _favoriteItem.favorite_components) {
            NSMutableDictionary *newDict = [NSMutableDictionary new];
            NSMutableArray *itemDict = [NSMutableArray new];
            Item *findedValue = [Item MR_findByAttribute:@"uid" withValue:[[elementArray valueForKey:@"item_id"] stringValue]];
            
            NSString *urlToImage;
            if (![[findedValue valueForKey:@"image_url"][0] hasPrefix:@"http://"] && ![[findedValue valueForKey:@"image_url"][0] hasPrefix:@"https://"]) {
                urlToImage = [NSString stringWithFormat:@"http://ishop.mycloset.ru%@",[findedValue valueForKey:@"image_url"][0]];
            } else {
                urlToImage = [findedValue valueForKey:@"image_url"][0];
            }
            
            NSString *newStr = [findedValue valueForKey:@"shop_uid"][0];
            NSString *newS = [findedValue valueForKey:@"uid"][0];
            NSString *nStr = [findedValue valueForKey:@"vendor_code"][0];
            
            if (![nStr isKindOfClass:[NSNull class]]) {
                [newDict setObject:nStr forKey:@"vendor_code"];
            } else {
                [newDict setObject:nStr forKey:@"_"];
            }
            
            uidString = newS;
            
            [newDict setObject:newStr forKey:@"shop_id"];
            [newDict setObject:newS forKey:@"sid"];
            
            
            [itemDict addObject:newS];
            [itemDict addObject:urlToImage];
            [itemDict addObject:[elementArray valueForKey:@"frame"]];
            
            [mDict addObject:[itemDict componentsJoinedByString:@";"]];
            
            [paramsDict setObject:newDict forKey:[NSString stringWithFormat:@"%d",number]];
            number = +1;
            }
        
        
//        NSMutableDictionary *newDict = [NSMutableDictionary new];
//        for (NSDictionary *new in <#collection#>) {
//            <#statements#>
//        }
        
        
                [[APIManager sharedManager]sendPostToServer:paramsDict andImage:image andItem:mDict andHandler:^(id errorMsg, id response) {
                    if (!errorMsg && [[response valueForKey:@"success"] boolValue]) {
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.shareButton setImage:[UIImage imageNamed:@"heart_on.png"] forState:UIControlStateNormal];
                        });
                        
                        [[APIManager sharedManager] setLiktToPost:[response valueForKey:@"id"] andHandler:^(id errorMsg, id response) {
                            
                            LikedItems *likeItem = [LikedItems MR_createEntity];
                            likeItem.myLikeID = [NSString stringWithFormat:@"%@",uidString];
                            likeItem.serverLikeID = [response valueForKey:@"id"];
                            [likeItem.managedObjectContext MR_saveToPersistentStoreAndWait];
                            
                        }];
                    }
                }];
//            });
//        });
    

    }
}

- (void)manageConnectionWithSocialType:(kSocialType)type{
    
    FacebookManager *socialAccountManager = nil;
    switch (type) {
        case kSocialTypeFacebook:
            socialAccountManager = (FacebookManager*)[FacebookManager sharedManager];
            break;
        case kSocialTypeTwitter:
            socialAccountManager = (FacebookManager*)[TwitterManager sharedManager];
            break;
        case kSocialTypeVk:
            socialAccountManager = (FacebookManager*)[VKManager sharedManager];
            break;
            
        default:
            break;
    }
    
    __weak typeof(self) aSelf = self;
    
    if ([socialAccountManager isConnected]){
        [SVProgressHUD show];
        [socialAccountManager shareImage:[self.canvasView getImage] success:^{
            [aSelf notifySuccessfullShare];
            [SVProgressHUD dismiss];
        } failure:^{
            [SVProgressHUD dismiss];
        }];
    } else if (socialAccountManager){
        
        [SVProgressHUD show];
        
        __block BOOL shouldHide = YES;
        
        dispatch_after_short(1.5f, ^{
            if (shouldHide) [SVProgressHUD dismiss];
        });
        
        [socialAccountManager connectWithCompletionHandler:^(NSDictionary *resultDict) {
            
            shouldHide = NO;
            [SVProgressHUD show];
            
            [socialAccountManager shareImage:[aSelf.canvasView getImage] success:^{
                [SVProgressHUD dismiss];
                [self notifySuccessfullShare];
            } failure:^{
                [SVProgressHUD dismiss];
            }];
            
        } failure:^{
            [SVProgressHUD dismiss];
        }];
    }
}

- (void)shareToInstagram{
    UIImage *image = [self.canvasView getImage];
    
    CGFloat max = MAX(image.size.width, image.size.height);
    
    UIGraphicsBeginImageContext(CGSizeMake(max, max));
    
    CGRect rect = CGRectMake(0, 0, image.size.width, image.size.height);

    if (image.size.width > image.size.height)
        rect.origin.y = (max - image.size.height)/2.0f;
    else
        rect.origin.x = (max - image.size.width)/2.0f;
        
    [image drawInRect:rect];
    
    UIImage *tempImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    if ([[UIApplication sharedApplication] canOpenURL: [NSURL URLWithString:@"whatsapp://app"]]){
        
        UIImage     * iconImage = tempImage;
        NSString    * savePath  = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/whatsAppTmp.wai"];
        
        [UIImageJPEGRepresentation(iconImage, 1.0) writeToFile:savePath atomically:YES];
        
        self.documentController = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:savePath]];
        self.documentController.UTI = @"net.whatsapp.image";
        self.documentController.delegate = self;
        
        [self.documentController presentOpenInMenuFromRect:CGRectMake(0, 0, 0, 0) inView:self.view animated: YES];
        
    } else {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"WhatsApp not installed." message:@"Your device has no WhatsApp installed." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
}

- (void)notifySuccessfullShare
{
    [[[UIAlertView alloc] initWithTitle:@"Запись успешно опубликована" message:nil delegate:nil cancelButtonTitle:@"ОК" otherButtonTitles:nil] show];
}

#pragma mark - Tutorial View

- (void)setupTutorialIfNeeded{
    
    if (![UserDefaults boolForKey:@"tutorial_shown_1"]){
        [UserDefaults setBool:YES forKey:@"tutorial_shown_1"];
        [UserDefaults synchronize];
        [self AddNewElementToTestCanvas];
        [self addFavoriteItemToCanvas];
    } else {
        [self addFavoriteItemToCanvas];
        return;
    }
    
//    TutorialView *view = [[TutorialView alloc] initWithSuperView:self.view];
//    view.height = self.view.height - self.shopButton.height + 10;
//    [self.view insertSubview:view belowSubview:self.cameraButton.superview];
//    
//    [view setup];
//    
//    __weak typeof(self) aSelf = self;
//    view.closeBlock = ^{
//        [aSelf removeTutorialIfNeeded];
//    };
//    
//    self.navigationItem.leftBarButtonItem.enabled = NO;
//    self.navigationItem.rightBarButtonItem.enabled = NO;
//    
//    _shareButton.hidden = YES;
//    _favButton.enabled = NO;
//    _shopButton.enabled = NO;
// 
//    _tutorialView = view;
}
- (void)removeTutorialIfNeeded{
    if (_tutorialView){
        [_tutorialView removeFromSuperview];
        _tutorialView = nil;
        self.navigationItem.leftBarButtonItem.enabled = YES;
        self.navigationItem.rightBarButtonItem.enabled = YES;
        self.favButton.enabled = YES;
        self.shopButton.enabled = YES;
        _shareButton.hidden = NO;
        
        [self addFavoriteItemToCanvas];
    }
}

- (void)addFavoriteItemToCanvas{
    Cat *cat = [Cat favoriteCategory];
    Item *item = [[cat items] lastObject];
//    self.index = (int)[self.itemsArray indexOfObject:item];
//    if (![self.itemsArray containsObject:item]) {
        self.index = self.itemsArray.count;
  //  }
    [self.canvasView clearCanvas];
    [self.canvasView addItemsToCanvasFromFavoriteItem:item];
    _favoriteItem = item;
    [self updateItems];
}


//- (void)addFavoriteItemToCanvasWithIndex:(int)itemIndex{
//    Cat *cat = [Cat favoriteCategory];
//    Item *item = [[cat items] objectAtIndex:itemIndex];
//    self.index = (int)[self.itemsArray indexOfObject:item];
//    if (![self.itemsArray containsObject:item]) {
//        self.index = self.itemsArray.count - 1;
//    }
//    [self.canvasView clearCanvas];
//    [self.canvasView addItemsToCanvasFromFavoriteItem:item];
//    _favoriteItem = item;
//    [self updateItems];
//}


- (void)AddNewElementToTestCanvas{
   //

        [self saveElementWithImagesName:[NSArray arrayWithObjects:@"образ, 3-1 и 4-1.jpg",@"образ, 3-2.jpg",@"образ, 3.jpg", nil] andItemName:@"2"];
        [self saveElementWithImagesName:[NSArray arrayWithObjects:@"образ, 3-1 и 4-1.jpg",@"образ, 4-2.jpg",@"образ, 4.jpg", nil] andItemName:@"3"];
//        [self saveElementWithImagesName:[NSArray arrayWithObjects:@"образ, 2-1.jpg",@"образ, 1-2 и 2-2.jpg",@"образ, 2.jpg", nil] andItemName:@"3"];
        [self saveElementWithImagesName:[NSArray arrayWithObjects:@"образ, 1-1.jpg",@"образ, 1-2 и 2-2.jpg",@"образ, 1.jpg", nil] andItemName:@"4"];
 
  // [self saveElementWithImagesName:[NSArray arrayWithObjects:@"top4.jpg",@"bot2.jpg",@"ob2new.jpg", nil] andItemName:@"2"];
   //[self saveElementWithImagesName:[NSArray arrayWithObjects:@"_uJXWQeTbcY.jpg",@"BxAZ3rfBcrk.jpg",@"ob4.jpg", nil] andItemName:@"3"];
}


- (void)saveElementWithImagesName:(NSArray*)imageNameArray andItemName:(NSString*)itemName{
    NSArray *categoryName = [NSArray arrayWithObjects:@"Верхняя одежда", @"Нижняя одежда", nil];
    NSArray *nameItemArray = [NSArray arrayWithObjects:@"Рубашка", @"Брюки", nil];
    Item *first = nil;
    Item *second = nil;
    for (int i = 0; i < 2; i++) {
        Item *newItem = [Item MR_createEntity];
        newItem.uidValue = [Item freeUIDValue];
        NSString *imageURL = [AppHelper saveImage:[UIImage imageNamed:[imageNameArray objectAtIndex:i]]];
        newItem.image_url = imageURL;
        newItem.name = [NSString stringWithFormat:@"%@ %@",[nameItemArray objectAtIndex:i], itemName];
        NSArray *catArray = [Cat allButFavorite];
        for (Cat *curentCat in catArray) {
            if ([curentCat.name isEqualToString:[categoryName objectAtIndex:i]]){
                newItem.category = curentCat;
            }
        }
        if (first == nil) {
            first = newItem;
        } else {
            second = newItem;
        }
        [newItem.managedObjectContext MR_saveToPersistentStoreAndWait];
    }

    Item *itemToFavorit = [Item MR_createEntity];
    itemToFavorit.uidValue = [Item freeUIDValue];
    NSString *imageFavoriteURL = [AppHelper saveImage:[UIImage imageNamed:[imageNameArray objectAtIndex:2]]];
    itemToFavorit.image_url = imageFavoriteURL;
    itemToFavorit.name = itemName;
    itemToFavorit.category = [Cat favoriteCategory];
    
    CGFloat width = [[UIScreen mainScreen] applicationFrame].size.width;
    CGFloat koef = width / 320.0f;
    CGRect frameForTop = CGRectMake(0, 0, width, 205 * koef);
    CGRect frameForBottom = CGRectMake(0, 183 * koef, width, 219 * koef);
    
    itemToFavorit.favorite_components = @[@{@"frame" : [NSValue valueWithCGRect:frameForBottom], @"item_id" : second.uid},
                                  @{@"frame" : [NSValue valueWithCGRect:frameForTop], @"item_id" : first.uid}];
    
    [itemToFavorit.managedObjectContext MR_saveToPersistentStoreAndWait];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
//    [self check];
//    [self editTapped];
}

#pragma mark - My Delegate Method

-(void)sendToDestinationViewItem:(id)item
{
    _stateEditing = !_stateEditing;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Изменить" style:UIBarButtonItemStyleBordered target:self action:@selector(editTapped)];
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemWithImage:[UIImage imageNamed:@"wardrobe"] target:self action:@selector(wardrobeTapped)];
    [self.canvasView cancelEditing];
    [self.canvasView removeState];
    [self performSegueWithIdentifier:@"buyForm" sender:item];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"buyForm"]) {
        BuyFormVC *vc = segue.destinationViewController;
        vc.item = (Item*)sender;
    }
}

@end