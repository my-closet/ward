//
//  FilterVC.h
//  wardrobe
//
//  Created by Oleg Mytsouda on 07.03.16.
//  Copyright © 2016 Farcom. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Shop.h"
#import "ShopCategory.h"

@protocol FiltersDelegate <NSObject>

- (void)returnFiltersValue:(NSMutableArray *)filters;

@end

@interface FilterVC : UIViewController

@property (strong, nonatomic) Shop *shop;
@property (strong, nonatomic) ShopCategory *category;

@property (assign, nonatomic) id<FiltersDelegate> delegate;

@end
