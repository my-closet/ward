
#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"
@interface MainCell : SWTableViewCell
@property (nonatomic, weak) IBOutlet UIImageView *imgView;;
- (void)setupWithItem:(Item*)item shopCategory:(ShopCategory*)shopCategory;
- (void)setupWithItem:(Item*)item mode:(BOOL)onlySelection category:(ShopCategory*)shopCategory;
- (void)setPriceArray:(NSMutableArray*)newArr;
- (void)setupWithShop:(id)shop;
- (void)setupWithShopCategory:(ShopCategory*)shopCategory shop:(Shop*)shop;
- (void)addAddButton;
@property (nonatomic, weak) IBOutlet UILabel *priceLabel;
@end
