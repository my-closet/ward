//
//  CollectionShopItem.m
//  wardrobe
//
//  Created by Oleg Mytsouda on 18.01.16.
//  Copyright © 2016 Farcom. All rights reserved.
//

#import "CollectionShopItem.h"

@implementation CollectionShopItem

-(void)awakeFromNib
{
    [self.contentView.layer setBorderColor:[UIColor colorWithRed:0/255.f green:0/255.f blue:0/255.f alpha:0.2f].CGColor];
    [self.contentView.layer setBorderWidth:0.3f];
}

- (void)loadShopImageWithURL:(NSURL*)url{
    [self.avatarImageView sd_setImageWithURL:url completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (image){
            self.avatarImageView.image = image;
            [self.avatarImageView setNeedsDisplay];
        } else {
            UIImage *image = [UIImage imageWithColor:RGB(255, 230, 230) radius:40 rect:CGRectMake(0, 0, 80, 80)];
            self.avatarImageView.image = image;
            [self.avatarImageView setNeedsDisplay];
        }
    }];
}

- (void)loadLocalImageWithItem:(Item*)item{
    __weak typeof(self) aSelf = self;
    __block NSString *url = [item.image_url copy];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        UIImage *image = [[AppHelper imageFromUUIDString:url] roundedImage];
        dispatch_after_short(0, ^{
            aSelf.avatarImageView.image = image;
            [aSelf.avatarImageView setNeedsDisplay];
        });
    });
}

@end
