
#import "WardrobeCell.h"

@implementation WardrobeCell

- (void)awakeFromNib {
    self.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.layer.borderWidth = ONE_PIXEL;
    self.layer.cornerRadius = 3.0f;
    self.clipsToBounds = YES;
}

@end
