//
//  TRLoadingViewController.m
//  Tyros
//
//  Created by Olegek on 30.06.14.
//  Copyright (c) 2014 The Tyros LLC. All rights reserved.
//

#import "LoadingVC.h"
#import "AppDelegate.h"

@interface LoadingVC () <UIApplicationDelegate>

@end

@implementation LoadingVC


+ (LoadingVC*) instance
{
    static LoadingVC* instance = nil;
    if (instance == nil) {
        instance = [[LoadingVC alloc] initWithNibName:@"LoadingVC" bundle:nil];
    }
    return instance;
}


- (void)showWithText:(NSString*)text
{
    [self showInView:[(AppDelegate *)[[UIApplication sharedApplication] delegate] topViewController].view withText:text];
}


- (void)showInView:(UIView*)view withText:(NSString*)text
{
    if (self.view.superview != nil) {
        [self hide];
    }
    CGRect newFrame = view.bounds;
    newFrame.origin.y = 0;
    self.view.frame = newFrame;
    [view addSubview:self.view];
    self.loadingLabel.text = text;
}


- (void)hide
{
    [self.view removeFromSuperview];
}


@end
