#import "PreviewVC.h"
#import "MWPhotoBrowser.h"
#import "CanvasView.h"
#import "ItemEditController.h"
#import "IDMPhotoBrowser.h"
#import "PECropViewController.h"
#import "MainViewController.h"
#import "LoadingVC.h"
#import <UIImageView+AFNetworking.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "BuyFormVC.h"
#import "PureLayout.h"
#import "CanvasItem.h"

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)

@interface PreviewVC ()<IDMPhotoBrowserDelegate, PECropViewControllerDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topImageViewConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftSideConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rightSideConstraint;

@property (weak, nonatomic) IBOutlet UIView *oldPriceView;
@property (weak, nonatomic) IBOutlet UIView *activityView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (weak, nonatomic) IBOutlet UIView *bottomBarView;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *oldPriceLabel;
@property (weak, nonatomic) IBOutlet UIButton *buyButton;
@property (weak, nonatomic) IBOutlet UIButton *addToMeButton;
@property (weak, nonatomic) IBOutlet UIButton *choseButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *countItem;

@property (strong, nonatomic) IBOutlet UIView *priceView;


@property (nonatomic, strong) CanvasView *canvasView;

@property (strong, nonatomic) Item *selectedItem;

- (IBAction)buyButtonTapped:(id)sender;
- (IBAction)addToMeButtonTapped:(id)sender;
- (IBAction)choseButtonTapped:(id)sender;
- (IBAction)countItemAction:(id)sender;

@property (strong, nonatomic) NSMutableArray *imageUrlArray;

@property (strong, nonatomic) UIImage *productImage;

@property (assign, nonatomic) int height;
@property (assign, nonatomic) int widht;
@property (assign, nonatomic) int top;

@end

@implementation PreviewVC
{
    BOOL _stateEditing;
}
//@synthesize newImage = _newImage;

//-(void)setProductImage:(UIImage *)productImage{
//    if (productImage == nil) {
//        NSLog(@"not work %d",self.index);
//    } else {
//        NSLog(@"work fine %d",self.index);
//    }
////     NSLog(@"work fine %d",self.index);
////   // self.productImage = productImage;
//}

//- (void) setNewImage:(UIImage *)newImage{
//    NSLog(@"Setting name to: %@", newImage);
//}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


- (void)viewDidLoad {
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"cantEditing"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [SVProgressHUD dismiss];
    [super viewDidLoad];
    
    self.title = self.shopCategory.name;
    
    self.countItem.title = @"Изменить";

    self.canvasView = [[CanvasView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, self.view.height - 110)];
    self.canvasView.ifNeedToShow = YES;
    
   // self.canvasView.delegate = self;
    
    self.canvasView.backgroundColor  = [UIColor clearColor];
    
    
    
    [self.view insertSubview:self.canvasView atIndex:1];
    
    [self.canvasView autoSetDimensionsToSize:self.canvasView.frame.size];
    [self.canvasView autoPinEdgeToSuperviewEdge:ALEdgeLeading];
    [self.canvasView autoPinEdgeToSuperviewEdge:ALEdgeTop];
    
    [self.canvasView setup];
    
    [self setupView];
    [self setupDesign];
 //   [self parseElementToArrays:self.photos];
    [self showInformInView];
    [self setBackgroundImage];
}

- (void)setBackgroundImage
{
    Cat *favoriteCategory = [Cat favoriteCategory];
    Cat *cat = [favoriteCategory MR_inContext:[NSManagedObjectContext MR_defaultContext]];
    int currentImageIndex = [[NSUserDefaults standardUserDefaults] integerForKey:@"mainItemIndex"];
    if ([[[cat items] array] count] > 0) {
        Item *itemForBacgroundImage = [[[cat items] array] objectAtIndex:currentImageIndex];
        UIImage *image = [AppHelper imageFromUUIDString:itemForBacgroundImage.image_url];
        if (!self.isMy) {
            self.backgroundImage.image = image;
        }
    }

//    int width = self.backgroundImage.frame.size.width;
//    CGFloat heightShouldBe = (width / image.size.width) * image.size.height;
//    
//    heightShouldBe = ceilf(MIN(width * 1.1f, heightShouldBe));
//    
//    if (heightShouldBe > 0 && heightShouldBe < 10000){
//        self.backgroundImage.frame.size.height = (float)heightShouldBe;
//    } else {
//        self.height = 300.0f;
//    }
    
}

- (void)setupView{
    if (self.photos.count > 1) {
        self.navigationItem.title = self.navBarTitle;
        UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc]
                                               initWithTarget:self
                                               action:@selector(didSwipeLeft)];
        swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
        [self.view addGestureRecognizer:swipeLeft];
        
        UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc]
                                                initWithTarget:self
                                                action:@selector(didSwipeRight)];
        swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
        [self.view addGestureRecognizer:swipeRight];
    }
   // self.imageUrlArray = [NSMutableArray new];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(bacToPreview) name:@"savePreviewElenetToMe" object:nil];
    if (IS_IPHONE_4_OR_LESS) {
        self.top = self.topImageViewConstraint.constant - 10;
        self.height = 270;
        self.widht = 240;
    } else if(IS_IPHONE_5){
        self.top = self.topImageViewConstraint.constant;
        self.height = 332;
        self.widht = 270;
    } else if(IS_IPHONE_6 ){
        self.top = self.topImageViewConstraint.constant;
        self.height = 425;
        self.widht = 345;
    } else if (IS_IPHONE_6P){
        self.top = self.topImageViewConstraint.constant;
        self.height = 510;
        self.widht = 414;
    }
    self.activityView.layer.cornerRadius = 14.f;
    [self.view sendSubviewToBack:self.activityView];
    
//    [RACObserve(self, productImage) subscribeNext:^(UIImage *newImage) {
//        if (newImage != nil) {
//            self.activityView.hidden = NO;
//            if (self.isMy) {
//                [self layoutMineImage:newImage];
//            } else {
//                [self layoutViewConstraintWithImageMetadata:newImage];
//            }
//        }
//    }];
}


- (void)layoutMineImage:(UIImage *)newImage{
    float koef, topH;
    self.imageViewHeightConstraint.constant = newImage.size.height;
    self.imageViewWidthConstraint.constant = newImage.size.width;
    self.leftSideConstraint.constant = (self.widht - newImage.size.width)/2;
    self.rightSideConstraint.constant = (self.widht - newImage.size.width)/2;
    
    if (newImage.size.width > self.widht){
        koef = (newImage.size.width/self.widht);
        int currentWidth = newImage.size.width/koef;
        self.imageViewWidthConstraint.constant = currentWidth;
        self.leftSideConstraint.constant = (self.widht - currentWidth)/2;
        self.rightSideConstraint.constant = (self.widht - currentWidth)/2;
        int currentHeight = newImage.size.height/koef;
        self.imageViewHeightConstraint.constant = currentHeight;
    //    self.topImageViewConstraint.constant = self.top + ((self.height - currentHeight)/2);
        topH = self.topImageViewConstraint.constant;
    } if (newImage.size.height > self.height) {
        koef = (newImage.size.height/self.height);
        int currentWidth = newImage.size.width/koef;
        self.imageViewWidthConstraint.constant = currentWidth;
        int currentHeight = newImage.size.height/koef;
        if (self.imageViewHeightConstraint.constant > self.height) {
            self.imageViewHeightConstraint.constant = currentHeight;
            self.leftSideConstraint.constant = (self.widht - currentWidth)/2;
            self.rightSideConstraint.constant = (self.widht - currentWidth)/2;
            self.topImageViewConstraint.constant = self.top + ((self.height - currentHeight)/2);
        } else {
            //self.topImageViewConstraint.constant = topH;
        }
    }
 //   self.topImageViewConstraint.constant = self.top + ((self.height - currentHeight)/2);

    [self.view layoutIfNeeded];
}


- (void)layoutViewConstraintWithImageMetadata:(UIImage *)newImage{
    float koef;
    int currentHeight;
    if (newImage.size.height > self.height) {
        koef = (newImage.size.height/self.height);
        int currentWidth = newImage.size.width/koef;
        self.imageViewWidthConstraint.constant = currentWidth;
        self.leftSideConstraint.constant = (self.widht - currentWidth)/2;
        self.rightSideConstraint.constant = (self.widht - currentWidth)/2;
        currentHeight = newImage.size.height/koef;
        self.imageViewHeightConstraint.constant = currentHeight;
        self.topImageViewConstraint.constant = self.top + ((self.height - currentHeight)/2);
    }
    if (newImage.size.width > self.widht){
        koef = (newImage.size.width/self.widht);
        int currentWidth = newImage.size.width/koef;
        self.imageViewWidthConstraint.constant = currentWidth;
        self.leftSideConstraint.constant = (self.widht - currentWidth)/2;
        self.rightSideConstraint.constant = (self.widht - currentWidth)/2;
        self.imageViewHeightConstraint.constant = currentHeight;
        self.topImageViewConstraint.constant = self.top + ((self.height - currentHeight)/2);
    }
//    else {
//        self.imageViewHeightConstraint.constant = newImage.size.height;
//        self.imageViewWidthConstraint.constant = newImage.size.width;
//    }
    [self.view layoutIfNeeded];
}

- (UIImage*)resizeImage:(UIImage*)image withWidth:(CGFloat)width withHeight:(CGFloat)height
{
    CGSize newSize = CGSizeMake(width, height);
    CGFloat widthRatio = newSize.width/image.size.width;
    CGFloat heightRatio = newSize.height/image.size.height;
    if (image.size.width < newSize.width)
    {
        widthRatio = 1.0;
    }
    
    if (image.size.height < newSize.height)
    {
        heightRatio = 1.0;
    }
    
    
    if(widthRatio > heightRatio)
    {
        newSize=CGSizeMake(image.size.width*heightRatio,image.size.height*heightRatio);
    }
    else
    {
        newSize=CGSizeMake(image.size.width*widthRatio,image.size.height*widthRatio);
    }
    
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

-(void)setupDesign{
    self.buyButton.hidden = self.isMy? YES : NO;
    self.addToMeButton.hidden = self.isMy? YES : NO;
    self.oldPriceView.hidden = self.isMy? YES : NO;
    self.priceLabel.hidden = self.isMy? YES : NO;
}


- (void)bacToPreview{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)parseElementToArrays:(NSMutableArray*)itemsArray{
    for (Item* model in itemsArray) {
        if (model.imageURL != nil) {
            [self.imageUrlArray addObject:model.imageURL];
        } else {
            if (model.image_url == nil) {
                model.image_url = @"";
            }
            [self.imageUrlArray addObject:model.image_url];
        }
    }

}


- (void)showInformInView{
    
    if (self.isMy) {
        self.priceView.hidden = YES;
    }
        
    [self.canvasView clearCanvas];
    [self.canvasView addItemToCanvas:[self.photos objectAtIndex:self.index]];
    [self.canvasView cancelEditing];
    self.imageView.hidden = YES;
    [self updateCountElementWithIndex:self.index];
    
//    self.imageView.image = nil;
//    dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
//    dispatch_async(q, ^{
//        UIImage *newImage;
//    
//        NSString *url;
//        if (![[[self.photos objectAtIndex:self.index]valueForKey:@"image_url"] hasPrefix:@"http://"] && ![[[self.photos objectAtIndex:self.index]valueForKey:@"image_url"] hasPrefix:@"https://"]) {
//            NSString *stringFromURL = [NSString stringWithFormat:@"%@%@",DOMAIN_URL,[[self.photos objectAtIndex:self.index]valueForKey:@"image_url"]];
//            url = stringFromURL;
//        } else {
//            url = [NSString stringWithFormat:@"%@",[self.imageUrlArray objectAtIndex:self.index]];
//        }
//        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
//        newImage = [UIImage imageWithData:imageData];
//        dispatch_async(dispatch_get_main_queue(), ^{
//            self.imageView.image = [self resizeImage:newImage withWidth:self.widht withHeight:self.height];
//        });
//        if (imageData == nil) {
//            NSString *url = [[self.photos objectAtIndex:self.index]valueForKey:@"image_url"];
//            UIImage *image = [AppHelper imageFromUUIDString:url];
//            dispatch_async(dispatch_get_main_queue(), ^{
//                self.imageView.image = [self resizeImage:image withWidth:self.widht withHeight:self.height];
//            });
//        }
//        
//    });
//    [self updateCountElementWithIndex:self.index];
}


-(void)updateCountElementWithIndex:(int)index{
    if (!self.isMy) {
        self.priceLabel.text = [NSString stringWithFormat:@"%@ PУБ", [self formatWithThousandSeparator:[[[self.photos objectAtIndex:index] valueForKey:@"price"] intValue]]];
        self.oldPriceLabel.text = [[self.photos objectAtIndex:index] valueForKey:@"shop_name"];
        [self.canvasView addSubview:self.priceView];
        [self.canvasView bringSubviewToFront:self.priceView];
    } else {
        self.oldPriceView.hidden = YES;
    }
}


//-(void)updateCountElementWithIndex:(int)index{
//    self.countItem.title = @"Изменить";
//    self.priceLabel.text = [NSString stringWithFormat:@"%@ PУБ", [self formatWithThousandSeparator:[[self.priceArray objectAtIndex:index] intValue]]];
//    if (!self.isMy) {
//        if (![[self.oldPriceArray objectAtIndex:index] isEqualToString:@"empty"]) {
//            self.oldPriceView.hidden = NO;
//            self.oldPriceLabel.text = [NSString stringWithFormat:@"%@ PУБ", [self formatWithThousandSeparator:[[self.oldPriceArray objectAtIndex:index] intValue]]];
//        } else {
//            self.oldPriceView.hidden = YES;
//        }
//    }
//}


-(NSString*)formatWithThousandSeparator:(NSInteger)number
{
    NSNumberFormatter* numberFormatter = [[NSNumberFormatter alloc] init];
    numberFormatter.usesGroupingSeparator = YES;
    numberFormatter.groupingSeparator = @" ";
    numberFormatter.groupingSize = 3;
    NSString *result = [numberFormatter stringFromNumber:[NSNumber numberWithInteger:number]];
    return result;
}


- (void)didSwipeLeft{
    self.countItem.title = @"Изменить";
    self.index +=1;
    if (self.index == self.photos.count) {
        self.index = 0;
    }
   [self updateCountElementWithIndex:self.index];
    
    CanvasItem *currentItem = [[self.canvasView canvasItemsArray] lastObject];
    self.canvasView.currentFrame = currentItem.frame;
    
    [self.canvasView clearCanvas];
    [self.canvasView addItemToCanvas:[self.photos objectAtIndex:self.index]];
    [self.canvasView cancelEditing];
    
//    self.imageView.image = nil;
//    dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
//    dispatch_async(q, ^{
//        UIImage *newImage;
//        
//        NSString *url;
//        if (![[[self.photos objectAtIndex:self.index]valueForKey:@"image_url"] hasPrefix:@"http://"] && ![[[self.photos objectAtIndex:self.index]valueForKey:@"image_url"] hasPrefix:@"https://"] ) {
//            NSString *stringFromURL = [NSString stringWithFormat:@"%@%@",DOMAIN_URL,[[self.photos objectAtIndex:self.index]valueForKey:@"image_url"]];
//            url = stringFromURL;
//        } else {
//            url = [NSString stringWithFormat:@"%@",[self.imageUrlArray objectAtIndex:self.index]];
//        }
//        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
//        newImage = [UIImage imageWithData:imageData];
//        dispatch_async(dispatch_get_main_queue(), ^{
//            self.imageView.image = [self resizeImage:newImage withWidth:self.widht withHeight:self.height];
//        });
//        if (imageData == nil) {
//            NSString *url = [[self.photos objectAtIndex:self.index]valueForKey:@"image_url"];
//            UIImage *image = [AppHelper imageFromUUIDString:url];
//            dispatch_async(dispatch_get_main_queue(), ^{
//                self.imageView.image = [self resizeImage:image withWidth:self.widht withHeight:self.height];
//            });
//        }
//        
//    });
//
//    self.productImage = self.imageView.image;
//    CATransition *animation = [CATransition animation];
//    [animation setDuration:0.3];
//    [animation setType:kCATransitionPush];
//    [animation setSubtype:kCATransitionFromRight];
//    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
//    [[self.imageView layer] addAnimation:animation forKey:nil];
}


- (void)loadLocalImageWithItem:(Item*)item{
    __weak typeof(self) aSelf = self;
    __block NSString *url = [item.image_url copy];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        UIImage *image = [AppHelper imageFromUUIDString:url];
        dispatch_after_short(0, ^{
            aSelf.imageView.image = image;
            aSelf.productImage = image;
            if (image == nil) {
                self.activityView.hidden = YES;
            }
            [aSelf.imageView setNeedsDisplay];
        });
    });
}


- (void)didSwipeRight{
    self.countItem.title = @"Изменить";
    self.index -=1;
    if (self.index == -1) {
        self.index = (int)self.photos.count-1;
    }
    
    [self.canvasView clearCanvas];
    [self.canvasView addItemToCanvas:[self.photos objectAtIndex:self.index]];
    [self.canvasView cancelEditing];
    [self updateCountElementWithIndex:self.index];
    
//    self.imageView.image = nil;
//    dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
//    dispatch_async(q, ^{
//        UIImage *newImage;
//        
//        NSString *url;
//        if (![[[self.photos objectAtIndex:self.index]valueForKey:@"image_url"] hasPrefix:@"http://"] && ![[[self.photos objectAtIndex:self.index]valueForKey:@"image_url"] hasPrefix:@"https://"] ) {
//            NSString *stringFromURL = [NSString stringWithFormat:@"%@%@",DOMAIN_URL,[[self.photos objectAtIndex:self.index]valueForKey:@"image_url"]];
//            url = stringFromURL;
//        } else {
//            url = [NSString stringWithFormat:@"%@",[self.imageUrlArray objectAtIndex:self.index]];
//        }
//        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
//        newImage = [UIImage imageWithData:imageData];
//        dispatch_async(dispatch_get_main_queue(), ^{
//            self.imageView.image = [self resizeImage:newImage withWidth:self.widht withHeight:self.height];
//        });
//        if (imageData == nil) {
//            NSString *url = [[self.photos objectAtIndex:self.index]valueForKey:@"image_url"];
//            UIImage *image = [AppHelper imageFromUUIDString:url];
//            dispatch_async(dispatch_get_main_queue(), ^{
//                self.imageView.image = [self resizeImage:image withWidth:self.widht withHeight:self.height];
//            });
//        }
//        
//    });
  //    [self.imageView setImageWithURL:[self.imageUrlArray objectAtIndex:self.index] placeholderImage:nil];
//    if (self.imageView.image == nil && self.isMy) {
//        [self loadLocalImageWithItem:[self.photos objectAtIndex:self.index]];
//    }
//    UIImage *currentImage = self.imageView.image;
//    self.imageView.image = nil;
//    self.imageView.image = [self resizeImage:currentImage withWidth:self.widht withHeight:self.height];
//    self.productImage = self.imageView.image;
//    UIImage *newImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:[self.imageUrlArray objectAtIndex:self.index]]];
//    if (newImage == nil) {
//        [self loadLocalImageWithItem:[self.photos objectAtIndex:self.index]];
//    } else {
//        self.imageView.image = newImage;
//    }
//    CATransition *animation = [CATransition animation];
//    [animation setDuration:0.3];
//    [animation setType:kCATransitionPush];
//    [animation setSubtype:kCATransitionFromLeft];
//    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut]];
//    [[self.imageView layer] addAnimation:animation forKey:nil];
   // self.imageView.contentMode=UIViewContentModeScaleToFill;
}


#pragma mark - Actions


- (IBAction)buyButtonTapped:(id)sender{
    [self performSegueWithIdentifier:@"buyForm" sender:[self.photos objectAtIndex:self.index]];
//    
//    if (self.productUrlArray.count > 0) {
//        NSString *productUrl = [self.productUrlArray objectAtIndex:self.index];
//        if (![productUrl isEqualToString:@"empty"]) {
//            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:productUrl]];
//        } else {
//            [self showSorryAlert];
//        }
//    } else {
//        [self showSorryAlert];
//        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://admin.mycloset.ru/api.php?action=getProductsByCategoryIdAndShopId&category_id=%d&shop_id=%d",self.shopCategory.uidValue,self.shop.uidValue]];
//        NSURLRequest *request = [NSURLRequest requestWithURL:url];
//        NSURLResponse *response;
//        NSError *error;
//        NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
//        NSDictionary *jsonObject=[NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:nil];
//        if(!error)
//        {
//            jsonObject = [jsonObject valueForKey:@"results"];
//            self.productUrlArray = [NSMutableArray new];
//            for (NSArray *arr in jsonObject){
//                if (![[arr valueForKey:@"url"] isEqual:[NSNull null]]) {
//                    [self.productUrlArray addObject:[arr valueForKey:@"url"]];
//                }
//            }
//            if (self.productUrlArray.count > 0) {
//                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[self.productUrlArray objectAtIndex:self.index]]];
//            }else{
//                [[[UIAlertView alloc] initWithTitle:@"Ошибка" message:@"Извините на даний момент у нас нет ссылки на эту вещь." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
//            }
//        }
//    }
}


-(void)showSorryAlert{
    [[[UIAlertView alloc] initWithTitle:@"Ошибка" message:@"Извините на даний момент у нас нет ссылки на эту вещь." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
}


- (void)getUrlArray{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://admin.mycloset.ru/api.php?action=getProductsByCategoryIdAndShopId&category_id=%d&shop_id=%d",self.shopCategory.uidValue,self.shop.uidValue]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLResponse *response;
    NSError *error;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSDictionary *jsonObject=[NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:nil];
    if(!error)
    {
        jsonObject = [jsonObject valueForKey:@"results"];
        self.productUrlArray = [NSMutableArray new];
        for (NSArray *arr in jsonObject){
            if (![[arr valueForKey:@"url"] isEqual:[NSNull null]]) {
                [self.productUrlArray addObject:[arr valueForKey:@"url"]];
            } else if (self.productUrlArray.count > 0){
                [self.productUrlArray addObject:@"empty"];
            }
            
            if (![[arr valueForKey:@"oldprice"] isEqual:[NSNull null]]) {
                [self.oldPriceArray addObject:[arr valueForKey:@"oldprice"]];
            } else {
                [self.oldPriceArray addObject:@"empty"];
            }
            
        }
        [SVProgressHUD dismiss];
    }
}



- (IBAction)addToMeButtonTapped:(id)sender {
    UIStoryboard *s = [UIStoryboard storyboardWithName:@"main" bundle:nil];
    ItemEditController *c = [s instantiateViewControllerWithIdentifier:NSStringFromClass([ItemEditController class])];
    c.item = [self.photos objectAtIndex:self.index];
    
    __block Item *itemForBlock = c.item;
    
    dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(q, ^{
        UIImage *newImage;
        
        NSString *url;
        if (![itemForBlock.image_url hasPrefix:@"http://"] && ![itemForBlock.image_url hasPrefix:@"https://"]) {
            NSString *stringFromURL = [NSString stringWithFormat:@"%@%@",DOMAIN_URL,itemForBlock.image_url];
            url = stringFromURL;
        } else {
            url = [NSString stringWithFormat:@"%@",itemForBlock.image_url];
        }
        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
        newImage = [UIImage imageWithData:imageData];
        dispatch_async(dispatch_get_main_queue(), ^{
            c.image = newImage;
            c.savedBlock = nil;
            c.targetCategory = nil;
            [self.navigationController pushViewController:c animated:YES];
        });
    });
}


- (IBAction)choseButtonTapped:(id)sender {
    Item *item = [self.photos objectAtIndex:self.index];
    
   __block UIImage *image = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:[[item imageURL] absoluteString]];
    if (image == nil) {
        UIImage *newImage = [AppHelper imageFromUUIDString:item.image_url];
            image = newImage;
    }
    if (image == nil){
        
    
        dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(q, ^{
            UIImage *newImage;
            
            NSString *url;
            if (![item.image_url hasPrefix:@"http://"] && ![item.image_url hasPrefix:@"https://"]) {
                NSString *stringFromURL = [NSString stringWithFormat:@"%@%@",DOMAIN_URL,item.image_url];
                url = stringFromURL;
            } else {
                url = [NSString stringWithFormat:@"%@",item.image_url];
            }
            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
            newImage = [UIImage imageWithData:imageData];
            dispatch_async(dispatch_get_main_queue(), ^{
                image = newImage;
                [self addToMainScreenWithImage:image andItem:item];
            });
            if (imageData == nil) {
                NSString *url = [[self.photos objectAtIndex:self.index]valueForKey:@"image_url"];
                UIImage *imageD = [AppHelper imageFromUUIDString:url];
                dispatch_async(dispatch_get_main_queue(), ^{
                    image = imageD;
                    [self addToMainScreenWithImage:image andItem:item];
                });
            }
            
        });
    }
    [self addToMainScreenWithImage:image andItem:item];
}


- (void)addToMainScreenWithImage:(UIImage*)image andItem:(Item*)item
{
    if (!image){
        [[[UIAlertView alloc] initWithTitle:nil
                                    message:@"Нельзя добавить вещь, потому что ее изображение отсутствует" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        return;
    }
    PECropViewController *controller = [[PECropViewController alloc] init];
    controller.delegate = self;
    controller.image = image;
    controller.toolbarHidden = YES;
    controller.title = @"Выберите область";
    self.selectedItem = item;
    
    if (self.isMy) {
        [controller performSelector:@selector(done:) withObject:nil];
        //[self.navigationController pushViewController:controller animated:YES];
    } else {
        UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:controller];
        [AppHelper setupNavigationController:nc];
        [self presentViewController:nc animated:YES completion:nil];
    }
}


- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage{
    ItemEditController *editController = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([ItemEditController class])];
    editController.image = croppedImage;
    editController.shopItem = self.selectedItem;
    editController.targetCategory = self.currentCategory;
    
    if (!self.isMy) {
        [controller.navigationController pushViewController:editController animated:YES];
        NSString *shopingURl = self.productUrlArray.count > 0 ? [self.productUrlArray objectAtIndex:self.index] : @"empty";
        [[NSUserDefaults standardUserDefaults] setObject:shopingURl forKey:@"productUrl"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFromShop"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    MainViewController *vController;
    if (self.isMy) {
        vController = (MainViewController*)self.navigationController.viewControllers.firstObject;
    } else {
        UINavigationController *root = (UINavigationController*)ApplicationDelegate.window.rootViewController;
        vController = [[root viewControllers] firstObject];
    }
    if ([vController isKindOfClass:[MainViewController class]])
    {
        editController.savedBlock = [vController itemAddToCanvasBlock];
        if (self.isMy){
            if (self.selectedItem.favorite_components != nil) {
                //[vController.canvasView clearCanvas];
               // [vController addFavoriteItemToCanvasWithIndex:self.index];
               // [vController addFavoriteItemToCanvasWithIndex:self.index];
                vController.swipe = YES;
                vController.index = self.index - 1;
                [vController didSwipeLeft];
            } else {
                [vController.canvasView addItemToCanvas:self.selectedItem];
            }
            [vController editTapped];
            self.selectedItem = nil;
        } else {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"needToSave"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
    if (self.isMy) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}


- (void)cropViewControllerDidCancel:(PECropViewController *)controller{
    _selectedItem = nil;
    [controller dismissViewControllerAnimated:YES completion:nil];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"buyForm"]) {
        BuyFormVC *vc = segue.destinationViewController;
        vc.item = (Item*)sender;
        vc.senderEmail = self.shop.email;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (IBAction)countItemAction:(id)sender
{
    _stateEditing = !_stateEditing;
    if (_stateEditing){
        [self.canvasView enableEditing];
        [self.canvasView saveState];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Готово" style:UIBarButtonItemStyleBordered target:self action:@selector(countItemAction:)];
//        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Отменить" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelTapped)];
    } else {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Изменить" style:UIBarButtonItemStyleBordered target:self action:@selector(countItemAction:)];
        [self.canvasView cancelEditing];
        [self.canvasView removeState];
    }

}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"cantEditing"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
