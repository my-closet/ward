//
//  CollectionShop.h
//  wardrobe
//
//  Created by Oleg Mytsouda on 11.04.16.
//  Copyright © 2016 Farcom. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, kShopMode){
    kShopModeShops,
    kShopModeCategories,
    kShopModeClothes,
};

@class Shop, ShopCategory;

@interface CollectionShop : UIViewController

@property (nonatomic, assign) kShopMode mode;
@property (nonatomic, strong) Shop *shop;
@property (nonatomic, strong) ShopCategory *shopCategory;

@end
