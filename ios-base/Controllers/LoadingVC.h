//
//  TRLoadingViewController.h
//  Tyros
//
//  Created by Olegek on 30.06.14.
//  Copyright (c) 2014 The Tyros LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadingVC : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *loadingLabel;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

+ (LoadingVC*) instance;

- (void)showWithText:(NSString*)text;
- (void)showInView:(UIView*)view withText:(NSString*)text;
- (void)hide;

@end
