
#import <UIKit/UIKit.h>

@interface ImagePreviewController : UIViewController
@property (nonatomic, strong) UIImage *image;
@end
