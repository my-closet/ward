//
//  ClotheCell.h
//  wardrobe
//
//  Created by Oleg Mytsouda on 26.02.16.
//  Copyright © 2016 Farcom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClotheCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *filtersLabel;
@property (strong, nonatomic) IBOutlet UIImageView *checkImage;

@property (strong, nonatomic) NSString *elementID;

@end
