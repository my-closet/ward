
#import <UIKit/UIKit.h>

@protocol WardrobeCellDelegate <NSObject>

- (void)wardrobeCellDidDelete:(id)cell;

@end

@interface WardrobeCell : UICollectionViewCell
- (void)setupWithCategory:(Cat*)category;
@property (nonatomic, assign) BOOL editing;

@property (nonatomic, weak) id <WardrobeCellDelegate> delegate;


@end
