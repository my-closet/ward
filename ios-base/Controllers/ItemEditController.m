
#import "ItemEditController.h"

@interface ItemEditController ()<UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UIAlertViewDelegate>
@property (nonatomic, weak) IBOutlet UITextField *nameTextField;
@property (nonatomic, weak) IBOutlet UIPickerView *pickerView;
@property (nonatomic, weak) IBOutlet UIView *bottomView;
@property (nonatomic, weak) IBOutlet UILabel *statusLabel;
@property (nonatomic, weak) IBOutlet UIButton *addCategoryButton;
@property (nonatomic, strong) NSMutableArray *allCategories;
- (IBAction) addCategoryTapped;
@end

@implementation ItemEditController
{
    Cat *_selectedCategory;
}
- (void)dealloc
{
    self.savedBlock = nil;
}
- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Сохранить" style:UIBarButtonItemStylePlain target:self action:@selector(saveTapped)];
    
    self.allCategories = [NSMutableArray arrayWithArray:[Cat allButFavorite]];
    
    [self.pickerView reloadAllComponents];
    
    if (self.allCategories.count == 0){
        [AppHelper regenerateDataBaseIfNeeded];
    } else {
        [self.pickerView selectRow:0 inComponent:0 animated:NO];
        [self pickerView:_pickerView didSelectRow:0 inComponent:0];
    }
    
    if (self.shopItem.name.length > 0){
        self.nameTextField.text = self.shopItem.name;
    }
}

- (void)saveTapped {
    NSString *msg = @"";

    if (_selectedCategory){
        
        self.nameTextField.text = [Item freeItemNameForCategory:_selectedCategory.name];
    }
    
    if (_selectedCategory == nil)
    {
        msg = [msg stringByAppendingString:@"\nВыберите категорию"];
    }
    
    if (msg.length > 0){
        [[[UIAlertView alloc] initWithTitle:nil
                                    message:msg
                                   delegate:nil
                          cancelButtonTitle:@"ОК"
                          otherButtonTitles:nil] show];
        return;
    }
    
    if (self.item == nil && self.image != nil)
    {
        self.item = [Item MR_createEntity];
        self.item.uidValue = [Item freeUIDValue];
    }
    
  // хуйню зробив
//   self.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:self.item.imageURL]];
   
    
    if (self.item.image_url == nil){
        NSString *imageURL = [AppHelper saveImage:self.image];
        self.item.image_url = imageURL;
    }
    
    if (_shopItem){
        _item.priceValue = _shopItem.priceValue;
        _item.shop_name = _shopItem.shop_name;
        _item.shop_uidValue = _shopItem.shop_uidValue;
        _item.currency = _shopItem.currency;
        _item.uid = _shopItem.uid;
        _item.name = _shopItem.name;
    }
    
    self.item.name = [self.nameTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    self.item.category = _selectedCategory;
    [self.item.managedObjectContext MR_saveToPersistentStoreAndWait];
    BLOCK_SAFE_RUN(self.savedBlock,self.item);
    self.savedBlock = nil;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"savePreviewElenetToMe" object:nil];
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void) addCategoryTapped
{
    [self dismissKeyboard];
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Введите название категории" message:nil delegate:self cancelButtonTitle:@"Отмена" otherButtonTitles:@"Создать", nil];
    alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alertView show];    
}
- (void)dismissKeyboard{
    [self.nameTextField becomeFirstResponder];
    [self.nameTextField resignFirstResponder];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self dismissKeyboard];
}
#pragma mark - Picker View Data source
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
  return _allCategories.count;
}
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    Cat *cat = [self.allCategories objectAtIndex:row];
    return cat.name;
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    Cat *cat = [self.allCategories objectAtIndex:row];
    [self setupStatusLabelWithText:cat.name];
    _selectedCategory = cat;
}

#pragma mark - Alert View Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"Создать"])
    {
        UITextField *tField = [alertView textFieldAtIndex:0];
        if ([tField.text stringByReplacingOccurrencesOfString:@" "
                                                   withString:@""].length > 0)
        {
            Cat *category = [Cat MR_createEntity];
            category.name = tField.text;
            [category.managedObjectContext MR_saveToPersistentStoreAndWait];
            [self.allCategories insertObject:category atIndex:0];
            [self.pickerView reloadAllComponents];
            [self.pickerView selectRow:0 inComponent:0 animated:NO];
            [self pickerView:_pickerView didSelectRow:0 inComponent:0];
        } else
        {
            [[[UIAlertView alloc] initWithTitle:nil message:@"Название новой категории не может быть пустым" delegate:nil cancelButtonTitle:@"ОК" otherButtonTitles:nil] show];
        }
    }
}

- (void)setupStatusLabelWithText:(NSString*)text
{
    if (text.length == 0)
    {
        self.statusLabel.text = @"Выберите категорию";
    }
    else
    {
        NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"Выбрана категория: "];
        NSAttributedString *string2 = [[NSAttributedString alloc] initWithString:text attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:15.0]}];
        [string appendAttributedString:string2];
        self.statusLabel.attributedText = string;
    }
}

#pragma mark - TextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}

@end
