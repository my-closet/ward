//
//  ClotheVC.m
//  wardrobe
//
//  Created by Oleg Mytsouda on 26.02.16.
//  Copyright © 2016 Farcom. All rights reserved.
//

#import "ClotheVC.h"
#import "ClotheCell.h"
#import "APIManager.h"

@interface ClotheVC () <UITableViewDataSource, UITableViewDelegate>

@property(strong, nonatomic) NSMutableArray *resultArray;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentControll;

- (IBAction)addShop:(id)sender;

@end

@implementation ClotheVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self getFiltersWithType:@"1"];
}

- (void)getFiltersWithType:(NSString*)type
{
    self.resultArray = [NSMutableArray new];
    [SVProgressHUD show];
    [SVProgressHUD dismiss];

//    if (self.shop == nil) {
        [[APIManager sharedManager]getAllFiltersWithType:type andCompleteBlockAndHandler:^(id errorMsg, id response) {
            if (response != nil) {
                NSDictionary *dJSON = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:nil];
                for (NSDictionary *respObject in [dJSON valueForKey:@"result"]) {
                    [self.resultArray addObject:respObject];
                }
                [self.tableView reloadData];
                [SVProgressHUD dismiss];
            }
        }];
//    } else {
//    
//    }
//        [[APIManager sharedManager] getCloseFiltersWithShopID:[NSString stringWithFormat:@"%d", self.shop.uidValue] andType:type andHandler:^(id errorMsg, id response) {
//            if (response != nil) {
//                NSDictionary *dJSON = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:nil];
//                for (NSDictionary *respObject in [dJSON valueForKey:@"result"]) {
//                    [self.resultArray addObject:respObject];
//                }
//                [self.tableView reloadData];
//                [SVProgressHUD dismiss];
//            }
//        }];
//    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.resultArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"clothe";
    ClotheCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    NSDictionary *dict = self.resultArray[indexPath.row];

    cell.elementID = [dict valueForKey:@"id"];
    cell.filtersLabel.text = [NSString stringWithFormat:@"%@",[dict valueForKey:@"name"]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary *dict = self.resultArray[indexPath.row];
    [[NSUserDefaults standardUserDefaults] setValue:[dict valueForKey:@"id"] forKey:@"getCategoryProductWithID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)segmentView:(id)sender {
    NSInteger selectedSegment = self.segmentControll.selectedSegmentIndex;
    selectedSegment += 1;
    [self getFiltersWithType:[NSString stringWithFormat:@"%ld",(long)selectedSegment]];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)addShop:(id)sender
{
     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://mycloset.ru"]];
}

@end
