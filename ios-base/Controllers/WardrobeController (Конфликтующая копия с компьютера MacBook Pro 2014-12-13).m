
#import "WardrobeController.h"
#import "WardrobeCell.h"
@interface WardrobeController ()

@end

@implementation WardrobeController

static NSString * const reuseIdentifier = @"cell";

#define kWardrobeCellWidth 150
#define kWardrobeCellHeight 220

+ (instancetype)controller
{
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.itemSize = CGSizeMake(kWardrobeCellWidth, kWardrobeCellHeight);
        WardrobeController *controller = [[WardrobeController alloc] initWithCollectionViewLayout:layout];
    layout.minimumLineSpacing = 10;
    layout.minimumInteritemSpacing = 0;
    return controller;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Гардероб";
    
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Register cell classes
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([WardrobeCell class]) bundle:nil] forCellWithReuseIdentifier:reuseIdentifier];
    
    self.collectionView.backgroundColor = [UIColor whiteColor];
    // Do any additional setup after loading the view.
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    NSInteger numberOfCells = self.view.frame.size.width / kWardrobeCellWidth;
    NSInteger edgeInsets = (self.view.frame.size.width - (numberOfCells * kWardrobeCellWidth)) / (numberOfCells + 1);
    
    return UIEdgeInsetsMake(0, edgeInsets, 0, edgeInsets);
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self.collectionView.collectionViewLayout invalidateLayout];
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 10;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    WardrobeCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    return cell;
}

@end
