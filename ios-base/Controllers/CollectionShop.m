//
//  CollectionShop.m
//  wardrobe
//
//  Created by Oleg Mytsouda on 11.04.16.
//  Copyright © 2016 Farcom. All rights reserved.
//

#import "CollectionShop.h"
#import "MainCell.h"
#import "ClothesTableController.h"
#import "Shop.h"
#import "ShopCategory.h"
#import "PECropViewController.h"
#import "Item.h"
#import "ItemEditController.h"
#import "MainViewController.h"
#import "PopupView.h"
#import "IDMPhotoBrowser.h"
#import "PreviewVC.h"
#import "ShopItemVC.h"
#import "CollectionShopItem.h"
#import "ClotheVC.h"
#import "FilterVC.h"

@interface CollectionShop ()<UICollectionViewDataSource, UICollectionViewDelegate, FiltersDelegate>

@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *heightFilterConstraint;

@property (strong, nonatomic) IBOutlet UIButton *filtersButton;
@property (strong, nonatomic) IBOutlet UIButton *clothing;
@property (strong, nonatomic) IBOutlet UILabel *filtersTitle;

@property (assign, nonatomic) BOOL isFromFilters;

@property (strong, nonatomic) NSArray *itemsArray;

- (IBAction)filtersAction:(id)sender;
- (IBAction)clothingAction:(id)sender;

@end

@implementation CollectionShop
{
    Item *_selectedItem;
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.itemsArray = [NSArray new];
    [SVProgressHUD show];
    
    if (self.shop == nil) {
        self.filtersTitle.text = @"Магазины";
    } else {
        self.filtersTitle.text = self.shop.name;
    }
    
    if (self.mode == kShopModeShops){
        self.navigationItem.titleView = [[AppHelper sharedHelper] titleViewWithString:@"Магазины"];
        [[APIManager sharedManager] getShopsWithCompleteBlock:^(NSArray *resultArray) {
            self.itemsArray = resultArray;
            [self.collectionView reloadData];
            [SVProgressHUD dismiss];
        }];
        
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addShopTapped)];
        
    } else if (self.mode == kShopModeCategories){
        self.navigationItem.titleView = [[AppHelper sharedHelper] titleViewWithString:@"Категории"];
        [[APIManager sharedManager] getCategoriesWithShop:self.shop completeBlock:^(NSArray *resultArray) {
            self.itemsArray = resultArray;
            [self.collectionView reloadData];
            [SVProgressHUD dismiss];
        }];
    } else if (self.mode == kShopModeClothes){
        self.navigationItem.titleView = [[AppHelper sharedHelper] titleViewWithString:self.shop.name];
        [[APIManager sharedManager] getClothesWithShop:self.shop shopCategory:self.shopCategory completeBlock:^(NSArray *resultArray) {
            self.itemsArray = resultArray;
            [self.collectionView reloadData];
            [SVProgressHUD dismiss];
        }];
    }
}

- (void)getItems
{
    self.navigationItem.titleView = [[AppHelper sharedHelper] titleViewWithString:@"Магазины"];
    if (!self.isFromFilters) {
        [SVProgressHUD show];
        if (![[[NSUserDefaults standardUserDefaults] valueForKey:@"getCategoryProductWithID"] isEqualToString:@""] && [[NSUserDefaults standardUserDefaults] valueForKey:@"getCategoryProductWithID"] != nil) {
            [[APIManager sharedManager] getClothesWithShop:self.shop shopCategory:[[NSUserDefaults standardUserDefaults] valueForKey:@"getCategoryProductWithID"] completeBlock:^(NSArray *resultArray) {
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"getCategoryProductWithID"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                self.itemsArray = resultArray;
                [self.collectionView reloadData];
                [SVProgressHUD dismiss];
            }];
        } else {
            NSString *shopCatUID = [NSString stringWithFormat:@"%d",self.shopCategory.uidValue];
            [[APIManager sharedManager] getClothesWithShop:self.shop shopCategory:shopCatUID completeBlock:^(NSArray *resultArray) {
                self.itemsArray = resultArray;
                [self.collectionView reloadData];
                [SVProgressHUD dismiss];
            }];
        }
    }
}

- (void)setupButtonsDesign
{
    self.filtersTitle.text = [NSString stringWithFormat:@"%@",self.shopCategory.name];
    
    self.filtersButton.layer.borderWidth = 1.f;
    self.filtersButton.layer.borderColor = [UIColor whiteColor].CGColor;
    self.filtersButton.layer.cornerRadius = 6.f;
    
    self.clothing.layer.borderWidth = 1.0f;
    self.clothing.layer.borderColor = [UIColor whiteColor].CGColor;
    self.clothing.layer.cornerRadius = 6.f;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.itemsArray count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    
    CollectionShopItem *cell = [[CollectionShopItem alloc]init];
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];

    NSURL *imageURL = nil;
    
    if (self.mode == kShopModeShops){
        Shop *newShop = self.itemsArray[indexPath.row];
        cell.nameM.text = newShop.name;
        cell.priceValue.text = @"";
        imageURL = newShop.imageURL;
    } else if (self.mode == kShopModeCategories){
        ShopCategory *newCategory = self.itemsArray[indexPath.row];
        cell.nameM.text = newCategory.name;
        cell.priceValue.text = @"";
        imageURL = newCategory.imageURL;
    }
    
    if (imageURL){
        [cell loadShopImageWithURL:imageURL];
    } else {
        UIImage *image = [UIImage imageWithColor:RGB(255, 230, 230) radius:40 rect:CGRectMake(0, 0, 80, 80)];
        cell.avatarImageView.image = image;
        [cell.avatarImageView setNeedsDisplay];
    }
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.view.frame.size.width/2 - 10, 220);;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"index path %ld",(long)indexPath.row);
    if (self.mode == kShopModeShops){
        Shop *shop = self.itemsArray[indexPath.row];
        [[NSUserDefaults standardUserDefaults] setObject:shop.address forKey:@"shopURL"];
        [self showShopWithMode:kShopModeCategories shop:shop category:nil];
    }
    else if (self.mode == kShopModeCategories){
        ShopCategory *shopCategory = self.itemsArray[indexPath.row];
        Shop *shop = self.shop;
        [self showShopWithMode:kShopModeClothes shop:shop category:shopCategory];
    } else if (self.mode == kShopModeClothes){
        ShopCategory *shopCategory = self.itemsArray[indexPath.row];
        Shop *shop = self.shop;
        [self showShopWithMode:kShopModeClothes shop:shop category:shopCategory];
    }
    [self.collectionView deselectItemAtIndexPath:indexPath animated:YES];
}

- (void)showShopWithMode:(kShopMode)mode shop:(Shop*)shop category:(ShopCategory*)category
{
    if (mode == kShopModeClothes) {
        NSMutableArray *sendItem = [NSMutableArray new];
        [sendItem addObject:shop];
        [sendItem addObject:category];
        [self performSegueWithIdentifier:@"ShopItem" sender:sendItem];
    } else {
        CollectionShop *controller = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([CollectionShop class])];
        controller.mode = mode;
        controller.shop = shop;
        controller.shopCategory = category;
        [self.navigationController pushViewController:controller animated:YES];
    }
}

-(void)returnFiltersValue:(NSMutableArray *)filters
{
    self.isFromFilters = YES;
    [SVProgressHUD show];
    NSString *filter = [filters componentsJoinedByString:@"?"];
    [[APIManager sharedManager] getClothesWithShop:self.shop shopCategory:[NSString stringWithFormat:@"%d",self.shopCategory.uidValue] andFilters:filter completeBlock:^(NSArray *resultArray) {
        self.itemsArray = resultArray;
        [self.collectionView reloadData];
        [SVProgressHUD dismiss];
    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"toPreview"]) {
        PreviewVC *vc = (PreviewVC*)segue.destinationViewController;
        vc.navBarTitle = self.shop.name;
        vc.photos = [sender objectAtIndex:1];
        vc.index = [[sender objectAtIndex:0] intValue];
        vc.shop = [sender objectAtIndex:2];
        vc.shopCategory = [sender objectAtIndex:3];
    } else if ([segue.identifier isEqualToString:@"toclothe"]) {
        ClotheVC *clothe = segue.destinationViewController;
        if (self.shop != nil) {
            clothe.shop = [sender valueForKey:@"shop"];
        }
    } else if ([segue.identifier isEqualToString:@"toFilters"]) {
        FilterVC *filter = segue.destinationViewController;
        filter.delegate = self;
        if (self.shop != nil) {
            filter.shop = [sender valueForKey:@"shop"];
        }
    } else if ([segue.identifier isEqualToString:@"ShopItem"]){
        ShopItemVC *item = segue.destinationViewController;
        item.shop = sender[0];
        item.shopCategory = sender[1];
    }
}

- (IBAction)filtersAction:(id)sender
{
    if (self.shop != nil) {
        NSDictionary * senderDict = @{@"shop":self.shop};
        [self performSegueWithIdentifier:@"toFilters" sender:senderDict];
    } else {
        [self performSegueWithIdentifier:@"toFilters" sender:nil];
    }
}

- (IBAction)clothingAction:(id)sender
{
    if (self.shop != nil) {
        NSDictionary * senderDict = @{@"shop":self.shop};
        [self performSegueWithIdentifier:@"toclothe" sender:senderDict];
    } else {
        [self performSegueWithIdentifier:@"toclothe" sender:nil];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
