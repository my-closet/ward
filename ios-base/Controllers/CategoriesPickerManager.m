#import "CategoriesPickerManager.h"
@interface CategoriesPickerManager() <UIPickerViewDataSource, UIPickerViewDelegate>

@end
@implementation CategoriesPickerManager
{
    Cat *_selectedCategory;
    UIPickerView *_pickerView;
    NSMutableArray *_allCategories;
    UITextField *_hiddenTextField;
    UIToolbar *_accessoryToolbar;
    Item *_item;
    UIViewController *_controller;
}
- (void)setupWithItem:(Item*)item controller:(UIViewController*)controller{
    
    _item = item;
    _controller = controller;
    
    _allCategories = [NSMutableArray arrayWithArray:[Cat allButFavorite]];
    
    Cat *localCategory = [item.category MR_inThreadContext];
    
    _pickerView = [[UIPickerView alloc] init];
    _pickerView.delegate = self;
    _pickerView.dataSource = self;
    
    _accessoryToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, controller.view.width, 44)];
    UIBarButtonItem *item1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelTapped)];
    UIBarButtonItem *item2 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *item3 = [[UIBarButtonItem alloc] initWithTitle:@"Сохранить" style:UIBarButtonItemStylePlain target:self action:@selector(saveTapped)];
    
    
    
    [_accessoryToolbar setItems:@[item1, item2, item3]];
    
    _hiddenTextField = [UITextField new];
    _hiddenTextField.hidden = YES;
    _hiddenTextField.inputAccessoryView = _accessoryToolbar;
    _hiddenTextField.inputView = _pickerView;
    [_hiddenTextField becomeFirstResponder];
    [_controller.navigationController.view addSubview:_hiddenTextField];
    
    NSInteger index = [_allCategories indexOfObject:localCategory];
    if (index != NSNotFound){
        _selectedCategory = localCategory;
        [_pickerView selectRow:index inComponent:0 animated:NO];
    }
}

- (void)dismiss{
    [_hiddenTextField resignFirstResponder];
}

- (void)cancelTapped{
    [_hiddenTextField resignFirstResponder];
    dispatch_after_short(0.3, ^{
        [_hiddenTextField removeFromSuperview];
    });
}
- (void)saveTapped{
    _item.category = _selectedCategory;
   // _item.uid = [NSNumber numberWithInt:2];
    [[_item managedObjectContext] MR_saveToPersistentStoreAndWait];
    [_hiddenTextField resignFirstResponder];
    [_controller viewWillAppear:YES];
    dispatch_after_short(0.3, ^{
        [_hiddenTextField removeFromSuperview];
    });
}

#pragma mark - Picker View Data source
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return _allCategories.count;
}
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    Cat *cat = [_allCategories objectAtIndex:row];
    return cat.name;
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    Cat *cat = [_allCategories objectAtIndex:row];
    _selectedCategory = cat;
}


@end
