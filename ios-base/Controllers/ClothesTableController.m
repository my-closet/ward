
#import "ClothesTableController.h"
#import "MainCell.h"
#import "ImagePreviewController.h"
#import "MainViewController.h"
#import "CameraHelper.h"
#import "CategoriesPickerManager.h"
#import "IDMPhotoBrowser.h"
#import "PopupView.h"
#import "PreviewVC.h"

@interface ClothesTableController () <SWTableViewCellDelegate, IDMPhotoBrowserDelegate, PopupViewDelegate>
@property (nonatomic, strong) NSMutableArray *itemsArray;
@property (nonatomic, strong) Cat *cat;

@end

@implementation ClothesTableController
{
    CategoriesPickerManager *_pickerManager;
}
- (void)setupWithShop:(id)shop
{
    self.navigationItem.titleView = [[AppHelper sharedHelper] titleViewWithString:@"La Moda"];
}
- (void)setupWithCategory:(Cat *)category
{
    Cat *cat = [category MR_inContext:[NSManagedObjectContext MR_defaultContext]];
    self.cat = cat;
    
    self.navigationItem.titleView = [[AppHelper sharedHelper] titleViewWithString:cat.name];
    
    if (!cat.favoriteValue)
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addTapped:)];
}


- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.itemsArray = [NSMutableArray arrayWithArray:_cat.items.array];
    [self.tableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [_pickerManager dismiss];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([MainCell class]) bundle:nil] forCellReuseIdentifier:@"MainCell"];
    
    self.tableView.tableFooterView = [UIView new];
    
}

#pragma mark - Actions

- (void)addTapped:(UIBarButtonItem*)item
{
    [self showCameraPopup];
}

- (void)showCameraPopup{
    PopupView *popup = [PopupView cameraView];
    popup.delegate = self;
    popup.tag = 999;
    [popup showInView:self.navigationController.view];
}

#pragma mark - Popup View

- (void)popupView:(PopupView*)popupView clickedButtonAtIndex:(int)index
{
    [[CameraHelper sharedHelper] launchPhotoCaptureForCategory:nil isLibrary:index == 1];
}
    

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.itemsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MainCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MainCell" forIndexPath:indexPath];
    
    Item *item = [self.itemsArray objectAtIndex:indexPath.row];
    
    BOOL presented = ([self.navigationController.presentingViewController isKindOfClass:[MainViewController class]]);
    [cell setupWithItem:item mode:!presented category:nil];
    cell.delegate = self;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    Item *item = [self.itemsArray objectAtIndex:indexPath.row];
    
    if ([self.navigationController.presentingViewController isKindOfClass:[MainViewController class]])
    {
        BLOCK_SAFE_RUN(self.selectionBlock,item);
        self.selectionBlock = nil;
    }
    else
    {
        MainCell *cell  = (MainCell*)[tableView cellForRowAtIndexPath:indexPath];
        [self showPreviewWithItems:self.itemsArray currentIndex:(int)indexPath.row fromView:nil];
    }
}

#pragma mark - Swipe delegate

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index
{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    Item *item = [self.itemsArray objectAtIndex:indexPath.row];
    
    BOOL moveSelected = cell.rightUtilityButtons.count ==2 && index == 0;
    
    [_pickerManager dismiss];
    
    if (moveSelected)
    {
        _pickerManager = [[CategoriesPickerManager alloc] init];
        [_pickerManager setupWithItem:item controller:self];
        [cell hideUtilityButtonsAnimated:YES];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:WillDeleteItemNotification object:item];
        [AppHelper deleteImageWithUUIDString:item.image_url];
        [self.itemsArray removeObject:item];
        [item MR_deleteEntity];
        [item.managedObjectContext MR_saveToPersistentStoreAndWait];
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        UINavigationController *root = (UINavigationController*)ApplicationDelegate.window.rootViewController;
        [[[root viewControllers] firstObject] addFavoriteItemToCanvas];
    }
}


#pragma mark - Photos Preview

- (void)showPreviewWithItems:(NSArray*)items currentIndex:(int)index fromView:(UIView*)view{
    
//    NSMutableArray *photos = [NSMutableArray array];
//    
//    for (Item *item in items){
//        IDMPhoto *photo;
//        if (item.uidValue < kLocalItemIDOffset){
//            photo = [[IDMPhoto alloc] initWithURL:[item imageURL]];
//        } else {
//            UIImage *image = [AppHelper imageFromUUIDString:item.image_url];
//            photo = [[IDMPhoto alloc] initWithImage:image];
//        }
//        [photos addObject:photo];
//    }

    NSString *currentElementIndex = [NSString stringWithFormat:@"%d", index];
    
    NSMutableArray *sendItem = [NSMutableArray new];
    [sendItem addObject:currentElementIndex];
    [sendItem addObject:items];
    [self performSegueWithIdentifier:@"toPreview" sender:sendItem];
    
//    IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:photos animatedFromView:view];
//    
//    if (index < photos.count)
//        [browser setInitialPageIndex:index];
//    
//    browser.displayArrowButton = NO;
//    browser.displayCounterLabel = YES;
//    browser.delegate = self;
//    browser.displayActionButton = NO;
//    [ApplicationDelegate.window.rootViewController presentViewController:browser animated:YES completion:nil];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"toPreview"]) {
        PreviewVC *vc = (PreviewVC*)segue.destinationViewController;
        vc.navBarTitle = @" ";
        vc.photos = [sender objectAtIndex:1];
        vc.index = [[sender objectAtIndex:0] intValue];
        vc.isMy = TRUE;
        vc.currentCategory = self.cat;
    }
}

- (void)photoBrowser:(IDMPhotoBrowser *)photoBrowser willDismissAtPageIndex:(NSUInteger)index{
    if (photoBrowser.addTapped){
        Item *item = [self.itemsArray objectAtIndex:index];
        BLOCK_SAFE_RUN(self.selectionBlock,item);
        self.selectionBlock = nil;
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

@end
