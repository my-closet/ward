//
//  FilterCell.m
//  wardrobe
//
//  Created by Oleg Mytsouda on 07.03.16.
//  Copyright © 2016 Farcom. All rights reserved.
//

#import "FilterCell.h"

@implementation FilterCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
