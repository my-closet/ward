#import <UIKit/UIKit.h>
#import "Cat.h"
#import "Shop.h"
#import "ShopCategory.h"

@interface PreviewVC : UIViewController

@property(strong, nonatomic) NSString *navBarTitle;
@property(strong, nonatomic) NSMutableArray *photos;
@property(assign, nonatomic) int index;
@property(assign, nonatomic) BOOL isMy;
@property(strong, nonatomic) Cat *currentCategory;
@property (nonatomic, strong) Shop *shop;
@property (nonatomic, strong) ShopCategory *shopCategory;
@property (strong, nonatomic) NSMutableArray *productUrlArray;
@property (strong, nonatomic) NSMutableArray *oldPriceArray;
@property (strong, nonatomic) NSMutableArray *priceArray;

- (void)getUrlArray;

@end
