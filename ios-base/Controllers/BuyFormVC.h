//
//  BuyFormVC.h
//  wardrobe
//
//  Created by Oleg Mytsouda on 24.11.15.
//  Copyright © 2015 Farcom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BuyFormVC : UIViewController

@property (strong, nonatomic) Item *item;
@property (strong, nonatomic) NSString *senderEmail;

@end
