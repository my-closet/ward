
#import "WardrobeController.h"
#import "WardrobeCell.h"
#import "ClothesTableController.h"
@interface WardrobeController () < WardrobeCellDelegate>
@property (nonatomic, strong) NSMutableArray *categories;
@end

@implementation WardrobeController

NSString *const WillDeleteItemNotification = @"willDeleteItemNotification";

static NSString * const reuseIdentifier = @"cell";

#define kWardrobeCellWidth 160
#define kWardrobeCellHeight 220
- (void)dealloc
{
    self.selectionBlock = nil;
}
+ (instancetype)controller
{
    UIStoryboard *s = [UIStoryboard storyboardWithName:@"main" bundle:nil];
    WardrobeController *controller = [s instantiateViewControllerWithIdentifier:NSStringFromClass([WardrobeController class])];
    
    return controller;
}


- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout*)self.collectionView.collectionViewLayout;
    layout.itemSize = CGSizeMake(kWardrobeCellWidth, kWardrobeCellHeight);
    layout.minimumLineSpacing = 10;
    layout.minimumInteritemSpacing = 0;

    self.collectionView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    
    self.title = @"Мой Гардероб";
    
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Register cell classes
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([WardrobeCell class]) bundle:nil] forCellWithReuseIdentifier:reuseIdentifier];
    
    self.collectionView.backgroundColor = [UIColor whiteColor];
    // Do any additional setup after loading the view.
    
    self.categories = [NSMutableArray arrayWithArray:[Cat MR_findAll]];
    
    Cat *cat = [Cat favoriteCategory];
    
    if (cat)
    {
        NSInteger index = [self.categories indexOfObject:cat];
        if (index != NSNotFound)
            [self.categories removeObjectAtIndex:index];
        [self.categories insertObject:cat atIndex:0];
    }
    
    if ([self.navigationController.viewControllers firstObject] == self)
    {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Отменить" style:UIBarButtonItemStylePlain target:self action:@selector(cancelTapped)];
    }
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.editButtonItem.title = @"Изменить";
    
    self.collectionView.alwaysBounceVertical = YES;
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.collectionView.contentInset = UIEdgeInsetsMake(10, 0, 0, 0);
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.collectionView reloadData];
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    [super setEditing:editing animated:animated];
    
    if (editing){
        self.editButtonItem.title = @"Готово";
    } else {
        self.editButtonItem.title = @"Изменить";
    }
    
    for (WardrobeCell *cell in self.collectionView.visibleCells)
    {
        cell.editing = self.editing;
    }
}

- (void)cancelTapped
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    NSInteger numberOfCells = self.view.frame.size.width / kWardrobeCellWidth;
    NSInteger edgeInsets = (self.view.frame.size.width - (numberOfCells * kWardrobeCellWidth)) / (numberOfCells + 1);
    
    return UIEdgeInsetsMake(0, edgeInsets, 0, edgeInsets);
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self.collectionView.collectionViewLayout invalidateLayout];
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _categories.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    WardrobeCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    [cell setupWithCategory:[_categories objectAtIndex:indexPath.item]];
    cell.delegate = self;
    cell.editing = self.editing;
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.editing){
        [self setEditing:NO animated:YES];
        return;
    }
    Cat *category = [_categories objectAtIndexOrNil:indexPath.item];
    UIStoryboard *s = [UIStoryboard storyboardWithName:@"main" bundle:nil];
    ClothesTableController *controller = [s instantiateViewControllerWithIdentifier:NSStringFromClass([ClothesTableController class])];
    [controller setupWithCategory:category];
    controller.selectionBlock = self.selectionBlock;
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - Wardrobe Cell Delete Delegate

- (void)wardrobeCellDidDelete:(WardrobeCell*)cell
{
    NSIndexPath *indexPath = [self.collectionView indexPathForCell:cell];
    Cat *category = [_categories objectAtIndexOrNil:indexPath.item];
    if (category)
    {
        for (Item *item in category.items)
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:WillDeleteItemNotification object:item];
            
            [AppHelper deleteImageWithUUIDString:item.image_url];
            [item MR_deleteEntity];
        }
        [category MR_deleteEntity];
        [_categories removeObjectAtIndex:indexPath.item];
        [self.collectionView deleteItemsAtIndexPaths:@[indexPath]];
        [category.managedObjectContext MR_saveToPersistentStoreAndWait];
    }
}
@end
