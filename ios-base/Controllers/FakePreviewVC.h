#import <UIKit/UIKit.h>
#import "Cat.h"
#import "Shop.h"
#import "ShopCategory.h"
#import <UIKit/UIKit.h>

@interface FakePreviewVC : UIViewController

@property(strong, nonatomic) NSMutableArray *photos;
@property(strong, nonatomic) Cat *currentCategory;
@property (nonatomic, strong) Shop *shop;
@property (nonatomic, strong) ShopCategory *shopCategory;

@property (strong, nonatomic) NSArray *value;

@property (strong, nonatomic) Item *getItem;

@end
