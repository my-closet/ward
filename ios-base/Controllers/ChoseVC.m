//
//  ChoseVC.m
//  wardrobe
//
//  Created by Oleg Mytsouda on 08.03.16.
//  Copyright © 2016 Farcom. All rights reserved.
//

#import "ChoseVC.h"
#import "ChoseCell.h"

@interface ChoseVC () <UITableViewDataSource, UITableViewDelegate, ChoseCellDelegate>

@end

@implementation ChoseVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = self.navTitle;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.valuesArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"value";
    ChoseCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.filtersLabel.text = self.valuesArray[indexPath.row];
    cell.delegate = self;
    
    if ([self.selectedItems containsObject:cell.filtersLabel.text]) {
        [cell changeImageWithBool:NO];
    } else {
        [cell changeImageWithBool:YES];
    }
    
    return cell;
}

- (void)selectedCell:(ChoseCell *)cell
{
    if ([self.selectedItems containsObject:cell.filtersLabel.text]) {
        [self.selectedItems enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([obj isEqualToString:cell.filtersLabel.text]) {
                [self.selectedItems removeObjectAtIndex:idx];
            }
        }];
    } else {
        [self.selectedItems addObject:cell.filtersLabel.text];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.delegate returnValueArray:self.selectedItems];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
