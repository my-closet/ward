
#import <UIKit/UIKit.h>
#import "CanvasView.h"
#import "RSBarcodes.h"

@interface MainViewController : UIViewController <UIAlertViewDelegate>

@property (nonatomic, strong) RSScannerViewController *scanner;

@property (nonatomic, strong) CanvasView *canvasView;

@property (nonatomic, assign) BOOL swipe;
@property (nonatomic, assign) int index;


- (ObjectCallback)itemAddToCanvasBlock;
- (void)editTapped;
- (void)addFavoriteItemToCanvas;
- (void)didSwipeLeft;

@end
