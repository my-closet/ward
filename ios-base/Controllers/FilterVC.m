//
//  FilterVC.m
//  wardrobe
//
//  Created by Oleg Mytsouda on 07.03.16.
//  Copyright © 2016 Farcom. All rights reserved.
//

#import "FilterVC.h"
#import "FilterCell.h"
#import "APIManager.h"
#import "ChoseVC.h"

@interface FilterVC () <UITableViewDataSource, UITableViewDelegate, ChoseDelegate>

@property (strong, nonatomic) NSMutableArray *resultArray;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentControll;

@property (strong, nonatomic) NSMutableArray *choseValue;

@property (strong, nonatomic) NSMutableArray *mDict;

@property (strong, nonatomic) FilterCell *lastSelectedCell;

- (IBAction)addShop:(id)sender;

@end

@implementation FilterVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self getFilterInrofm];
    self.mDict = [NSMutableArray new];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)getFilterInrofm
{
    [SVProgressHUD show];
    if (self.shop == nil) {
        [[APIManager sharedManager] getAllFiltersWithCompleteBlockAndHandler:^(id errorMsg, id response) {
            self.resultArray = [NSMutableArray new];
            self.choseValue = [NSMutableArray new];
            if (response != nil) {
                NSDictionary *dJSON = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:nil];
                for (NSDictionary *respObject in [dJSON valueForKey:@"result"]) {
                    [self.resultArray addObject:respObject];
                }
                [self.tableView reloadData];
            }
            [SVProgressHUD dismiss];
        }];
    } else {
        [[APIManager sharedManager] getShopFiltersWithCompleteBlockWithShopID:[NSString stringWithFormat:@"%d", self.shop.uidValue] andHandler:^(id errorMsg, id response) {
            self.resultArray = [NSMutableArray new];
            self.choseValue = [NSMutableArray new];
            if (response != nil) {
                NSDictionary *dJSON = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:nil];
                for (NSDictionary *respObject in [dJSON valueForKey:@"result"]) {
                    [self.resultArray addObject:respObject];
                }
                [self.tableView reloadData];
            }
            [SVProgressHUD dismiss];
        }];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.resultArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"filter";
    FilterCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    NSDictionary *dict = self.resultArray[indexPath.row];
    
    cell.element = dict;
    cell.filtersLabel.text = [dict valueForKey:@"name"];
    cell.filterValue.text = nil;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    self.lastSelectedCell = [tableView cellForRowAtIndexPath:indexPath];
    [self performSegueWithIdentifier:@"chose" sender:self.resultArray[indexPath.row]];
}

- (IBAction)segmentView:(id)sender
{
    NSInteger selectedSegment = self.segmentControll.selectedSegmentIndex;
    if (selectedSegment == 0) {
        
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"chose"]) {
        ChoseVC *chose = segue.destinationViewController;
        chose.delegate = self;
        chose.selectedItems = self.lastSelectedCell.filterValue.text == nil ? [NSMutableArray new] : [NSMutableArray arrayWithArray:[self.lastSelectedCell.filterValue.text componentsSeparatedByString:@","]];
        chose.navTitle = [sender valueForKey:@"name"];
        chose.valuesArray = [sender valueForKey:@"values"];
    }
}

-(void)returnValueArray:(NSArray *)valuesArray
{
    self.lastSelectedCell.filterValue.text = [valuesArray componentsJoinedByString:@","];
    for (NSString *value in valuesArray) {
        [self.mDict addObject:[NSString stringWithFormat:@"field_filter[%@]=%@",[self.lastSelectedCell.element valueForKey:@"id"],value]];
    }
}

- (IBAction)doneAction:(id)sender
{
    [self.delegate returnFiltersValue:self.mDict];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)addShop:(id)sender
{
         [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://mycloset.ru"]];
}

@end