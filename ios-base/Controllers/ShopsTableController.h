
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, kShopMode){
    kShopModeShops,
    kShopModeCategories,
    kShopModeClothes,
};

@class Shop, ShopCategory;

@interface ShopsTableController : UITableViewController
@property (nonatomic, assign) kShopMode mode;
@property (nonatomic, strong) Shop *shop;
@property (nonatomic, strong) ShopCategory *shopCategory;
@end
