#import "MWPhotoBrowser.h"
#import "CanvasView.h"
#import "ItemEditController.h"
#import "IDMPhotoBrowser.h"
#import "PECropViewController.h"
#import "MainViewController.h"
#import "LoadingVC.h"
#import <UIImageView+AFNetworking.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "BuyFormVC.h"
#import "FakePreviewVC.h"
#import "LikedItems.h"

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)

@interface FakePreviewVC () <IDMPhotoBrowserDelegate, PECropViewControllerDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topImageViewConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftSideConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rightSideConstraint;

@property (weak, nonatomic) IBOutlet UIView *activityView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIButton *buyButton;
@property (weak, nonatomic) IBOutlet UIButton *addToMeButton;
@property (weak, nonatomic) IBOutlet UIButton *choseButton;

@property (nonatomic, strong) CanvasView *canvasView;

@property (strong, nonatomic) Item *selectedItem;

- (IBAction)buyButtonTapped:(id)sender;
- (IBAction)addToMeButtonTapped:(id)sender;
- (IBAction)choseButtonTapped:(id)sender;

@property (assign, nonatomic) int height;
@property (assign, nonatomic) int widht;
@property (assign, nonatomic) int top;

@property (strong, nonatomic) Item *findedValue;

@end

@implementation FakePreviewVC

{
    BOOL _stateEditing;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad
{
    [SVProgressHUD dismiss];
    
//    self.findedValue = [Item MR_findByAttribute:@"name" withValue:[self.photos valueForKey:@"id"]];
//    if ([self.findedValue isKindOfClass:[NSArray class]]) {
//        self.findedValue = (Item*)[Item MR_findByAttribute:@"name" withValue:[self.photos valueForKey:@"id"]][0];
//    }
    
    self.findedValue = self.getItem;
    
    [super viewDidLoad];
    [self setupView];
    [self showInformInView];
}

- (void)setupView{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(bacToPreview) name:@"savePreviewElenetToMe" object:nil];
    if (IS_IPHONE_4_OR_LESS) {
        self.top = self.topImageViewConstraint.constant - 10;
        self.height = 270;
        self.widht = 240;
    } else if(IS_IPHONE_5){
        self.top = self.topImageViewConstraint.constant;
        self.height = 332;
        self.widht = 270;
    } else if(IS_IPHONE_6 ){
        self.top = self.topImageViewConstraint.constant;
        self.height = 425;
        self.widht = 345;
    } else if (IS_IPHONE_6P){
        self.top = self.topImageViewConstraint.constant;
        self.height = 510;
        self.widht = 414;
    }
    self.activityView.layer.cornerRadius = 14.f;
    [self.view sendSubviewToBack:self.activityView];
}

- (UIImage*)resizeImage:(UIImage*)image withWidth:(CGFloat)width withHeight:(CGFloat)height
{
    CGSize newSize = CGSizeMake(width, height);
    CGFloat widthRatio = newSize.width/image.size.width;
    CGFloat heightRatio = newSize.height/image.size.height;
    if (image.size.width < newSize.width) {
        widthRatio = 1.0;
    } if (image.size.height < newSize.height) {
        heightRatio = 1.0;
    } if(widthRatio > heightRatio) {
        newSize=CGSizeMake(image.size.width*heightRatio,image.size.height*heightRatio);
    } else {
        newSize=CGSizeMake(image.size.width*widthRatio,image.size.height*widthRatio);
    }
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


- (void)bacToPreview
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)showInformInView
{
    self.imageView.image = nil;
//    dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
//    dispatch_async(q, ^{
//        UIImage *newImage;
//        
//        NSString *url;
//        if (![[self.photos valueForKey:@"image_url"] hasPrefix:@"http://"] && ![[self.photos valueForKey:@"image"] hasPrefix:@"https://"]) {
//            NSString *stringFromURL = [NSString stringWithFormat:@"%@%@",DOMAIN_URL,[self.photos valueForKey:@"image"]];
//            url = stringFromURL;
//        } else {
//            url = [NSString stringWithFormat:@"%@",[self.photos valueForKey:@"image_url"]];
//        }
//        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
//        newImage = [UIImage imageWithData:imageData];
//        
//        if (newImage != nil) {
//            dispatch_async(dispatch_get_main_queue(), ^{
//                self.imageView.image = [self resizeImage:newImage withWidth:self.widht withHeight:self.height];
//            });
//        } else {
            NSString *url = [self.photos valueForKey:@"image"];
    
    
    
            UIImage *image = [AppHelper imageFromUUIDString:[self.findedValue image_url]];
          //  dispatch_async(dispatch_get_main_queue(), ^{
                self.imageView.image = [self resizeImage:image withWidth:self.widht withHeight:self.height];
           // });
        //}
    //});
}


- (void)loadLocalImageWithItem:(Item*)item{
    __weak typeof(self) aSelf = self;
    __block NSString *url = [item.image_url copy];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        UIImage *image = [AppHelper imageFromUUIDString:url];
        dispatch_after_short(0, ^{
            aSelf.imageView.image = image;
            if (image == nil) {
                self.activityView.hidden = YES;
            }
            [aSelf.imageView setNeedsDisplay];
        });
    });
}

- (IBAction)buyButtonTapped:(id)sender
{
    [[APIManager sharedManager] setLiktToPost:[self.getItem.uid stringValue] andHandler:^(id errorMsg, id response) {
        if (errorMsg == nil) {
            LikedItems *likeItem = [LikedItems MR_createEntity];
            likeItem.myLikeID = [NSString stringWithFormat:@"%@",self.selectedItem.uid];
            likeItem.serverLikeID = [response valueForKey:@"id"];
            [likeItem.managedObjectContext MR_saveToPersistentStoreAndWait];
        }
    }];
}

- (IBAction)addToMeButtonTapped:(id)sender {
    UIStoryboard *s = [UIStoryboard storyboardWithName:@"main" bundle:nil];
    ItemEditController *c = [s instantiateViewControllerWithIdentifier:NSStringFromClass([ItemEditController class])];
    c.item = self.findedValue;
    //c.targetCategory = [Cat favoriteCategory];

    __block Item *itemForBlock = c.item;
    
    dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(q, ^{
        UIImage *newImage;
        
        NSString *url;
        if (![itemForBlock.image_url hasPrefix:@"http://"] && ![itemForBlock.image_url hasPrefix:@"https://"]) {
            NSString *stringFromURL = [NSString stringWithFormat:@"%@%@",DOMAIN_URL,itemForBlock.image_url];
            url = stringFromURL;
        } else {
            url = [NSString stringWithFormat:@"%@",itemForBlock.image_url];
        }
        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
        newImage = [UIImage imageWithData:imageData];
        dispatch_async(dispatch_get_main_queue(), ^{
            c.image = newImage;
            c.savedBlock = nil;
        //    c.targetCategory = [Cat favoriteCategory];
            [self.navigationController pushViewController:c animated:YES];
        });
    });
}


- (IBAction)choseButtonTapped:(id)sender {
    Item *item = self.findedValue;
    
    __block UIImage *image = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:[item image_url]];
    
    dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(q, ^{
        UIImage *newImage;
        
       
//       // if (![item.image_url hasPrefix:@"http://"] && ![item.image_url hasPrefix:@"https://"]) {
//      //      NSString *stringFromURL = [NSString stringWithFormat:@"%@%@",DOMAIN_URL,item.image_url];
//            url = stringFromURL;
//        } else {
//            url = [NSString stringWithFormat:@"%@",item.image_url];
//        }
//        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
//        newImage = [UIImage imageWithData:imageData];
//        
//        if (newImage != nil) {
//            dispatch_async(dispatch_get_main_queue(), ^{
//                image = newImage;
//                [self addToMainScreenWithImage:image andItem:item];
//            });
//        } else {
            NSString *url = item.image_url;
            UIImage *imageD = [AppHelper imageFromUUIDString:url];
            dispatch_async(dispatch_get_main_queue(), ^{
                image = imageD;
                [self addToMainScreenWithImage:image andItem:item];
            });
       // }
    });
}


- (void)addToMainScreenWithImage:(UIImage*)image andItem:(Item*)item
{
    if (!image){
        [[[UIAlertView alloc] initWithTitle:nil
                                    message:@"Нельзя добавить вещь, потому что ее изображение отсутствует" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        return;
    }
    PECropViewController *controller = [[PECropViewController alloc] init];
    controller.delegate = self;
    controller.image = image;
    controller.toolbarHidden = YES;
    controller.title = @"Выберите область";
    self.selectedItem = item;
    
    [controller performSelector:@selector(done:) withObject:nil];
}

- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage{
    ItemEditController *editController = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([ItemEditController class])];
    editController.image = croppedImage;
    editController.shopItem = self.selectedItem;
    editController.targetCategory = self.currentCategory;
    MainViewController *vController;
    vController = (MainViewController*)self.navigationController.viewControllers.firstObject;
    if ([vController isKindOfClass:[MainViewController class]])
    {
        editController.savedBlock = [vController itemAddToCanvasBlock];
        [vController.canvasView addItemToCanvas:self.selectedItem];
        [vController.canvasView clearCanvas];
     //   [vController.canvasView addItemsToCanvasFromFavoriteItem:self.selectedItem];
        [vController addFavoriteItemToCanvas];
        [vController editTapped];
        self.selectedItem = nil;
    } else {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"needToSave"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)cropViewControllerDidCancel:(PECropViewController *)controller
{
    _selectedItem = nil;
    [controller dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

@end