
#import "TabButton.h"

@implementation TabButton
-(void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.imageView sizeToFit];
    
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    CGFloat imgHeight = self.imageView.height;
    CGFloat labelHeight = self.height / 2.8;
    

    self.imageView.y = (self.height - imgHeight - labelHeight) / 2.0f;
    self.imageView.x = (self.width - self.imageView.width) / 2.0f;

    self.titleLabel.frame = CGRectMake(0, self.imageView.bottomY, self.width, labelHeight);
    
    self.titleLabel.clipsToBounds = NO;

    
//    self.titleLabel.backgroundColor = [UIColor colorWithWhite:0 alpha:0.4];
//    self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.2];
}

@end
