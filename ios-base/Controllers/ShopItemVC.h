//
//  ShopItemVC.h
//  wardrobe
//
//  Created by Oleg Mytsouda on 18.01.16.
//  Copyright © 2016 Farcom. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Shop.h"
#import "ShopCategory.h"

@interface ShopItemVC : UIViewController

@property(strong, nonatomic) NSString *navBarTitle;

@property (nonatomic, strong) Shop *shop;
@property (nonatomic, strong) ShopCategory *shopCategory;

@end
