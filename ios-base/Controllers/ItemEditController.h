
#import <UIKit/UIKit.h>

@interface ItemEditController : UIViewController
@property (nonatomic, strong) Item *item;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic,assign) NSURL *imageURL;
@property (nonatomic, strong) Cat *targetCategory;
@property (nonatomic, copy) ObjectCallback savedBlock;

@property (nonatomic, strong) Item *shopItem;
@end
