
#import "MainCell.h"
#import "Shop.h"
#import "ShopCategory.h"
@interface MainCell()

@property (nonatomic, weak) IBOutlet UILabel *topLabel;
@property (nonatomic, weak) IBOutlet UILabel *bottomLabel;
@property (nonatomic, weak) IBOutlet UIButton *actionButton;
//@property (nonatomic, weak) IBOutlet UILabel *priceLabel;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *topLabelTopConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *topLabelTrailingConstraint;

@property (strong, nonatomic) NSMutableArray *pArray;

@end
@implementation MainCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    self.priceLabel.hidden = YES;
    
}

- (void)setupWithShop:(Shop*)shop
{
    if (shop.imageURL){
        [self loadImageWithURL:shop.imageURL];
    } else {
        UIImage *image = [UIImage imageWithColor:RGB(255, 230, 230) radius:40 rect:CGRectMake(0, 0, 80, 80)];
        self.imgView.image = image;
        [self.imgView setNeedsDisplay];
    }
    
    self.priceLabel.hidden = YES;
    self.actionButton.hidden = YES;
    self.topLabelTopConstraint.constant = 10;
    
    self.topLabel.text = shop.name;
    self.bottomLabel.text = shop.address;
    
}

- (void)setupWithShopCategory:(ShopCategory*)shopCategory shop:(Shop*)shop
{
    self.priceLabel.hidden = YES;
    self.actionButton.hidden = YES;
    self.topLabelTopConstraint.constant = 10;
    
    self.topLabel.text = shopCategory.name;
    self.bottomLabel.text = shop.name;
    
    [self loadShopImageWithURL:[shopCategory imageURL]];
}

- (void)setupWithItem:(Item*)item mode:(BOOL)onlySelection category:(ShopCategory*)shopCategory
{
    if (!onlySelection) {
        NSString *stringFromURL;
        if (![item.image_url hasPrefix:@"http://"] && [item.image_url hasPrefix:@"https://"]) {
            stringFromURL = [NSString stringWithFormat:@"%@%@",DOMAIN_URL,item.image_url];
            [item imageURLFromStr:stringFromURL];
        } else {
            stringFromURL = item.image_url;
        }
    if (item.uidValue < kLocalItemIDOffset){
            [self loadShopImageWithURL:[item imageURLFromStr:stringFromURL]];
        } else {
            [self loadLocalImageWithItem:item];
        }
    } else {
        if (item.uidValue < kLocalItemIDOffset){
            NSString *stringFromURL;
            if (![item.image_url hasPrefix:@"http://"] && ![item.image_url hasPrefix:@"https://"] && ![item.image_url containsString:@"-"]) {
                stringFromURL = [NSString stringWithFormat:@"%@%@",DOMAIN_URL,item.image_url];
                [item imageURLFromStr:stringFromURL];
                [self loadShopImageWithURL:[item imageURLFromStr:stringFromURL]];
            } else if ([item.image_url hasPrefix:@"http://"] && [item.image_url hasPrefix:@"https://"]) {
                stringFromURL = item.image_url;
                [self loadShopImageWithURL:[item imageURLFromStr:stringFromURL]];
            } else {
                [self loadLocalImageWithItem:item];
            }
        } else {
            [self loadLocalImageWithItem:item];
            
        }
    }
    if (shopCategory){
        self.bottomLabel.hidden = NO;
        self.bottomLabel.text = shopCategory.name;
        self.priceLabel.hidden = NO;
        
        self.priceLabel.text = [[AppHelper sharedHelper] currencyStringFromString:item.price];
        
        self.topLabelTopConstraint.constant = 10;
        self.topLabelTrailingConstraint.constant = 56;
    }
//    else {
//        if (item.imageURL) {
//            [self loadImageWithURL:item.imageURL];
//          //  UIImage *image = [[UIImage imageWithData:[NSData dataWithContentsOfURL:item.imageURL]] roundedImage];
//           // self.imgView.image = image;
//        } else {
//           // UIImage *image = [[AppHelper imageFromUUIDString:item.image_url] roundedImage];
//          //  self.imgView.image = image;
//            [self loadLocalImageWithItem:item];
//        }
//        self.bottomLabel.hidden = YES;
//        self.priceLabel.hidden = YES;
//        self.topLabelTopConstraint.constant = 0;
//        self.topLabelTrailingConstraint.constant = 8;
//    }
    
    self.topLabel.text = item.name;

    self.actionButton.hidden = YES;
    
    if (onlySelection){
        NSArray *twoButtons = [self rightButtons];
        if (item.category.favoriteValue){
            UIButton *deleteButton = [twoButtons lastObject];
            [self setRightUtilityButtons:@[deleteButton] WithButtonWidth:90];
        }
        else {
            [self setRightUtilityButtons:[self rightButtons] WithButtonWidth:106];
        }
    }else {
        self.rightUtilityButtons = nil;
    }
}

- (void)loadShopImageWithURL:(NSURL*)url{
    [self.imgView sd_setImageWithURL:url completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (image){
            self.imgView.image = [image roundedImage];
            [self.imgView setNeedsDisplay];
        } else {
            UIImage *image = [UIImage imageWithColor:RGB(255, 230, 230) radius:40 rect:CGRectMake(0, 0, 80, 80)];
            self.imgView.image = image;
            [self.imgView setNeedsDisplay];
        }
    }];
}

- (void)loadImageWithURL:(NSURL*)url{
    NSString *stringFromURL = [url absoluteString];
    stringFromURL = [stringFromURL stringByReplacingOccurrencesOfString:@"images/" withString:@"images/icons/"];
    NSURL *newURL = [NSURL URLWithString:stringFromURL];
    [self.imgView sd_setImageWithURL:newURL completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (image){
            self.imgView.image = [image roundedImage];
            [self.imgView setNeedsDisplay];
        } else {
            UIImage *image = [UIImage imageWithColor:RGB(255, 230, 230) radius:40 rect:CGRectMake(0, 0, 80, 80)];
            self.imgView.image = image;
            [self.imgView setNeedsDisplay];
        }
    }];
}

- (void)loadLocalImageWithItem:(Item*)item{
    
    __weak typeof(self) aSelf = self;
    __block NSString *url = [item.image_url copy];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        UIImage *image = [[AppHelper imageFromUUIDString:url] roundedImage];
        
//        UIImage *image = [[UIImage imageWithData:[NSData dataWithContentsOfURL:item.imageURL]] roundedImage];
        dispatch_after_short(0, ^{
            aSelf.imgView.image = image;
            [aSelf.imgView setNeedsDisplay];
        });
    });
}

- (void)setupWithItem:(Item*)item shopCategory:(ShopCategory*)shopCategory
{
    [self setupWithItem:item mode:NO category:shopCategory];
}

- (void)setPriceArray:(NSMutableArray*)newArr{
    self.pArray = newArr;
}

- (void)addAddButton
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     RGB(48, 214, 93)
                                                title:@"Добавить"];
    UIButton *b = [rightUtilityButtons firstObject];
    [b.titleLabel setFont:[UIFont systemFontOfSize:15.0]];
    b.titleLabel.adjustsFontSizeToFitWidth = NO;
    
    self.rightUtilityButtons = rightUtilityButtons;
}

- (NSArray *)rightButtons
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor lightGrayColor]
                                                title:@"Переместить"];
    
    
    
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:1.0f green:0.231f blue:0.188 alpha:1.0f]
                                                title:@"Удалить"];
    
    for (UIButton *button in rightUtilityButtons)
    {
        [button.titleLabel setFont:[UIFont systemFontOfSize:15.0f]];
    }
    
    UIButton *first = [rightUtilityButtons firstObject];
    [first.titleLabel setAdjustsFontSizeToFitWidth:NO];
    
    return rightUtilityButtons;
}

@end
