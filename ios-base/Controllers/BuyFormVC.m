//
//  BuyFormVC.m
//  wardrobe
//
//  Created by Oleg Mytsouda on 24.11.15.
//  Copyright © 2015 Farcom. All rights reserved.
//

#import "BuyFormVC.h"
#import <MessageUI/MFMailComposeViewController.h>
#import <MessageUI/MessageUI.h>

static UIViewController *currentVC;

@interface BuyFormVC () <UITextFieldDelegate, MFMailComposeViewControllerDelegate>

@property (strong, nonatomic) IBOutlet UIImageView *itemImageView;
@property (strong, nonatomic) IBOutlet UILabel *shopLabel;
@property (strong, nonatomic) IBOutlet UILabel *itemLabel;
@property (strong, nonatomic) IBOutlet UILabel *oldPrice;
@property (strong, nonatomic) IBOutlet UIView *oldPriceView;
@property (strong, nonatomic) IBOutlet UILabel *currentPriceLabel;


@property (strong, nonatomic) IBOutlet UITextField *nameTF;
@property (strong, nonatomic) IBOutlet UITextField *phoneTF;
@property (strong, nonatomic) IBOutlet UITextField *cityTF;

@property (strong, nonatomic) NSString *nameText;
@property (strong, nonatomic) NSString *phoneText;
@property (strong, nonatomic) NSString *cityText;

@property (assign, nonatomic) int height;

@property (assign, nonatomic) BOOL animatedKeyboard;

- (IBAction)goToSiteAction:(id)sender;
- (IBAction)buyAction:(id)sender;

@end

@implementation BuyFormVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupDesign];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWillShow:(NSNotification *)notification
{
    CGRect keyboardBounds;
    [[notification.userInfo valueForKey:UIKeyboardFrameBeginUserInfoKey] getValue: &keyboardBounds];
    self.height = keyboardBounds.size.height;
    [self animateTextField:keyboardBounds.size.height up:YES];
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    [self animateTextField:self.height up:NO];
}

- (void) animateTextField:(int)height up:(BOOL)up
{
    int movementDistance = height;
    const float movementDuration = 0.3f;
    if (self.animatedKeyboard && up) {
        movementDistance = 0;
    }
    int movement = (up ? -movementDistance : movementDistance);
    [UIView animateWithDuration:movementDuration delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    } completion:nil];
    if (up) {
        self.animatedKeyboard = YES;
    } else {
        self.animatedKeyboard = NO;
    }
}

- (void)setupDesign
{
    [self showAvatar];
    self.shopLabel.text = self.item.shop_name;
    self.itemLabel.text = self.item.name;
    if ([self.item.oldPrice intValue] != 0) {
        self.oldPrice.text = [NSString stringWithFormat:@"%@ РУБ",[self formatWithThousandSeparator:[self.item.oldPrice intValue]]];
        self.oldPriceView.hidden = NO;
    } else {
        self.oldPriceView.hidden = YES;
    }
    self.currentPriceLabel.text = [NSString stringWithFormat:@"%@ РУБ",[self formatWithThousandSeparator:[self.item.price intValue]]];
}

-(NSString*)formatWithThousandSeparator:(NSInteger)number
{
    NSNumberFormatter* numberFormatter = [[NSNumberFormatter alloc] init];
    numberFormatter.usesGroupingSeparator = YES;
    numberFormatter.groupingSeparator = @" ";
    numberFormatter.groupingSize = 3;
    NSString *result = [numberFormatter stringFromNumber:[NSNumber numberWithInteger:number]];
    return result;
}

- (void)showAvatar
{
    self.itemImageView.image = nil;
    dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(q, ^{
        UIImage *newImage;
        NSString *url;
        if (![self.item.image_url hasPrefix:@"http://"] && ![self.item.image_url hasPrefix:@"https://"]) {
            NSString *stringFromURL = [NSString stringWithFormat:@"%@%@",DOMAIN_URL,self.item.image_url];
            url = stringFromURL;
        } else {
            url = [NSString stringWithFormat:@"%@",self.item.image_url];
        }
        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
        newImage = [UIImage imageWithData:imageData];
        dispatch_async(dispatch_get_main_queue(), ^{
            self.itemImageView.image  = newImage;
        });
        if (imageData == nil) {
            NSString *url = self.item.image_url;
            UIImage *image = [AppHelper imageFromUUIDString:url];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.itemImageView.image = image;
            });
        }
        
    });
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}

- (IBAction)goToSiteAction:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.item.site_url]];
}

- (IBAction)buyAction:(id)sender
{
    if (self.nameText != nil && ![self.nameText isEqualToString:@""] && ![self.nameText isEqualToString:@" "] && self.phoneText != nil && ![self.phoneText isEqualToString:@""] && ![self.phoneText isEqualToString:@" "] && self.cityText != nil && ![self.cityText isEqualToString:@""] && ![self.cityText isEqualToString:@" "]) {
        [self sendMailWithParametersViewController:self];
    } else {
        [[[UIAlertView alloc] initWithTitle:@"Ошибка" message:@"Заполните все поля" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }
}

- (void)sendMailWithParametersViewController:(UIViewController *)vc
{
    if ([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailComposer = [[MFMailComposeViewController alloc]init];
        [mailComposer setMailComposeDelegate:self];
        [mailComposer setToRecipients:[NSArray arrayWithObjects:self.senderEmail, nil]];
        [mailComposer setSubject:@"Заказ товара"];
        if (self.nameText.length > 0 && self.phoneText.length > 0 && self.cityText.length > 0) {
            NSString *mailerText = [NSString stringWithFormat:@"%@, %@, %@, %@",self.nameText, self.phoneText, self.cityText, self.item.site_url];
            [mailComposer setMessageBody:mailerText isHTML:YES];
        }
        if (self.itemImageView.image != nil) {
            NSData *attachment = UIImageJPEGRepresentation(self.itemImageView.image, 1);
            [mailComposer addAttachmentData:attachment mimeType:@"image/jpeg" fileName:@"tovar.jpg"];
        }
        [self presentViewController:mailComposer animated:YES completion:nil];
    } else {
        NSString *errorMessage = @"There are no Email accounts configured. You can add or create Email account in Settings.";
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:errorMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
    if (result) {
        NSLog(@"Result : %d",result);
    }
    if (error) {
        NSLog(@"Error : %@",error);
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
