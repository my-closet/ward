#import <UIKit/UIKit.h>

@interface WardrobeController : UICollectionViewController
+ (instancetype)controller;
@property (nonatomic, copy) ObjectCallback selectionBlock;
@end
