//
//  FavoritesCell.m
//  wardrobe
//
//  Created by Oleg Mytsouda on 26.11.15.
//  Copyright © 2015 Farcom. All rights reserved.
//

#import "FavoritesCell.h"

@implementation FavoritesCell


- (void)awakeFromNib
{

}

- (IBAction)likeButtonTapped:(id)sender
{
    [self.delegate sendLikeToPostWithCell:self];
}

@end
