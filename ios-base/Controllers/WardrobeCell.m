
#import "WardrobeCell.h"
@interface WardrobeCell() <UITextFieldDelegate, UIAlertViewDelegate>
@property (nonatomic, weak) IBOutlet UITextField *catTitle;
@property (nonatomic, weak) IBOutlet UILabel *itemsCount;
@property (nonatomic, weak) IBOutlet UIImageView *catImageView;
@property (nonatomic, weak) IBOutlet UIView *containerView;
@property (nonatomic, weak) IBOutlet UIButton *deleteButton;
@property (nonatomic, strong) Cat *category;
@end
@implementation WardrobeCell

- (void)setEditing:(BOOL)editing
{
    _editing = editing;
    self.deleteButton.hidden = !_editing;
    self.catTitle.enabled = _editing;
    if (_editing){
        self.catTitle.borderStyle = UITextBorderStyleRoundedRect;
    } else {
        self.catTitle.borderStyle = UITextBorderStyleNone;
    }
    self.itemsCount.alpha = !_editing;
    
    if ([_category favoriteValue]){
        self.catTitle.enabled = NO;
        self.catTitle.borderStyle = UITextBorderStyleNone;
        self.itemsCount.alpha = 1;
        self.deleteButton.hidden = YES;
    }
}

- (void)setupWithCategory:(Cat *)category
{
    self.category = category;
    self.catTitle.text = category.name;
    self.itemsCount.text = [NSString stringWithFormat:@"%d",(int)category.items.count];
    
    Item *lastItem = [category.items lastObject];
    UIImage *image = [AppHelper imageFromUUIDString:lastItem.image_url];
//    if (lastItem.imageURL != nil) {
//        self.catImageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:lastItem.imageURL]];
//    } else {
        if (image){
            self.catImageView.image = image;
        } else {
            self.catImageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:lastItem.imageURL]];
//            self.catImageView.image = [UIImage imageNamed:@"dress-placeholder"];
        }
    //}
}
- (void)awakeFromNib {
    self.containerView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.containerView.layer.borderWidth = ONE_PIXEL;
    self.containerView.layer.cornerRadius = 3.0f;
    self.containerView.clipsToBounds = YES;
    self.catTitle.delegate = self;
    
    [self.deleteButton addTarget:self action:@selector(deleteTapped) forControlEvents:UIControlEventTouchUpInside];
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if (textField.text.length == 0)
        self.catTitle.text = self.category.name;
    else{
        self.category.name = self.catTitle.text;
        [[self.category managedObjectContext] MR_saveToPersistentStoreWithCompletion:nil];
    }
    
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}


#pragma mark - Actions

- (void)deleteTapped
{
    NSString *message = [NSString stringWithFormat:@"Вы действительно хотите удалить категорию \"%@\"",self.category.name];
    [[[UIAlertView alloc] initWithTitle:nil
                               message:message
                               delegate:self cancelButtonTitle:@"Отмена" otherButtonTitles:@"Удалить", nil] show];
    
    
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([alertView cancelButtonIndex] != buttonIndex)
    {
        if ([self.delegate respondsToSelector:@selector(wardrobeCellDidDelete:)])
            [self.delegate wardrobeCellDidDelete:self];
    }
}


@end
