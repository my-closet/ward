#import "ShopsTableController.h"
#import "MainCell.h"
#import "ClothesTableController.h"
#import "Shop.h"
#import "ShopCategory.h"
#import "PECropViewController.h"
#import "Item.h"
#import "ItemEditController.h"
#import "MainViewController.h"
#import "PopupView.h"
#import "IDMPhotoBrowser.h"
#import "PreviewVC.h"
#import "ShopItemVC.h"

@interface ShopsTableController () < PECropViewControllerDelegate, SWTableViewCellDelegate, PopupViewDelegate, IDMPhotoBrowserDelegate>
@property (nonatomic, strong) NSArray *itemsArray;

@property (nonatomic, strong) NSMutableArray *oldPriceArray;
@property (nonatomic, strong) NSMutableArray *productUrlArray;
@property (nonatomic, strong) NSMutableArray *priceArray;

@end

@implementation ShopsTableController
{
    Item *_selectedItem;
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"";
    
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([MainCell class]) bundle:nil] forCellReuseIdentifier:@"MainCell"];
    
    self.tableView.tableFooterView = [UIView new];
    
    [SVProgressHUD show];
    
    if (self.mode == kShopModeShops){
        self.navigationItem.titleView = [[AppHelper sharedHelper] titleViewWithString:@"Магазины"];
        [[APIManager sharedManager] getShopsWithCompleteBlock:^(NSArray *resultArray) {
            self.itemsArray = resultArray;
            [self.tableView reloadData];
            [SVProgressHUD dismiss];
        }];
        
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addShopTapped)];
        
    } else if (self.mode == kShopModeCategories){
        self.navigationItem.titleView = [[AppHelper sharedHelper] titleViewWithString:@"Категории"];
        [[APIManager sharedManager] getCategoriesWithShop:self.shop completeBlock:^(NSArray *resultArray) {
            self.itemsArray = resultArray;
            [self.tableView reloadData];
            [SVProgressHUD dismiss];
        }];
    } else if (self.mode == kShopModeClothes){
        self.navigationItem.titleView = [[AppHelper sharedHelper] titleViewWithString:self.shop.name];
        [[APIManager sharedManager] getClothesWithShop:self.shop shopCategory:self.shopCategory completeBlock:^(NSArray *resultArray) {
            self.itemsArray = resultArray;
            [self getUrlArray];
            [self.tableView reloadData];
            [SVProgressHUD dismiss];            
        }];
    }
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.itemsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MainCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MainCell" forIndexPath:indexPath];
    
    id obj = self.itemsArray[indexPath.row];
    
    if ([obj isKindOfClass:[Shop class]])
        [cell setupWithShop:obj];
    else if ([obj isKindOfClass:[ShopCategory class]])
        [cell setupWithShopCategory:obj shop:self.shop];
    else if ([obj isKindOfClass:[Item class]]){
   //     [cell setPriceArray:self.priceArray];
        [cell setupWithItem:obj shopCategory:self.shopCategory];
   //     cell.priceLabel.text = [NSString stringWithFormat:@"%@ р.",[self formatWithThousandSeparator:[self.priceArray[indexPath.row] integerValue]]];
        [cell addAddButton];
        cell.delegate = self;
    }
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.mode == kShopModeShops){
        Shop *shop = self.itemsArray[indexPath.row];
        [[NSUserDefaults standardUserDefaults] setObject:shop.address forKey:@"shopURL"];
        [self showShopWithMode:kShopModeCategories shop:shop category:nil];
    }
    else if (self.mode == kShopModeCategories){
        ShopCategory *shopCategory = self.itemsArray[indexPath.row];
        Shop *shop = self.shop;
        [self showShopWithMode:kShopModeClothes shop:shop category:shopCategory];
    } else if (self.mode == kShopModeClothes){
        [self showPreviewWithItems:self.itemsArray currentIndex:(int)indexPath.row fromView:nil];
//        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Loading" message:nil
//                                                delegate:nil
//                                       cancelButtonTitle:nil
//                                       otherButtonTitles:nil, nil];
//        
//        UIActivityIndicatorView *loading = [[UIActivityIndicatorView alloc]
//                                            initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
//        //loading.frame=CGRectMake(150, 150, 16, 16);
//        [myAlertView addSubview:loading];
//        [myAlertView show];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)showShopWithMode:(kShopMode)mode shop:(Shop*)shop category:(ShopCategory*)category
{
    if (mode == kShopModeClothes) {
        NSMutableArray *sendItem = [NSMutableArray new];
        [sendItem addObject:shop];
        [sendItem addObject:category];
        [self performSegueWithIdentifier:@"ShopItem" sender:sendItem];
    } else {
        ShopsTableController *controller = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([ShopsTableController class])];
        controller.mode = mode;
        controller.shop = shop;
        controller.shopCategory = category;
        [self.navigationController pushViewController:controller animated:YES];
    }
}


#pragma mark - Swipe cell delegate

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index{
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    [self addItemAtIndex:(int)indexPath.row];
    [cell hideUtilityButtonsAnimated:YES];
}


#pragma mark - Adding Item to local items

- (void)addItemAtIndex:(int)index{
    
    Item *item = [self.itemsArray objectAtIndex:index];
    
    UIImage *image = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:[[item imageURL] absoluteString]];
    if (!image){
        [[[UIAlertView alloc] initWithTitle:nil
                                    message:@"Нельзя добавить вещь, потому что ее изображение отсутствует" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
        return;
    }
    
    
    PECropViewController *controller = [[PECropViewController alloc] init];
    controller.delegate = self;
    controller.image = image;
    controller.toolbarHidden = YES;
    controller.title = @"Выберите область";
    _selectedItem = item;
    
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:controller];
    [AppHelper setupNavigationController:nc];
    [self presentViewController:nc animated:YES completion:nil];
}

- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage{

    ItemEditController *editController = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([ItemEditController class])];
    editController.image = croppedImage;
    editController.shopItem = _selectedItem;
    
    [controller.navigationController pushViewController:editController animated:YES];
    _selectedItem = nil;
    
    UINavigationController *root = (UINavigationController*)ApplicationDelegate.window.rootViewController;
    
    MainViewController *vController = [[root viewControllers] firstObject];
    
    if ([vController isKindOfClass:[MainViewController class]])
    {
        editController.savedBlock = [vController itemAddToCanvasBlock];
    }
    
}

- (void)cropViewControllerDidCancel:(PECropViewController *)controller{
    _selectedItem = nil;
    [controller dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Add Shop

- (void)addShopTapped{
    PopupView *popup = [PopupView addShopView];
    popup.delegate = self;
    [popup showInView:self.navigationController.view];
}

- (void)popupView:(id)popupView clickedButtonAtIndex:(int)index{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:WEBSITE_URL]];
}


- (void)showPreviewWithItems:(NSArray*)items currentIndex:(int)index fromView:(UIView*)view{
    
//    NSMutableArray *photos = [NSMutableArray new];
//    for (Item *item in items){
//        IDMPhoto *photo;
//        if (item.uidValue < kLocalItemIDOffset){
//            photo = [[IDMPhoto alloc] initWithURL:[item imageURL]];
//        } else {
//            UIImage *image = [AppHelper imageFromUUIDString:item.image_url];
//            photo = [[IDMPhoto alloc] initWithImage:image];
//        }
//        [photos addObject:photo];
//    }
    
    //uncommit
   // [SVProgressHUD show];
    NSString *currentElementIndex = [NSString stringWithFormat:@"%d", index];
    
    NSMutableArray *sendItem = [NSMutableArray new];
    [sendItem addObject:currentElementIndex];
    [sendItem addObject:items];
    [sendItem addObject:self.shop];
    [sendItem addObject:self.shopCategory];
    [sendItem addObject:self.oldPriceArray];
    [sendItem addObject:self.productUrlArray];
    [sendItem addObject:self.priceArray];
    [self performSegueWithIdentifier:@"toPreview" sender:sendItem];
    
    //
//    
//    IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:photos animatedFromView:view];
//    
//    if (index < photos.count)
//        [browser setInitialPageIndex:index];
//    
//    browser.displayArrowButton = NO;
//    browser.displayCounterLabel = YES;
//    browser.navigationController.navigationBarHidden = NO;
//    browser.delegate = self;
//    browser.displayActionButton = NO;
//    
 //   [self.navigationController showViewController:browser sender:nil];
//    [self.navigationController pushViewController:browser animated:NO];
//    [self presentViewController:browser animated:YES completion:nil];
//    [ApplicationDelegate.window.rootViewController presentViewController:browser animated:YES completion:nil];
}


- (void)getUrlArray{
    self.oldPriceArray = [NSMutableArray new];
    self.productUrlArray = [NSMutableArray new];
    self.priceArray = [NSMutableArray new];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://admin.mycloset.ru/api.php?action=getProductsByCategoryIdAndShopId&category_id=%d&shop_id=%d",self.shopCategory.uidValue,self.shop.uidValue]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLResponse *response;
    NSError *error;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSDictionary *jsonObject=[NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:nil];
    if(!error)
    {
        jsonObject = [jsonObject valueForKey:@"results"];
        for (NSArray *arr in jsonObject){
            if (![[arr valueForKey:@"url"] isEqual:[NSNull null]]) {
                [self.productUrlArray addObject:[arr valueForKey:@"url"]];
            } else {
                [self.productUrlArray addObject:@"empty"];
            }
            
            if (![[arr valueForKey:@"oldprice"] isEqual:[NSNull null]]) {
                [self.oldPriceArray addObject:[arr valueForKey:@"oldprice"]];
            } else {
                [self.oldPriceArray addObject:@"empty"];
            }
            
            if (![[arr valueForKey:@"price"] isEqual:[NSNull null]]) {
                [self.priceArray addObject:[arr valueForKey:@"price"]];
            } else {
                [self.priceArray addObject:@"empty"];
            }

        }
    }
}


-(NSString*)formatWithThousandSeparator:(NSInteger)number
{
    NSNumberFormatter* numberFormatter = [[NSNumberFormatter alloc] init];
    numberFormatter.usesGroupingSeparator = YES;
    numberFormatter.groupingSeparator = @" ";
    numberFormatter.groupingSize = 3;
    NSString *result = [numberFormatter stringFromNumber:[NSNumber numberWithInteger:number]];
    return result;
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"toPreview"]) {
        PreviewVC *vc = (PreviewVC*)segue.destinationViewController;
       // [vc performSelectorInBackground:@selector(getUrlArray) withObject:nil];
        vc.navBarTitle = self.shop.name;
        vc.photos = [sender objectAtIndex:1];
        vc.index = [[sender objectAtIndex:0] intValue];
        vc.shop = [sender objectAtIndex:2];
        vc.shopCategory = [sender objectAtIndex:3];
        vc.oldPriceArray = [NSMutableArray new];
        vc.oldPriceArray = [sender objectAtIndex:4];
        vc.productUrlArray = [NSMutableArray new];
        vc.productUrlArray = [sender objectAtIndex:5];
        vc.priceArray = [NSMutableArray new];
        vc.priceArray = [sender objectAtIndex:6];
    } else if ([segue.identifier isEqualToString:@"ShopItem"]){
        ShopItemVC *vc = (ShopItemVC*)segue.destinationViewController;
        vc.navBarTitle = self.shop.name;
        vc.shop = [sender objectAtIndex:0];
        vc.shopCategory = [sender objectAtIndex:1];
    }
}


- (void)photoBrowser:(IDMPhotoBrowser *)photoBrowser didDismissAtPageIndex:(NSUInteger)index{
    if (photoBrowser.addTapped)
        [self addItemAtIndex:(int)index];
}

@end
