//
//  FakeShopItemVC.m
//  wardrobe
//
//  Created by Oleg Mytsouda on 03.05.16.
//  Copyright © 2016 Farcom. All rights reserved.
//

#import "FakeShopItemVC.h"
#import "CollectionShopItem.h"
#import "PreviewVC.h"
#import "ClotheVC.h"
#import "FilterVC.h"

@interface FakeShopItemVC () <UICollectionViewDataSource, UICollectionViewDelegate, FiltersDelegate>

@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;

@property (strong, nonatomic) IBOutlet UIButton *filtersButton;
@property (strong, nonatomic) IBOutlet UIButton *clothing;
@property (strong, nonatomic) IBOutlet UILabel *filtersTitle;

@property (assign, nonatomic) BOOL isFromFilters;

@property (strong, nonatomic) NSMutableArray *itemsArray;

- (IBAction)filtersAction:(id)sender;
- (IBAction)clothingAction:(id)sender;

@property (assign, nonatomic) int pageNumber;

@end

@implementation FakeShopItemVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialization];
    [self setupButtonsDesign];
    self.title = @"Магазины";
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.pageNumber = 1;
    [self getItemsWithPage:self.pageNumber];
}

- (void)setupButtonsDesign
{
    if (self.shopCategory.name != nil) {
        self.filtersTitle.text = [NSString stringWithFormat:@"%@",self.shopCategory.name];
    } else {
        self.filtersTitle.text = @"Все разделы";
    }
    
    self.filtersButton.layer.borderWidth = 1.f;
    self.filtersButton.layer.borderColor = [UIColor whiteColor].CGColor;
    self.filtersButton.layer.cornerRadius = 6.f;
    
    self.clothing.layer.borderWidth = 1.0f;
    self.clothing.layer.borderColor = [UIColor whiteColor].CGColor;
    self.clothing.layer.cornerRadius = 6.f;
}

- (void)initialization
{
    self.itemsArray = [NSMutableArray new];
}

- (void)getItemsWithPage:(int)page
{
    self.pageNumber = page;
    [[APIManager sharedManager]getPopularPage:[NSString stringWithFormat:@"%i",page] WithCompleteBlockAndHandler:^(id errorMsg, id response) {
        if (!errorMsg) {
            [self.itemsArray addObjectsFromArray:response];
            [self.collectionView reloadData];
            [SVProgressHUD dismiss];
        }
    }];
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.itemsArray count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    
    CollectionShopItem *cell = [[CollectionShopItem alloc]init];
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    Item *newItem = self.itemsArray[indexPath.row];
    
    cell.nameM.text = newItem.shop_name;
    cell.priceValue.text = [[AppHelper sharedHelper] currencyStringFromString:newItem.price];
    
    NSString *stringFromURL;
    if (![newItem.image_url hasPrefix:@"http://"] && ![newItem.image_url hasPrefix:@"https://"]) {
        stringFromURL = [NSString stringWithFormat:@"%@%@",DOMAIN_URL,newItem.image_url];
        [newItem imageURLFromStr:stringFromURL];
    } else {
        stringFromURL = newItem.image_url;
    }
    if (newItem.uidValue < kLocalItemIDOffset){
        [cell loadShopImageWithURL:[newItem imageURLFromStr:stringFromURL]];
    } else {
        [cell loadLocalImageWithItem:newItem];
    }
    
    if (indexPath.row + 2 > self.itemsArray.count) {
        int currentPageIndex = self.itemsArray.count / 50;
        if (self.pageNumber == currentPageIndex) {
            [self getItemsWithPage:currentPageIndex + 1];
        }
    }
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.view.frame.size.width/2 - 10, 220);;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    NSString *currentElementIndex = [NSString stringWithFormat:@"%ld", (long)indexPath.row];
    
    NSMutableArray *sendItem = [NSMutableArray new];
    [sendItem addObject:currentElementIndex];
    [sendItem addObject:self.itemsArray];
   // [sendItem addObject:self.shop];
   // [sendItem addObject:self.shopCategory];
    [self performSegueWithIdentifier:@"toPreview" sender:sendItem];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"toPreview"]) {
        PreviewVC *vc = (PreviewVC*)segue.destinationViewController;
      //  vc.navBarTitle = self.shop.name;
        vc.photos = [sender objectAtIndex:1];
        vc.index = [[sender objectAtIndex:0] intValue];
        //vc.shop = [sender objectAtIndex:2];
       // vc.shopCategory = [sender objectAtIndex:3];
    } else if ([segue.identifier isEqualToString:@"toclothe"]) {
        ClotheVC *clothe = segue.destinationViewController;
        if (sender != nil) {
            clothe.shop = [sender valueForKey:@"shop"];
        }
    } else if ([segue.identifier isEqualToString:@"toFilters"]) {
        FilterVC *filter = segue.destinationViewController;
        if (sender != nil) {
            filter.shop = [sender valueForKey:@"shop"];
        }
        filter.delegate = self;
    }
}

-(void)returnFiltersValue:(NSMutableArray *)filters
{
    self.isFromFilters = YES;
    [SVProgressHUD show];
    NSString *filter = [filters componentsJoinedByString:@"?"];
    [[APIManager sharedManager] getClothesWithShop:self.shop shopCategory:[NSString stringWithFormat:@"%d",self.shopCategory.uidValue] andFilters:filter completeBlock:^(NSArray *resultArray) {
        [self.itemsArray addObjectsFromArray:resultArray];
        [self.collectionView reloadData];
        [SVProgressHUD dismiss];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)filtersAction:(id)sender
{
    if (self.shop != nil) {
        NSDictionary * senderDict = @{@"shop":self.shop};
        [self performSegueWithIdentifier:@"toFilters" sender:senderDict];
    } else {
        [self performSegueWithIdentifier:@"toFilters" sender:nil];
    }
}

- (IBAction)clothingAction:(id)sender
{
    if (self.shop != nil) {
        NSDictionary * senderDict = @{@"shop":self.shop};
        [self performSegueWithIdentifier:@"toclothe" sender:senderDict];
    } else {
        [self performSegueWithIdentifier:@"toclothe" sender:nil];
    }
}

@end
