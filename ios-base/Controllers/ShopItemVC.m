//
//  ShopItemVC.m
//  wardrobe
//
//  Created by Oleg Mytsouda on 18.01.16.
//  Copyright © 2016 Farcom. All rights reserved.
//

#import "ShopItemVC.h"
#import "CollectionShopItem.h"
#import "PreviewVC.h"
#import "ClotheVC.h"
#import "FilterVC.h"

@interface ShopItemVC () <UICollectionViewDataSource, UICollectionViewDelegate, FiltersDelegate>

@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;

@property (strong, nonatomic) IBOutlet UIButton *filtersButton;
@property (strong, nonatomic) IBOutlet UIButton *clothing;
@property (strong, nonatomic) IBOutlet UILabel *filtersTitle;

@property (assign, nonatomic) BOOL isFromFilters;

@property (strong, nonatomic) NSArray *itemsArray;

- (IBAction)filtersAction:(id)sender;
- (IBAction)clothingAction:(id)sender;

@end

@implementation ShopItemVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialization];
    [self setupButtonsDesign];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self getItems];
}

- (void)setupButtonsDesign
{
    self.filtersTitle.text = [NSString stringWithFormat:@"%@",self.shopCategory.name];
    
    self.filtersButton.layer.borderWidth = 1.f;
    self.filtersButton.layer.borderColor = [UIColor whiteColor].CGColor;
    self.filtersButton.layer.cornerRadius = 6.f;
    
    self.clothing.layer.borderWidth = 1.0f;
    self.clothing.layer.borderColor = [UIColor whiteColor].CGColor;
    self.clothing.layer.cornerRadius = 6.f;
}

- (void)initialization
{
    self.itemsArray = [NSArray new];
}

- (void)getItems
{
    self.navigationItem.titleView = [[AppHelper sharedHelper] titleViewWithString:@"Магазины"];
    if (!self.isFromFilters) {
        [SVProgressHUD show];
        if (![[[NSUserDefaults standardUserDefaults] valueForKey:@"getCategoryProductWithID"] isEqualToString:@""] && [[NSUserDefaults standardUserDefaults] valueForKey:@"getCategoryProductWithID"] != nil) {
            [[APIManager sharedManager] getClothesWithShop:self.shop shopCategory:[[NSUserDefaults standardUserDefaults] valueForKey:@"getCategoryProductWithID"] completeBlock:^(NSArray *resultArray) {
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"getCategoryProductWithID"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                self.itemsArray = resultArray;
                [self.collectionView reloadData];
                [SVProgressHUD dismiss];
            }];
        } else {
            NSString *shopCatUID = [NSString stringWithFormat:@"%d",self.shopCategory.uidValue];
            [[APIManager sharedManager] getClothesWithShop:self.shop shopCategory:shopCatUID completeBlock:^(NSArray *resultArray) {
                self.itemsArray = resultArray;
                [self.collectionView reloadData];
                [SVProgressHUD dismiss];
            }];
        }
    }
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.itemsArray count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    
    CollectionShopItem *cell = [[CollectionShopItem alloc]init];
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    Item *newItem = self.itemsArray[indexPath.row];
    
    cell.nameM.text = self.shop.name;
    cell.priceValue.text = [[AppHelper sharedHelper] currencyStringFromString:newItem.price];

    NSString *stringFromURL;
    if (![newItem.image_url hasPrefix:@"http://"] && ![newItem.image_url hasPrefix:@"https://"]) {
        stringFromURL = [NSString stringWithFormat:@"%@%@",DOMAIN_URL,newItem.image_url];
        [newItem imageURLFromStr:stringFromURL];
    } else {
        stringFromURL = newItem.image_url;
    }
    if (newItem.uidValue < kLocalItemIDOffset){
        [cell loadShopImageWithURL:[newItem imageURLFromStr:stringFromURL]];
    } else {
        [cell loadLocalImageWithItem:newItem];
    }

    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.view.frame.size.width/2 - 10, 220);;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    NSString *currentElementIndex = [NSString stringWithFormat:@"%d", indexPath.row];
    
    NSMutableArray *sendItem = [NSMutableArray new];
    [sendItem addObject:currentElementIndex];
    [sendItem addObject:self.itemsArray];
    [sendItem addObject:self.shop];
    [sendItem addObject:self.shopCategory];
    [self performSegueWithIdentifier:@"toPreview" sender:sendItem];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"toPreview"]) {
        PreviewVC *vc = (PreviewVC*)segue.destinationViewController;
        vc.navBarTitle = self.shop.name;
        vc.photos = [sender objectAtIndex:1];
        vc.index = [[sender objectAtIndex:0] intValue];
        vc.shop = [sender objectAtIndex:2];
        vc.shopCategory = [sender objectAtIndex:3];
    } else if ([segue.identifier isEqualToString:@"toclothe"]) {
        ClotheVC *clothe = segue.destinationViewController;
        clothe.shop = [sender valueForKey:@"shop"];
    } else if ([segue.identifier isEqualToString:@"toFilters"]) {
        FilterVC *filter = segue.destinationViewController;
        filter.shop = [sender valueForKey:@"shop"];
        filter.delegate = self;
    }
}

-(void)returnFiltersValue:(NSMutableArray *)filters
{
    self.isFromFilters = YES;
    [SVProgressHUD show];
    NSString *filter = [filters componentsJoinedByString:@"?"];
    [[APIManager sharedManager] getClothesWithShop:self.shop shopCategory:[NSString stringWithFormat:@"%d",self.shopCategory.uidValue] andFilters:filter completeBlock:^(NSArray *resultArray) {
        self.itemsArray = resultArray;
        [self.collectionView reloadData];
        [SVProgressHUD dismiss];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)filtersAction:(id)sender
{
    NSDictionary * senderDict = @{@"shop":self.shop, @"category":self.shopCategory};
    [self performSegueWithIdentifier:@"toFilters" sender:senderDict];
}

- (IBAction)clothingAction:(id)sender
{
    NSDictionary * senderDict = @{@"shop":self.shop, @"category":self.shopCategory};
    [self performSegueWithIdentifier:@"toclothe" sender:senderDict];
}

@end
