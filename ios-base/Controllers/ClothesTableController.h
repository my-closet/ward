
#import <UIKit/UIKit.h>

@interface ClothesTableController : UITableViewController
- (void)setupWithShop:(id)shop;
- (void)setupWithCategory:(Cat*)category;
@property (nonatomic, copy) ObjectCallback selectionBlock;
@end
