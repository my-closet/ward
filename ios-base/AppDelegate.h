
#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

- (UIViewController*)topViewController;
- (UIViewController*)topViewControllerWithRootViewController:(UIViewController*)rootViewController;

@end

