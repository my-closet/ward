
#import "CanvasItem.h"
@interface CanvasItem() <UIGestureRecognizerDelegate, UIScrollViewDelegate>
@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) IBOutlet UIButton *deleteButton;
@property (weak, nonatomic) IBOutlet UIButton *buyButton;
@property (nonatomic, strong) IBOutlet UIImageView *topMover;
@property (nonatomic, strong) IBOutlet UIImageView *bottomMover;
@property (nonatomic, strong, readwrite) Item *item;
@property (nonatomic, strong) UIGestureRecognizer *tapGesture;
@property (nonatomic, strong) UIPanGestureRecognizer *panGesture;
@property (nonatomic, strong) UIPanGestureRecognizer *resizeGestureTop;
@property (nonatomic, strong) UIPanGestureRecognizer *resizeGestureBottom;

@property (nonatomic, strong) NSMutableDictionary *objectValueDefaults;

- (IBAction)buyButtonAction:(id)sender;

@end

#define kCanvasMinimumHeight 60
@implementation CanvasItem
{
    CGPoint _panCoord;
    CGRect _startFrame;
    CGPoint _resizeCoord;
    
    CGFloat _maximumHeight;
    BOOL _hasObserver;
}
- (void)dealloc
{
    [self removeObserversSafely];
}
- (void)removeObserversSafely
{
    if (_hasObserver){
        _hasObserver = NO;
        [self.scrollView removeObserver:self forKeyPath:@"contentOffset"];
    }
}
- (void)addObserversSafely
{
    if (!_hasObserver){
        _hasObserver = YES;
        [self.scrollView addObserver:self forKeyPath:@"contentOffset" options:NSKeyValueObservingOptionNew context:NULL];
    }
    
}
- (void)removeFromSuperview
{
    [self removeObserversSafely];
    [super removeFromSuperview];
}

- (void)setLinesHidden:(BOOL)hidden
{
    [self viewWithTag:21].hidden = hidden;
    [self viewWithTag:22].hidden = hidden;
    
    if (hidden){
        self.topMover.hidden = YES;
        self.bottomMover.hidden = YES;
    } else {
        self.topMover.hidden = !_active;
        self.bottomMover.hidden = !_active;
    }
}
- (void)setEditable:(BOOL)editable
{
    _editable = editable;

    self.scrollView.scrollEnabled = _editable;
    self.panGesture.enabled = _editable;
    self.tapGesture.enabled = _editable;
    self.resizeGestureTop.enabled = _editable;
    self.resizeGestureBottom.enabled = _editable;
    
    if (!_editable){
        self.active = NO;
    }
}
- (void)setActive:(BOOL)active
{
    _active = active;
    self.topMover.hidden = !active;
    self.bottomMover.hidden = !active;
    if (!active)
        [_scrollView setContentOffset:CGPointZero animated:YES];
}

- (void)didMoveToSuperview{
    [super didMoveToSuperview];
    
    _maximumHeight = self.superview.height - 2*kCanvasTopBottomPadding;
}

+ (instancetype)itemWithItem:(Item*)item
{
    CanvasItem *canvasItem = [[Bundle loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil] firstObject];
    [canvasItem setupWithItem:item];
    return canvasItem;
}
- (void)setupWithItem:(Item*)item
{
    self.item = item;
    
//    [self addSubview:self.informView];
//    [self bringSubviewToFront:self.informView];
    UIImage *image = [AppHelper imageFromUUIDString:item.image_url];
    if (image == nil) {
        // check
        if (![item.image_url hasPrefix:@"http://"] && ![item.image_url hasPrefix:@"https://"]) {
            image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",DOMAIN_URL,item.image_url]]]];
        } else {
            image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",item.image_url]]]];
        }
//        
//        image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",DOMAIN_URL,item.image_url]]]];
    }
    
    CGFloat heightShouldBe = (self.width / image.size.width-90) * image.size.height;
    
    heightShouldBe = ceilf(MIN(self.width * 1.1f, heightShouldBe));
    
    if (heightShouldBe > 0 && heightShouldBe < 10000){
        self.height = (float)heightShouldBe;
    } else {
        self.height = 300.0f;
    }
    
//    if (self.newFrame != nil) {
//  //  self.frame = *(self.newFrame);
//    }
    
    self.scrollView.delegate = self;
    self.scrollView.pagingEnabled = NO;
    
    self.userInteractionEnabled = YES;
    self.imageView.image = image;
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(dragging:)];
    pan.maximumNumberOfTouches = 1;
    pan.minimumNumberOfTouches = 1;
    self.panGesture = pan;
    self.panGesture.delegate = self;
    [self addGestureRecognizer:pan];
    
    UIPanGestureRecognizer *resizePan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(resizePanRecognized:)];
    resizePan.maximumNumberOfTouches = 1;
    resizePan.minimumNumberOfTouches = 1;
    self.resizeGestureTop = resizePan;
    [self.topMover addGestureRecognizer:resizePan];

    UIPanGestureRecognizer *resizePanBottom = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(resizePanRecognized:)];
    resizePanBottom.maximumNumberOfTouches = 1;
    resizePanBottom.minimumNumberOfTouches = 1;
    self.resizeGestureBottom = resizePanBottom;
    [self.bottomMover addGestureRecognizer:resizePanBottom];
    
    self.imageView.backgroundColor = [UIColor whiteColor];
    //self.imageView.backgroundColor = CANVAS_BG_COLOR;
    self.imageView.frame = self.scrollView.bounds;
    
    self.deleteButton.frame = CGRectMake(self.width, 0, 60, self.scrollView.height);

   // self.buyButton.frame = CGRectMake(self.width, 0, 60, self.scrollView.height/2);
    //self.deleteButton.frame = CGRectMake(self.width, self.scrollView.height/2, 60, self.scrollView.height/2);
    
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.showsVerticalScrollIndicator = NO;
    
    [self setNeedsLayout];
    
    self.tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapped:)];
    [self addGestureRecognizer:self.tapGesture];
    
    self.scrollView.backgroundColor = [UIColor whiteColor];
    
    [self.deleteButton addTarget:self action:@selector(deleteTapped) forControlEvents:UIControlEventTouchUpInside];
    
    [self addObserversSafely];
    
    self.imageView.userInteractionEnabled = YES;
    
    self.scrollView.alwaysBounceHorizontal = YES;
}
- (void)tapped:(UIGestureRecognizer*)tapGesture
{
    if ([self.delegate respondsToSelector:@selector(canvasItemDidBeginEditing:)])
        [self.delegate canvasItemDidBeginEditing:self];
}
-(void)dragging:(UIPanGestureRecognizer *)gesture
{
    if(gesture.state == UIGestureRecognizerStateBegan)
    {
        //NSLog(@"Received a pan gesture");
        _panCoord = [gesture locationInView:gesture.view];
        if ([self.delegate respondsToSelector:@selector(canvasItemDidBeginEditing:)])
            [self.delegate canvasItemDidBeginEditing:self];
    }

    CGPoint newCoord = [gesture locationInView:gesture.view];
    float dY = newCoord.y - _panCoord.y;
    
    CGFloat candidateY = gesture.view.y + dY;
    
    if (candidateY < - kCanvasTopBottomPadding) candidateY = - kCanvasTopBottomPadding;
    if (candidateY + gesture.view.height > self.superview.height + kCanvasTopBottomPadding) candidateY = self.superview.height - gesture.view.height + kCanvasTopBottomPadding;
    
    self.y = candidateY;
}
- (void)resizePanRecognized:(UIPanGestureRecognizer*)gesture
{
    if(gesture.state == UIGestureRecognizerStateBegan)
    {
        //NSLog(@"Received a pan gesture");
        _resizeCoord = [gesture locationInView:ApplicationDelegate.window.rootViewController.view];
        if ([self.delegate respondsToSelector:@selector(canvasItemDidBeginEditing:)])
            [self.delegate canvasItemDidBeginEditing:self];
        _startFrame = self.frame;
    }
    
    CGPoint newCoord = [gesture locationInView:ApplicationDelegate.window.rootViewController.view];
    float dY = newCoord.y - _resizeCoord.y;
    
    if (gesture.view == self.topMover || gesture.view == self.bottomMover){
        
        if (gesture.view == self.bottomMover)
            dY = - dY;
        
        CGFloat candidateY = _startFrame.origin.y + dY;
        if (candidateY < - kCanvasTopBottomPadding) candidateY = - kCanvasTopBottomPadding;
        if (candidateY + self.height > self.superview.height + kCanvasTopBottomPadding) candidateY = self.superview.height - self.height + kCanvasTopBottomPadding;
        
      //  if (![[NSUserDefaults standardUserDefaults] boolForKey:@"cantEditing"]) {
            CGFloat newHeight = _startFrame.size.height - dY;
            
            if (newHeight > kCanvasMinimumHeight && newHeight < _maximumHeight){
                NSLog(@"dy:%f newheight:%f",dY, newHeight);
                if (gesture.view == self.bottomMover)
                {
                    candidateY = self.y;
                }
                self.frame = CGRectMake(0, candidateY, self.superview.width, newHeight);
            }
       // }
        
//        CGFloat newHeight = _startFrame.size.height - dY;
//        
//        if (newHeight > kCanvasMinimumHeight && newHeight < _maximumHeight){
//            NSLog(@"dy:%f newheight:%f",dY, newHeight);
//            if (gesture.view == self.bottomMover)
//            {
//                candidateY = self.y;
//            }
//            self.frame = CGRectMake(0, candidateY, self.superview.width, newHeight);
//        }
    }
    if (gesture.view == self.bottomMover)
    {
        
    }
}
- (void)layoutSubviews
{
   // [self bringSubviewToFront:self.leftImage];
//
    
    [self.imageView insertSubview:self.leftImage atIndex:0];
    [self.imageView insertSubview:self.rightImage atIndex:0];
    [self.imageView insertSubview:self.informView atIndex:0];

//    [self.imageView insertSubview:<#(nonnull UIView *)#> atIndex:<#(NSInteger)#>]
    
    self.imageView.frame = self.scrollView.bounds;

    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"cantEditing"]) {
        
    self.deleteButton.frame = CGRectMake(self.width, 0, 60, self.scrollView.height);
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"isFromShop"] != nil) {
        self.objectValueDefaults = [NSMutableDictionary new];
        if ([[NSUserDefaults standardUserDefaults] dictionaryForKey:@"buyProduction"] != nil) {
            self.objectValueDefaults = [[NSUserDefaults standardUserDefaults] valueForKey:@"buyProduction"];
        }
        NSMutableArray *newArray = [[NSMutableArray alloc] initWithObjects:self.item.uid,[[NSUserDefaults standardUserDefaults] objectForKey:@"productUrl"], nil];
        NSString *uID  = [NSString stringWithFormat:@"%@",self.item.uid];
        NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:self.objectValueDefaults];
        [dict setValue:newArray forKey:uID];
        [[NSUserDefaults standardUserDefaults] setObject:dict forKey:@"buyProduction"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"isFromShop"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
//    self.deleteButton.frame = CGRectMake(self.width, 0, 60, self.scrollView.height);
    if ([[NSUserDefaults standardUserDefaults] dictionaryForKey:@"buyProduction"] != nil) {
        NSDictionary *checkForBuyButton = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"buyProduction"];
        NSArray *checkArr = [checkForBuyButton allKeys];
        for (int i = 0; i < checkArr.count; i++) {
            if ([[checkArr objectAtIndex:i] isEqualToString:[NSString stringWithFormat:@"%@", self.item.uid]]) {
                    self.buyButton.frame = CGRectMake(self.width, 0, 60, self.scrollView.height/2);
                    self.deleteButton.frame = CGRectMake(self.width, self.scrollView.height/2, 60, self.scrollView.height/2);
            }
        }
    }
        
//        
//        if ([checkArr containsObject:[NSString stringWithFormat:@"%@", self.item.uid]]) {
//            self.buyButton.frame = CGRectMake(self.width, 0, 60, self.scrollView.height);
//        } else {
//            self.deleteButton.frame = CGRectMake(self.width, 0, 60, self.scrollView.height);
//
//        }
//        
//        for (NSString* check in checkArr) {
//            if ([check isEqualToString:[NSString stringWithFormat:@"%@", self.item.uid]]) {
//                self.buyButton.frame = CGRectMake(self.width, 0, 60, self.scrollView.height);
//                //self.deleteButton.frame = CGRectMake(self.width, self.scrollView.height/2, 60, self.scrollView.height/2);
//                break;
//            } else {
//                self.deleteButton.frame = CGRectMake(self.width, 0, 60, self.scrollView.height);
//                break;
//            }
//        }
    } else {
        self.buyButton.frame = CGRectMake(self.width, 0, 0, self.scrollView.height/2);
        self.deleteButton.frame = CGRectMake(self.width, self.scrollView.height/2, 0, self.scrollView.height/2);
        self.deleteButton.hidden = YES;
        self.buyButton.hidden = YES;
    }
    
   // self.deleteButton.frame = CGRectMake(self.width, 0, 60, self.scrollView.height);
//    self.buyButton.frame = CGRectMake(self.width, 0, 60, self.scrollView.height/2);
//    self.deleteButton.frame = CGRectMake(self.width, self.scrollView.height/2, 60, self.scrollView.height/2);
    
    self.scrollView.frame = CGRectMake(0, kCanvasTopBottomPadding, self.width, self.height - 2*kCanvasTopBottomPadding);
    
    self.scrollView.contentSize = CGSizeMake(self.deleteButton.width + self.width, self.scrollView.height);
    self.scrollView.contentSize = CGSizeMake(self.buyButton.width + self.width, self.scrollView.height);
    
    [self.scrollView bringSubviewToFront:self.imageView];
    
    UIView *topLine = [self viewWithTag:21];
    UIView *bottomLine = [self viewWithTag:22];
    topLine.y = kCanvasTopBottomPadding;
    topLine.height = ONE_PIXEL;
    
    bottomLine.y = self.height - kCanvasTopBottomPadding;
    bottomLine.height = ONE_PIXEL;
    
    [super layoutSubviews];
    
    NSLog(@"canvas item:%@",self);
}

- (void)deleteTapped
{
    if ([self.delegate respondsToSelector:@selector(canvasItemDidDelete:)])
        [self.delegate canvasItemDidDelete:self];
}

#pragma mark - Gesture Delegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if (gestureRecognizer == self.panGesture)
    {
        CGPoint top = [touch locationInView:self.topMover];
        CGPoint bottom = [touch locationInView:self.bottomMover];
        
       // if (![[NSUserDefaults standardUserDefaults] boolForKey:@"cantEditing"]) {

        
        if (CGRectContainsPoint(self.topMover.bounds, top) ||
            CGRectContainsPoint(self.bottomMover.bounds, bottom))
            return NO;
       // }
    }
    return YES;
}

#pragma mark - Scroll Delegate

-(void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    NSLog(@"currentx:%f targetX:%f velocityx:%f",scrollView.contentOffset.x,targetContentOffset->x,velocity.x);
    
    CGPoint p;
    
    //if (![[NSUserDefaults standardUserDefaults] boolForKey:@"cantEditing"]) {

    
    if (scrollView.contentOffset.x > self.deleteButton.width /2)
    {
        p = CGPointMake(self.deleteButton.width, 0);
        targetContentOffset = &p;
    } else {
        p = CGPointZero;
        targetContentOffset = &p;
    }
    
//    }
    
    if(velocity.x == 0)
        [scrollView setContentOffset:p animated:YES];
}
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if ([self.delegate respondsToSelector:@selector(canvasItemDidBeginEditing:)])
        [self.delegate canvasItemDidBeginEditing:self];
}

#pragma mark - Observe
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"contentOffset"])
    {
      //  if (![[NSUserDefaults standardUserDefaults] boolForKey:@"cantEditing"]) {

        CGFloat x = [[change objectForKey:@"new"] CGPointValue].x;
        self.deleteButton.x = self.width - self.deleteButton.width + x;
        self.buyButton.x = self.width - self.deleteButton.width + x;
       // }
    }
}


- (IBAction)buyButtonAction:(id)sender {
    NSMutableDictionary *checDict = [[NSUserDefaults standardUserDefaults] objectForKey:@"buyProduction"];
    NSString *key = [NSString stringWithFormat:@"%@",self.item.uid];
//    if ([checDict objectForKey:key]) {
//        NSString *sourceURL = [NSString stringWithFormat:@"%@",[[checDict objectForKey:key] objectAtIndex:1]];
//        if ([sourceURL isEqualToString:@"empty"]) {
//            [[[UIAlertView alloc] initWithTitle:@"Ошибка" message:@"Извините на даний момент у нас нет ссылки на эту вещь." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
//        } else {
//            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:sourceURL]];
//        }
//    }
 
    if ([self.delegate respondsToSelector:@selector(stopEditing)]){
        [self.delegate stopEditing];
        [self.delegate goToBuyForm:self.item];
    }
}


@end
