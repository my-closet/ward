
#import "PopupView.h"

@interface PopupView() < UIGestureRecognizerDelegate>
@property (nonatomic, strong) UIView *dimView;
- (IBAction)closeTapped;
@end
@implementation PopupView

+ (instancetype)popupView
{
    PopupView *popup = [[Bundle loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil] firstObject];
    return popup;
}

+ (instancetype)addShopView
{
    PopupView *popup = [[Bundle loadNibNamed:@"AddShopView" owner:nil options:nil] firstObject];
    return popup;
}

+ (instancetype)cameraView{
    PopupView *popup = [[Bundle loadNibNamed:@"AddPhotoView" owner:nil options:nil] firstObject];
    return popup;
}

- (void)showInView:(UIView *)view
{
    UIView *dimView = [UIView new];
    dimView.frame = view.bounds;
    dimView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.7];
    [view addSubview:dimView];
    dimView.userInteractionEnabled = YES;
    dimView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [dimView setTranslatesAutoresizingMaskIntoConstraints:YES];
    
    [dimView addSubview:self];
    self.width = view.width - 20*2;
    self.center = CGPointMake(dimView.width/2.0, dimView.height/2.0);
    self.autoresizingMask = UIViewAutoresizingNone;
    [self setTranslatesAutoresizingMaskIntoConstraints:YES];

    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dimViewTapped)];
    gesture.delegate = self;
    [dimView addGestureRecognizer:gesture];
    
    self.dimView = dimView;
    
    UIButton *b1 = (UIButton*)[self viewWithTag:10];
    [b1 setBackgroundImage:[UIImage imageWithColor:RGB(245, 245, 245)] forState:UIControlStateHighlighted];

    UIButton *b2 = (UIButton*)[self viewWithTag:11];
    [b2 setBackgroundImage:[UIImage imageWithColor:RGB(245, 245, 245)] forState:UIControlStateHighlighted];

    UIButton *b3 = (UIButton*)[self viewWithTag:12];
    [b3 setBackgroundImage:[UIImage imageWithColor:RGB(245, 245, 245)] forState:UIControlStateHighlighted];
    
    UIButton *b4 = (UIButton*)[self viewWithTag:13];
    [b4 setBackgroundImage:[UIImage imageWithColor:RGB(245, 245, 245)] forState:UIControlStateHighlighted];

    UIButton *b5 = (UIButton*)[self viewWithTag:14];
    [b5 setBackgroundImage:[UIImage imageWithColor:RGB(245, 245, 245)] forState:UIControlStateHighlighted];
    
    UIButton *b6 = (UIButton*)[self viewWithTag:15];
    [b6 setBackgroundImage:[UIImage imageWithColor:RGB(245, 245, 245)] forState:UIControlStateHighlighted];

    UIButton *b7 = (UIButton*)[self viewWithTag:20];
    [b7 setBackgroundImage:[UIImage imageWithColor:RGB(245, 245, 245)] forState:UIControlStateHighlighted];
    
    UIButton *b8 = (UIButton*)[self viewWithTag:21];
    [b8 setBackgroundImage:[UIImage imageWithColor:RGB(245, 245, 245)] forState:UIControlStateHighlighted];
    
    [b1 addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [b2 addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [b3 addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [b4 addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [b5 addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [b6 addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [b7 addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [b8 addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *v1 = [b1.superview addBottomLine];
    UIView *v2 = [b2.superview addBottomLine];
    UIView *v3 = [b3.superview addBottomLine];
    UIView *v4 = [b4.superview addBottomLine];
    UIView *v5 = [b5.superview addBottomLine];
    UIView *v7 = [b7.superview addBottomLine];
    UIView *v8 = [b8.superview addBottomLine];
    
    v1.width = 400;
    v2.width = 400;
    v3.width = 400;
    v4.width = 400;
    v5.width = 400;
    v7.width = 400;
    v8.width = 400;

    self.backgroundColor = [UIColor whiteColor];
    
    UIView *topView = [self viewWithTag:1];
    topView.backgroundColor = [AppHelper defaultRedColor];
    self.clipsToBounds = YES;
    self.layer.cornerRadius = 5.0f;
    
    self.userInteractionEnabled = YES;
    
    [self layoutIfNeeded];
}

-(void) dimViewTapped
{
    [self.superview removeFromSuperview];
    [self removeFromSuperview];
}

- (void)closeTapped
{
    [self dimViewTapped];
}

- (void)buttonTapped:(UIButton*)sender
{
    if ([self.delegate respondsToSelector:@selector(popupView:clickedButtonAtIndex:)])
        [self.delegate popupView:self clickedButtonAtIndex:(int)(sender.tag - 10)];
     
    [self closeTapped];
}

#pragma mark - Gesture Recognizer delegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    CGPoint point = [touch locationInView:self.dimView];
    return !CGRectContainsPoint(self.frame, point);
}

@end
