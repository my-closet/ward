//
//  NewPopupVC.h
//  wardrobe
//
//  Created by Oleg Mytsouda on 24.05.16.
//  Copyright © 2016 Farcom. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NewPopupDelegate <NSObject>

- (void)popupView:(id)popupView clickedButtonAtIndex:(int)index;

@end


@interface NewPopupVC : UIViewController

+ (instancetype)popupView;
+ (instancetype)addShopView;
+ (instancetype)cameraView;
- (void)showInView:(UIView*)view;
@property (nonatomic, weak) id <NewPopupDelegate> delegate;

@end
