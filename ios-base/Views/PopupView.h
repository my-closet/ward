
#import <UIKit/UIKit.h>

@protocol PopupViewDelegate <NSObject>

- (void)popupView:(id)popupView clickedButtonAtIndex:(int)index;

@end

@interface PopupView : UIView
+ (instancetype)popupView;
+ (instancetype)addShopView;
+ (instancetype)cameraView;
- (void)showInView:(UIView*)view;
@property (nonatomic, weak) id <PopupViewDelegate> delegate;
@end
