
#import <UIKit/UIKit.h>
@class CanvasItem;

@protocol SegueWithItemDelegate <NSObject>

- (void)sendToDestinationViewItem:(id)item;

@end

@interface CanvasView : UIView

@property (weak, nonatomic) id<SegueWithItemDelegate> delegate;

- (CanvasItem*)addItemToCanvas:(Item*)item;
- (CanvasItem*)addItemToCanvasWithCanvas:(CanvasItem*)item;
- (void)addItemsToCanvasFromFavoriteItem:(Item*)favItem;
- (void)canvasItemDidDelete:(CanvasItem *)canvasItem;

- (void)saveState;
- (void)removeState;
- (void)restoreState;

- (void)cancelEditing;
- (void)enableEditing;

- (void)setup;

@property (assign, nonatomic) CGRect currentFrame;

@property (nonatomic, strong) NSMutableArray *canvasItemsArray;

- (void)saveFavoriteItemAsDraft:(BOOL)asDraft;
- (void)saveFavoriteItem;
- (void)saveChangesForExistingFavoriteItem:(Item*)favItem;
- (UIImage*)getImage;
- (void)clearCanvas;

@property (assign, nonatomic) BOOL ifNeedToShow;

@end
