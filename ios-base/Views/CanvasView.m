
#import "ImagePreviewController.h"
#import "CanvasView.h"
#import "CanvasItem.h"
#define kMaximumNumberOfItemsOnCanvas 7

@interface CanvasView()<CanvasItemDelegate>
@end
@implementation CanvasView
{
    UIView *_dimView;
    NSMutableArray *_statesArray;
}
- (void)setup
{
    _canvasItemsArray = [NSMutableArray array];
    _statesArray = [NSMutableArray array];
    _dimView = [[UIView alloc] initWithFrame:self.bounds];
    _dimView.autoresizingMask = UIViewAutoresizingNone;
    [self addSubview:_dimView];
    _dimView.backgroundColor = [UIColor blackColor];
    _dimView.alpha = 0.5f;
    _dimView.hidden = YES;
    _dimView.userInteractionEnabled = NO;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleWillDeleteItemNotification:) name:WillDeleteItemNotification object:nil];
        
    self.translatesAutoresizingMaskIntoConstraints = NO;
    
    
    //change canvas bg Color
  //  self.backgroundColor = CANVAS_BG_COLOR;
}

- (void)setBounds:(CGRect)bounds
{
    [super setBounds:bounds];
    
    if (bounds.size.height < 10){
        NSLog(@"frame!");
    }

}

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    
    if (frame.size.height < 10){
        NSLog(@"frame!");
    }
    
}

- (CanvasItem*)addItemToCanvasWithCanvas:(CanvasItem*)item
{
    if (!item) return nil;
    
    if (_canvasItemsArray.count == kMaximumNumberOfItemsOnCanvas)
    {
        [[[UIAlertView alloc] initWithTitle:nil
                                    message:[NSString stringWithFormat:@"Вы не можете добавить больше %d вещей на рабочую область!",kMaximumNumberOfItemsOnCanvas]
                                   delegate:nil
                          cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];
        return nil;
    } else {
        CanvasItem *canvasItem = item;
        if (self.ifNeedToShow) {
            canvasItem.leftImage.hidden = NO;
            canvasItem.rightImage.hidden = NO;
        } else {
            canvasItem.leftImage.hidden = YES;
            canvasItem.rightImage.hidden = YES;
        }
        canvasItem.delegate = self;
        canvasItem.editable = YES;
        [_canvasItemsArray addObject:canvasItem];
        canvasItem.width = self.width;
        [self addSubview:canvasItem];
        return canvasItem;
    }
}

- (CanvasItem*)addItemToCanvas:(Item*)item
{
    if (!item) return nil;
    
    if (_canvasItemsArray.count == kMaximumNumberOfItemsOnCanvas)
    {
        [[[UIAlertView alloc] initWithTitle:nil
                                    message:[NSString stringWithFormat:@"Вы не можете добавить больше %d вещей на рабочую область!",kMaximumNumberOfItemsOnCanvas]
                                   delegate:nil
                          cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];
        return nil;
    } else {
        CanvasItem *canvasItem = [CanvasItem itemWithItem:item];
        if (self.ifNeedToShow) {
            [canvasItem addSubview:canvasItem.informView];
            [canvasItem bringSubviewToFront:canvasItem.informView];
            canvasItem.leftImage.hidden = NO;
            canvasItem.rightImage.hidden = NO;
            canvasItem.informView.hidden = NO;
        } else {
            canvasItem.leftImage.hidden = YES;
            canvasItem.rightImage.hidden = YES;
        }
        
        if (self.ifNeedToShow) {
            canvasItem.x = self.currentFrame.origin.x;
            canvasItem.y = self.currentFrame.origin.y;
            canvasItem.imageView.layer.borderColor = [[UIColor redColor] CGColor];
            canvasItem.imageView.layer.borderWidth = 2.f;
            canvasItem.imageView.layer.cornerRadius = 12.f;
            canvasItem.imageView.layer.masksToBounds = YES;
          //  canvasItem.imageView.maskView
        }
        canvasItem.delegate = self;
        canvasItem.editable = YES;
        [_canvasItemsArray addObject:canvasItem];
        canvasItem.width = self.width;
        [self addSubview:canvasItem];
         return canvasItem;
    }
}
- (void)addItemsToCanvasFromFavoriteItem:(Item*)favItem
{
    if (favItem != nil){
    for (NSDictionary *d in favItem.favorite_components){
        NSNumber *uid = d[@"item_id"];
        if (uid){
            CGRect frame;
            if ([d[@"frame"] isKindOfClass:[NSString class]]) {
                frame = CGRectFromString(d[@"frame"]);
            } else {
                frame = [d[@"frame"] CGRectValue];
            }
        
            Item *item = [Item MR_findFirstByAttribute:@"uid" withValue:uid];

//            NSString *urlToImage;
//            if (![[item valueForKey:@"image_url"] hasPrefix:@"http://"] && ![[item valueForKey:@"image_url"] hasPrefix:@"https://"]) {
//                urlToImage = [NSString stringWithFormat:@"http://ishop.mycloset.ru%@",[item valueForKey:@"image_url"]];
//            } else {
//                urlToImage = [item valueForKey:@"image_url"];
//            }
            
            if (!item)
                continue;
            
            CanvasItem *canvasItem = [self addItemToCanvas:item];
            canvasItem.active = NO;
            canvasItem.editable = NO;
            canvasItem.frame = frame;
//            canvasItem.URLtoImage = urlToImage;
//            [canvasItem setNeedsLayout];
        }
    }
    }
}
- (void)canvasItemDidBeginEditing:(CanvasItem *)canvasItem
{
    [self bringSubviewToFront:_dimView];
    [self bringSubviewToFront:canvasItem];
    _dimView.hidden = NO;
    for (CanvasItem *item in _canvasItemsArray)
    {
        if (item != canvasItem)
            item.active = NO;
    }
    canvasItem.active = YES;
}
- (void)canvasItemDidDelete:(CanvasItem *)canvasItem
{
    [canvasItem removeFromSuperview];
    [self sendSubviewToBack:_dimView];
    [_canvasItemsArray removeObject:canvasItem];
}


-(void)stopEditing{
    [self cancelEditing];
    [self enableEditing];
//    for (CanvasItem *canvasItem in _canvasItemsArray)
//    {
//        canvasItem.editable = NO;
//    }
//    for (CanvasItem *canvasItem in _canvasItemsArray)
//    {
//        canvasItem.editable = YES;
//    }
}

- (void)cancelEditing
{
    _dimView.hidden = YES;
    for (CanvasItem *canvasItem in _canvasItemsArray)
    {
        canvasItem.editable = NO;
    }
}
- (void)enableEditing
{
    [self sendSubviewToBack:_dimView];
    _dimView.hidden = NO;
    _dimView.frame = self.bounds;
    
    for (CanvasItem *canvasItem in _canvasItemsArray)
    {
        canvasItem.editable = YES;
    }
    
    for (CanvasItem *canvasItem in _canvasItemsArray)
        canvasItem.active = NO;
    CanvasItem *lastItem = [self.subviews lastObject];
    if ([lastItem isKindOfClass:[CanvasItem class]]){
        [self bringSubviewToFront:_dimView];
        [self bringSubviewToFront:lastItem];
        lastItem.active = YES;
    }
}

- (void)saveState
{
    _statesArray = [NSMutableArray array];
    for (CanvasItem *item in self.subviews)
    {
        if ([item isKindOfClass:[CanvasItem class]]){
            NSValue *frame = [NSValue valueWithCGRect:item.frame];
            if (frame && item.item.uid){
                [_statesArray addObject:@{@"frame" : frame,
                                      @"item"  : item.item.uid}];
            } else {
                NSLog(@"frame:%@ item:%@",frame, item.item.uid);
            }
        }
    }
}
- (void)removeState
{
    _statesArray = nil;
}
- (void)restoreState
{
    if (_statesArray.count == 0)
        return;
    
    // first validate items
    BOOL valid = YES;
    
    if (_canvasItemsArray.count + (_dimView != nil) != self.subviews.count)
        valid = NO;
    
    if (!valid)
    {
        [self _clearCanvas];
        
        for (NSDictionary *dict in _statesArray)
        {
            NSNumber *objID = dict[@"item"];
            Item *item = [Item MR_findFirstByAttribute:@"uid" withValue:objID];
            if (item)
            {
                [self addItemToCanvas:item];
            }
            else
            {
                [self _clearCanvas];
                return;
            }
        }
    }
    
    for (int i = 0; i < _statesArray.count; i++)
    {
        NSDictionary *dict = _statesArray[i];
        
        NSNumber *objID = dict[@"item"];
        CanvasItem *canvasItem = nil;
        for (CanvasItem *item in _canvasItemsArray)
        {
            if ([item.item.uid isEqual:objID])
            {
                canvasItem = item;
                break;
            }
        }
        if (canvasItem){
            [self addSubview:canvasItem];
            CGRect frame = [dict[@"frame"] CGRectValue];
            canvasItem.frame = frame;
        }
    }
}

- (void)_clearCanvas
{
    for (CanvasItem *item in _canvasItemsArray)
    {
        [item removeFromSuperview];
    }
    [_canvasItemsArray removeAllObjects];
}
- (void)clearCanvas
{
    [self _clearCanvas];
}

#pragma mark - Notification Center

- (void)handleWillDeleteItemNotification:(NSNotification*)notif
{
    Item *itemToDelete = [notif object];
    
    CanvasItem *canvasItemToDelete = nil;
    
//    if (_canvasItemsArray.count == 0) {
//        NSArray *itemNeedDelete = [NSArray arrayWithArray:itemToDelete.favorite_components];
//        for (CanvasItem *canvasItem in itemNeedDelete)
//        {
//            if ([canvasItem.item.objectID isEqual:itemToDelete.objectID])
//            {
//                canvasItemToDelete = canvasItem;
//                break;
//            }
//        }
//    }
  
    for (CanvasItem *canvasItem in _canvasItemsArray)
    {
        if ([canvasItem.item.objectID isEqual:itemToDelete.objectID])
        {
            canvasItemToDelete = canvasItem;
            break;
        }
    }
    
    if (canvasItemToDelete)
    {
        [self canvasItemDidDelete:canvasItemToDelete];
    }
}


#pragma mark - Save handling

- (void)saveFavoriteItemAsDraft:(BOOL)asDraft
{
    if (self.canvasItemsArray.count == 0)
        return;
    
    
//    for (CanvasItem*newItem in self.canvasItemsArray) {
//        
//    }
    
    UIImage *image = [self getImage];
    
    Item *item = nil;
    
    Cat *cat = [Cat favoriteCategory];
    
    if (asDraft){
        NSString *draftTitle = @"Последний несохраненный образ";
        
        item = [Item MR_findFirstByAttribute:@"name" withValue:draftTitle];
        if (item != nil){
            item.name = [Item freeFavoriteName];
        }
        item = [Item MR_createEntity];
        item.uidValue = [Item freeUIDValue];
        item.name = draftTitle;
    }else{
        item = [Item MR_createEntity];
        item.uidValue = [Item freeUIDValue];
        item.name = [NSString stringWithFormat:@"%lu",[cat.items count]+1];
    }
    
    NSString *imageURL = [AppHelper saveImage:image];

    
    for (CanvasItem *canvasItem in self.canvasItemsArray) {
        Item *findedValue = [Item MR_findByAttribute:@"uid" withValue:canvasItem.item.uid];
        if (findedValue == nil) {
            NSLog(@"work");
            canvasItem.item.image_url = imageURL;
            [self saveChangesForExistingFavoriteItem:canvasItem.item];
            [canvasItem.item.managedObjectContext MR_saveToPersistentStoreAndWait];
            break;
        }
    }
    
    item.image_url = imageURL;
    item.category = cat;
    
    [self saveChangesForExistingFavoriteItem:item];
    [item.managedObjectContext MR_saveToPersistentStoreAndWait];
    
//    if (!asDraft)
////        [[[UIAlertView alloc] initWithTitle:nil
////                                    message:@"Образ сохранен в категорию \"Избранное\""
////                                   delegate:nil
////                          cancelButtonTitle:@"OK"
////                          otherButtonTitles:nil] show];
//    else
//        [self _clearCanvas];
    if(asDraft){
        [self _clearCanvas];
    }
}

- (void)saveFavoriteItem
{
    [self saveFavoriteItemAsDraft:NO];
}
- (void)saveChangesForExistingFavoriteItem:(Item *)item
{
    NSMutableArray *itemsArray = [NSMutableArray array];
    for (CanvasItem *item in self.subviews)
    {
        if ([item isKindOfClass:[CanvasItem class]]){
            NSDictionary *d = @{@"frame" : [NSValue valueWithCGRect:item.frame], @"item_id" : @(item.item.uidValue)};
            [itemsArray addObject:d];
        }
    }
   // NSMutableArray* someValue = [NSMutableArray new];
    //[someValue addObject:itemsArray];
    item.favorite_components = itemsArray;
    
    [item.managedObjectContext MR_saveToPersistentStoreAndWait];
    
}
- (UIImage*)getImage
{
    CGFloat topY = 5000;
    CGFloat bottomY = -100;
    
    _dimView.alpha = 0;
    
    for (CanvasItem *c in self.subviews)
    {
        if ([c isKindOfClass:[CanvasItem class]]){
            if (c.y < topY)
                topY = c.y;
            if (c.bottomY > bottomY)
                bottomY = c.bottomY;
            [c setLinesHidden:YES];
        }
    }
    
    CGRect frame = self.frame;
    
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, NO, [UIScreen mainScreen].scale);
    
    [self drawViewHierarchyInRect:CGRectMake(0, 0, frame.size.width, frame.size.height) afterScreenUpdates:YES];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();

    self.frame = frame;
    
    topY += kCanvasTopBottomPadding;
    bottomY -= kCanvasTopBottomPadding;
    
    image = [image imageCroppingForRect:CGRectMake(0, topY, self.width, bottomY - topY)];
    
    for (CanvasItem *c in self.subviews)
    {
        if ([c isKindOfClass:[CanvasItem class]])
            [c setLinesHidden:NO];
    }

    _dimView.alpha = 0.5;
    
    
//#ifdef DEBUG
//    ImagePreviewController *controller = [[ImagePreviewController alloc] init];
//    controller.image = image;
//    [ApplicationDelegate.window.rootViewController presentViewController:controller animated:YES completion:nil];
//    return nil;
//#endif
//    
    
    
    return image;
}


- (void)goToBuyForm:(id)item
{
    [self.delegate sendToDestinationViewItem:item];
}

@end
