
#import <UIKit/UIKit.h>

@interface TutorialView : UIView
- (instancetype)initWithSuperView:(UIView*)view;
- (void)setup;

@property (nonatomic, copy) Callback closeBlock;

@end
