
#define kCanvasTopBottomPadding 10

#import <UIKit/UIKit.h>
@class CanvasItem;
@protocol CanvasItemDelegate <NSObject>

- (void)canvasItemDidBeginEditing:(CanvasItem*)canvasItem;
- (void)canvasItemDidDelete:(CanvasItem*)canvasItem;
- (void)stopEditing;

- (void)goToBuyForm:(id)item;

@end

@interface CanvasItem : UIView

@property (nonatomic, strong, readonly) Item *item;

- (void)setLinesHidden:(BOOL)hidden;

+ (instancetype)itemWithItem:(Item*)item;
@property (nonatomic, weak) id <CanvasItemDelegate> delegate;

@property (nonatomic, assign) BOOL editable; // default YES
@property (nonatomic, assign) BOOL active; // default NO
@property (nonatomic, assign) BOOL cantEditing;// default NO

@property (nonatomic, strong) NSString *URLtoImage;

@property (strong, nonatomic) IBOutlet UIImageView *leftImage;
@property (strong, nonatomic) IBOutlet UIImageView *rightImage;

@property (strong, nonatomic) IBOutlet UIView *informView;
@property (strong, nonatomic) IBOutlet UILabel *shopLabel;
@property (strong, nonatomic) IBOutlet UILabel *valueLabel;

@property (nonatomic, strong) IBOutlet UIImageView *imageView;

- (void)unHidePreviewElement;
- (void)hidePreviewElement;

@end
