
#import "TutorialView.h"
#import "PureLayout.h"
@interface TutorialView() <UIScrollViewDelegate>

@property (nonatomic, weak) IBOutlet UIPageControl *pageControl;
@property (nonatomic, weak) IBOutlet UIScrollView *scrollView;
@property (nonatomic, weak) IBOutlet UIView *view1;
@property (nonatomic, weak) IBOutlet UIView *view2;
@property (nonatomic, weak) IBOutlet UIView *view3;
@end

@implementation TutorialView

- (instancetype)initWithSuperView:(UIView*)view
{
    self = [[Bundle loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil] firstObject];
    self.frame = view.bounds;
    self.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [self setTranslatesAutoresizingMaskIntoConstraints:YES];
    [view addSubview:self];
    return self;
}

- (void)layoutSubviews{
    
    [super layoutSubviews];
    
    _scrollView.contentSize = CGSizeMake(_view3.rightX, _scrollView.height);
}

- (void)setup{
    self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.8];
    
    [_scrollView addSubview:_view1];
    [_scrollView addSubview:_view2];
    [_scrollView addSubview:_view3];
    
    _view1.backgroundColor = [UIColor clearColor];
    _view2.backgroundColor = [UIColor clearColor];
    _view3.backgroundColor = [UIColor clearColor];
    
    _scrollView.pagingEnabled = YES;
    
    _scrollView.delegate = self;
    
    _scrollView.autoresizesSubviews = YES;

    _view1.frame = _scrollView.bounds;
    _view2.frame = _scrollView.bounds;
    _view3.frame = _scrollView.bounds;
    
    _view2.x = _view1.rightX;
    _view3.x = _view2.rightX;
    
    [self setupShareButton];
    [self setupCloseButton];
}

- (void)setupCloseButton{
    UIButton *button = (UIButton*)[_view3 viewWithTag:99];
    [button addTarget:self action:@selector(closeTapped) forControlEvents:UIControlEventTouchUpInside];
}

- (void)closeTapped{
    BLOCK_SAFE_RUN(self.closeBlock);
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat x = scrollView.contentOffset.x;    
    int page = roundf(x / scrollView.width);
    self.pageControl.currentPage = page;
}


- (void)setupShareButton
{
    UIButton *shareButton =  (UIButton*)[_view3 viewWithTag:10];
    
    UIImage *image = [UIImage imageWithColor:RGBA(205, 205, 205,0.6f) radius:20 rect:CGRectMake(0, 0, 40, 40)];
    [shareButton setBackgroundImage:image forState:UIControlStateNormal];
    
    shareButton.backgroundColor = nil;
    
    shareButton.alpha = 0.7;
}

@end
