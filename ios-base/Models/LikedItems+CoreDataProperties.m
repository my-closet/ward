//
//  LikedItems+CoreDataProperties.m
//  wardrobe
//
//  Created by Oleg Mytsouda on 07.05.16.
//  Copyright © 2016 Farcom. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "LikedItems+CoreDataProperties.h"

@implementation LikedItems (CoreDataProperties)

@dynamic myLikeID;
@dynamic serverLikeID;

@end
