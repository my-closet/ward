#import "_Item.h"

extern NSString *const WillDeleteItemNotification;

#define kLocalItemIDOffset 2000000000


@interface Item : _Item {}

- (NSURL*)imageURL;

+ (NSString*)freeFavoriteName;
+ (int32_t)freeUIDValue;
+ (NSString*)freeItemNameForCategory:(NSString*)categoryName;
@end
