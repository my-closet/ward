#import "RootObject.h"

@interface RootObject ()

// Private interface goes here.

@end

@implementation RootObject


+ (instancetype)objectWithId:(NSInteger)objID{
    RootObject *obj = [RootObject MR_findFirstByAttribute:@"uid" withValue:@(objID)];
    return obj;
}
- (void)updateWithDict:(NSDictionary *)dict {
    [self updateWithDict:dict inContext:[NSManagedObjectContext MR_defaultContext]];
}

- (void)updateWithDict:(NSDictionary *)dict inContext:(NSManagedObjectContext *)localContext {
    self.uid = [NSNumber numberWithInt:[[dict objectForKeyOrNil:@"id"] intValue]];
}

+ (id)objectWithDict:(NSDictionary *)dict {
    return [self objectWithDict:dict inContext:[NSManagedObjectContext MR_defaultContext]];
}

+ (id)objectWithDict:(NSDictionary *)dict inContext:(NSManagedObjectContext *)localContext {
    id uid = [dict objectForKeyOrNil:@"id"];
    id object = [self.class MR_findFirstByAttribute:@"uid" withValue:uid inContext:localContext];
    if (!object) {
        object = [self.class MR_createInContext:localContext];
    }
    [object updateWithDict:dict inContext:localContext];
    return object;
}

+ (void)importObjectsFromArray:(NSArray *)array withCompletion:(Callback)complete {
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        for (NSDictionary *dict in array) {
            [self objectWithDict:dict inContext:localContext];
        }
    } completion:^(BOOL success, NSError *error) {
        BLOCK_SAFE_RUN(complete);
    }];
}

+ (void)getObjectsWithArray:(NSArray *)array sortedBy:(NSString *)sortedBy ascending:(BOOL)ascending callback:(ArrayCallback)block {
    __block NSMutableArray *objectIds = [NSMutableArray array];
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        for (NSDictionary *dict in array) {
            id object = [self objectWithDict:dict inContext:localContext];
            if ([object respondsToSelector:@selector(uid)])
                [objectIds addObject:[object uid]];
        }
    } completion:^(BOOL success, NSError *error) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uid IN %@", objectIds];
        NSArray *objects = [self.class MR_findAllSortedBy:sortedBy ascending:ascending withPredicate:predicate];
        BLOCK_SAFE_RUN(block, objects);
    }];
}

- (NSURL*)imageURLFromString:(NSString*)string{
    
    if (string.length == 0) return nil;
    
    NSURL *returnedUrl;
    
    if ([string hasPrefix:@"http://"] || [string hasPrefix:@"https://"]) {
        returnedUrl = [NSURL URLWithString:string];
    }else if ([[string substringToIndex:1] isEqualToString:@"/"] == NO){
        returnedUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@/files/logos/%@",DOMAIN_URL,string]];
    }
    
    return returnedUrl;
    
}

- (NSURL*)imageURLFromStr:(NSString*)string{
    return [NSURL URLWithString:string];
}

@end
