// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to LikedItems.h instead.

#import <CoreData/CoreData.h>

extern const struct LikedItemsAttributes {
	__unsafe_unretained NSString *myLikeID;
	__unsafe_unretained NSString *serverLikeID;
} LikedItemsAttributes;

@interface LikedItemsID : NSManagedObjectID {}
@end

@interface _LikedItems : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) LikedItemsID* objectID;

@property (nonatomic, strong) NSString* myLikeID;

//- (BOOL)validateMyLikeID:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* serverLikeID;

//- (BOOL)validateServerLikeID:(id*)value_ error:(NSError**)error_;

@end

@interface _LikedItems (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveMyLikeID;
- (void)setPrimitiveMyLikeID:(NSString*)value;

- (NSString*)primitiveServerLikeID;
- (void)setPrimitiveServerLikeID:(NSString*)value;

@end
