//
//  LikedItems.h
//  wardrobe
//
//  Created by Oleg Mytsouda on 07.05.16.
//  Copyright © 2016 Farcom. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface LikedItems : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "LikedItems+CoreDataProperties.h"
