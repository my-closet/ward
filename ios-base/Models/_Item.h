// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Item.h instead.

#import <CoreData/CoreData.h>
#import "RootObject.h"

extern const struct ItemAttributes {
	__unsafe_unretained NSString *currency;
	__unsafe_unretained NSString *favorite_components;
	__unsafe_unretained NSString *image_url;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *oldPrice;
	__unsafe_unretained NSString *price;
	__unsafe_unretained NSString *shop_name;
	__unsafe_unretained NSString *shop_uid;
	__unsafe_unretained NSString *site_url;
	__unsafe_unretained NSString *vendor_code;
} ItemAttributes;

extern const struct ItemRelationships {
	__unsafe_unretained NSString *category;
} ItemRelationships;

@class Cat;

@class NSObject;

@interface ItemID : RootObjectID {}
@end

@interface _Item : RootObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) ItemID* objectID;

@property (nonatomic, strong) NSString* currency;

//- (BOOL)validateCurrency:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) id favorite_components;

//- (BOOL)validateFavorite_components:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* image_url;

//- (BOOL)validateImage_url:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* oldPrice;

@property (atomic) int16_t oldPriceValue;
- (int16_t)oldPriceValue;
- (void)setOldPriceValue:(int16_t)value_;

//- (BOOL)validateOldPrice:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* price;

@property (atomic) int16_t priceValue;
- (int16_t)priceValue;
- (void)setPriceValue:(int16_t)value_;

//- (BOOL)validatePrice:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* shop_name;

//- (BOOL)validateShop_name:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* shop_uid;

@property (atomic) int32_t shop_uidValue;
- (int32_t)shop_uidValue;
- (void)setShop_uidValue:(int32_t)value_;

//- (BOOL)validateShop_uid:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* site_url;

//- (BOOL)validateSite_url:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* vendor_code;

//- (BOOL)validateVendor_code:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) Cat *category;

//- (BOOL)validateCategory:(id*)value_ error:(NSError**)error_;

@end

@interface _Item (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveCurrency;
- (void)setPrimitiveCurrency:(NSString*)value;

- (id)primitiveFavorite_components;
- (void)setPrimitiveFavorite_components:(id)value;

- (NSString*)primitiveImage_url;
- (void)setPrimitiveImage_url:(NSString*)value;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSNumber*)primitiveOldPrice;
- (void)setPrimitiveOldPrice:(NSNumber*)value;

- (int16_t)primitiveOldPriceValue;
- (void)setPrimitiveOldPriceValue:(int16_t)value_;

- (NSNumber*)primitivePrice;
- (void)setPrimitivePrice:(NSNumber*)value;

- (int16_t)primitivePriceValue;
- (void)setPrimitivePriceValue:(int16_t)value_;

- (NSString*)primitiveShop_name;
- (void)setPrimitiveShop_name:(NSString*)value;

- (NSNumber*)primitiveShop_uid;
- (void)setPrimitiveShop_uid:(NSNumber*)value;

- (int32_t)primitiveShop_uidValue;
- (void)setPrimitiveShop_uidValue:(int32_t)value_;

- (NSString*)primitiveSite_url;
- (void)setPrimitiveSite_url:(NSString*)value;

- (NSString*)primitiveVendor_code;
- (void)setPrimitiveVendor_code:(NSString*)value;

- (Cat*)primitiveCategory;
- (void)setPrimitiveCategory:(Cat*)value;

@end
