// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Cat.h instead.

#import <CoreData/CoreData.h>

extern const struct CatAttributes {
	__unsafe_unretained NSString *favorite;
	__unsafe_unretained NSString *image_url;
	__unsafe_unretained NSString *name;
} CatAttributes;

extern const struct CatRelationships {
	__unsafe_unretained NSString *items;
} CatRelationships;

@class Item;

@interface CatID : NSManagedObjectID {}
@end

@interface _Cat : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) CatID* objectID;

@property (nonatomic, strong) NSNumber* favorite;

@property (atomic) BOOL favoriteValue;
- (BOOL)favoriteValue;
- (void)setFavoriteValue:(BOOL)value_;

//- (BOOL)validateFavorite:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* image_url;

//- (BOOL)validateImage_url:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSOrderedSet *items;

- (NSMutableOrderedSet*)itemsSet;

@end

@interface _Cat (ItemsCoreDataGeneratedAccessors)
- (void)addItems:(NSOrderedSet*)value_;
- (void)removeItems:(NSOrderedSet*)value_;
- (void)addItemsObject:(Item*)value_;
- (void)removeItemsObject:(Item*)value_;

- (void)insertObject:(Item*)value inItemsAtIndex:(NSUInteger)idx;
- (void)removeObjectFromItemsAtIndex:(NSUInteger)idx;
- (void)insertItems:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeItemsAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInItemsAtIndex:(NSUInteger)idx withObject:(Item*)value;
- (void)replaceItemsAtIndexes:(NSIndexSet *)indexes withItems:(NSArray *)values;

@end

@interface _Cat (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveFavorite;
- (void)setPrimitiveFavorite:(NSNumber*)value;

- (BOOL)primitiveFavoriteValue;
- (void)setPrimitiveFavoriteValue:(BOOL)value_;

- (NSString*)primitiveImage_url;
- (void)setPrimitiveImage_url:(NSString*)value;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSMutableOrderedSet*)primitiveItems;
- (void)setPrimitiveItems:(NSMutableOrderedSet*)value;

@end
