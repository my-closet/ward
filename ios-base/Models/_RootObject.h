// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to RootObject.h instead.

#import <CoreData/CoreData.h>

extern const struct RootObjectAttributes {
	__unsafe_unretained NSString *uid;
} RootObjectAttributes;

@interface RootObjectID : NSManagedObjectID {}
@end

@interface _RootObject : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) RootObjectID* objectID;

@property (nonatomic, strong) NSNumber* uid;

@property (atomic) int32_t uidValue;
- (int32_t)uidValue;
- (void)setUidValue:(int32_t)value_;

//- (BOOL)validateUid:(id*)value_ error:(NSError**)error_;

@end

@interface _RootObject (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveUid;
- (void)setPrimitiveUid:(NSNumber*)value;

- (int32_t)primitiveUidValue;
- (void)setPrimitiveUidValue:(int32_t)value_;

@end
