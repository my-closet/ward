//
//  LikedItems+CoreDataProperties.h
//  wardrobe
//
//  Created by Oleg Mytsouda on 07.05.16.
//  Copyright © 2016 Farcom. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "LikedItems.h"

NS_ASSUME_NONNULL_BEGIN

@interface LikedItems (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *myLikeID;
@property (nullable, nonatomic, retain) NSString *serverLikeID;

@end

NS_ASSUME_NONNULL_END
