// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Item.m instead.

#import "_Item.h"

const struct ItemAttributes ItemAttributes = {
	.currency = @"currency",
	.favorite_components = @"favorite_components",
	.image_url = @"image_url",
	.name = @"name",
	.oldPrice = @"oldPrice",
	.price = @"price",
	.shop_name = @"shop_name",
	.shop_uid = @"shop_uid",
	.site_url = @"site_url",
	.vendor_code = @"vendor_code",
};

const struct ItemRelationships ItemRelationships = {
	.category = @"category",
};

@implementation ItemID
@end

@implementation _Item

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Item" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Item";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Item" inManagedObjectContext:moc_];
}

- (ItemID*)objectID {
	return (ItemID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"oldPriceValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"oldPrice"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"priceValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"price"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"shop_uidValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"shop_uid"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic currency;

@dynamic favorite_components;

@dynamic image_url;

@dynamic name;

@dynamic oldPrice;

- (int16_t)oldPriceValue {
	NSNumber *result = [self oldPrice];
	return [result shortValue];
}

- (void)setOldPriceValue:(int16_t)value_ {
	[self setOldPrice:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveOldPriceValue {
	NSNumber *result = [self primitiveOldPrice];
	return [result shortValue];
}

- (void)setPrimitiveOldPriceValue:(int16_t)value_ {
	[self setPrimitiveOldPrice:[NSNumber numberWithShort:value_]];
}

@dynamic price;

- (int16_t)priceValue {
	NSNumber *result = [self price];
	return [result shortValue];
}

- (void)setPriceValue:(int16_t)value_ {
	[self setPrice:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitivePriceValue {
	NSNumber *result = [self primitivePrice];
	return [result shortValue];
}

- (void)setPrimitivePriceValue:(int16_t)value_ {
	[self setPrimitivePrice:[NSNumber numberWithShort:value_]];
}

@dynamic shop_name;

@dynamic shop_uid;

- (int32_t)shop_uidValue {
	NSNumber *result = [self shop_uid];
	return [result intValue];
}

- (void)setShop_uidValue:(int32_t)value_ {
	[self setShop_uid:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveShop_uidValue {
	NSNumber *result = [self primitiveShop_uid];
	return [result intValue];
}

- (void)setPrimitiveShop_uidValue:(int32_t)value_ {
	[self setPrimitiveShop_uid:[NSNumber numberWithInt:value_]];
}

@dynamic site_url;

@dynamic vendor_code;

@dynamic category;

@end

