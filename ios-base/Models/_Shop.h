// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Shop.h instead.

#import <CoreData/CoreData.h>
#import "RootObject.h"

extern const struct ShopAttributes {
	__unsafe_unretained NSString *address;
	__unsafe_unretained NSString *email;
	__unsafe_unretained NSString *image_url;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *shop_description;
} ShopAttributes;

@interface ShopID : RootObjectID {}
@end

@interface _Shop : RootObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) ShopID* objectID;

@property (nonatomic, strong) NSString* address;

//- (BOOL)validateAddress:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* email;

//- (BOOL)validateEmail:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* image_url;

//- (BOOL)validateImage_url:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* shop_description;

//- (BOOL)validateShop_description:(id*)value_ error:(NSError**)error_;

@end

@interface _Shop (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveAddress;
- (void)setPrimitiveAddress:(NSString*)value;

- (NSString*)primitiveEmail;
- (void)setPrimitiveEmail:(NSString*)value;

- (NSString*)primitiveImage_url;
- (void)setPrimitiveImage_url:(NSString*)value;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

- (NSString*)primitiveShop_description;
- (void)setPrimitiveShop_description:(NSString*)value;

@end
