// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Shop.m instead.

#import "_Shop.h"

const struct ShopAttributes ShopAttributes = {
	.address = @"address",
	.email = @"email",
	.image_url = @"image_url",
	.name = @"name",
	.shop_description = @"shop_description",
};

@implementation ShopID
@end

@implementation _Shop

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Shop" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Shop";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Shop" inManagedObjectContext:moc_];
}

- (ShopID*)objectID {
	return (ShopID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic address;

@dynamic email;

@dynamic image_url;

@dynamic name;

@dynamic shop_description;

@end

