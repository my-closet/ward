// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ShopCategory.m instead.

#import "_ShopCategory.h"

const struct ShopCategoryAttributes ShopCategoryAttributes = {
	.category_description = @"category_description",
	.image_url = @"image_url",
	.name = @"name",
};

@implementation ShopCategoryID
@end

@implementation _ShopCategory

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"ShopCategory" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"ShopCategory";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"ShopCategory" inManagedObjectContext:moc_];
}

- (ShopCategoryID*)objectID {
	return (ShopCategoryID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic category_description;

@dynamic image_url;

@dynamic name;

@end

