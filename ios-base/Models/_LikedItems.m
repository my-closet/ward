// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to LikedItems.m instead.

#import "_LikedItems.h"

const struct LikedItemsAttributes LikedItemsAttributes = {
	.myLikeID = @"myLikeID",
	.serverLikeID = @"serverLikeID",
};

@implementation LikedItemsID
@end

@implementation _LikedItems

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"LikedItems" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"LikedItems";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"LikedItems" inManagedObjectContext:moc_];
}

- (LikedItemsID*)objectID {
	return (LikedItemsID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic myLikeID;

@dynamic serverLikeID;

@end

