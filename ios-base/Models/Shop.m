#import "Shop.h"

@interface Shop ()

// Private interface goes here.

@end

@implementation Shop

- (void)updateWithDict:(NSDictionary *)dict inContext:(NSManagedObjectContext *)localContext
{
    [super updateWithDict:dict inContext:localContext];
    
    self.name = dict[@"name"];
    self.address = dict[@"location"];
    self.image_url = dict[@"logo"];
    self.email = dict[@"email"];
}

- (NSURL*)imageURL{
    return [self imageURLFromString:self.image_url];
}

@end
