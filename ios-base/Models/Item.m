#import "Item.h"

@interface Item ()

// Private interface goes here.

@end

@implementation Item



- (void)updateWithDict:(NSDictionary *)dict inContext:(NSManagedObjectContext *)localContext{
    [super updateWithDict:dict inContext:localContext];
   // NSNumber *priceNumber = [NSNumber numberWithInt:[dict[@"price"] intValue]];
    self.priceValue = [dict[@"price"] intValue];
    
    if (![dict[@"oldprice"] isKindOfClass:[NSNull class]]) {
        self.oldPriceValue = [dict[@"oldprice"] intValue];
    }
    
   // [self setAPriceValueWithInteger:priceNumber];
    if (![dict[@"img"] isKindOfClass:[NSNull class]]) {
        self.image_url = dict[@"img"];
    }
    self.name = dict[@"name"];
    self.site_url = dict[@"url"];

}

// Custom logic goes here.

+(NSString*)freeItemNameForCategory:(NSString*)categoryName
{
    BOOL found = YES;
    int32_t i = 0;
    NSString *name = nil;
    while (found) {
        
        i++;
        
        name = [NSString stringWithFormat:@"%@ %d",categoryName,i];
        
        Item *item = [Item MR_findFirstByAttribute:@"name" withValue:name];
        found = (item != nil);
    }
    return name;
}

+(NSString*)freeFavoriteName
{
    BOOL found = YES;
    int32_t i = 0;
    
    NSString *favName = @"Сохраненный образ";
    
    NSString *name = nil;
    while (found) {
        
        i++;
        
        name = [NSString stringWithFormat:@"%@ %d",favName,i];
        
        Item *item = [Item MR_findFirstByAttribute:@"name" withValue:name];
        found = (item != nil);
    }
    return name;
}



+(int32_t)freeUIDValue
{
    BOOL found = YES;
    int32_t i = kLocalItemIDOffset;
    while (found) {
        i++;
        Item *item = [Item MR_findFirstByAttribute:@"uid" withValue:@(i)];
        found = (item != nil);
    }
    return i;
}

- (NSURL*)imageURL{
    return [self imageURLFromString:self.image_url];
}

@end








