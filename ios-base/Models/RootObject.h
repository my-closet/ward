#import "_RootObject.h"

@interface RootObject : _RootObject {}
+ (instancetype) objectWithId:(NSInteger)objID;

- (void)updateWithDict:(NSDictionary *)dict;
- (void)updateWithDict:(NSDictionary *)dict inContext:(NSManagedObjectContext *)localContext;
+ (id)objectWithDict:(NSDictionary *)dict;
+ (id)objectWithDict:(NSDictionary *)dict inContext:(NSManagedObjectContext *)localContext;

+ (void)importObjectsFromArray:(NSArray *)array withCompletion:(Callback)complete;
+ (void)getObjectsWithArray:(NSArray *)array sortedBy:(NSString *)sortedBy ascending:(BOOL)ascending callback:(ArrayCallback)block;

- (NSURL*)imageURLFromString:(NSString*)string;
- (NSURL*)imageURLFromStr:(NSString*)string;

@end
