#import "ShopCategory.h"

@interface ShopCategory ()

// Private interface goes here.

@end

@implementation ShopCategory

- (void)updateWithDict:(NSDictionary *)dict inContext:(NSManagedObjectContext *)localContext{
    [super updateWithDict:dict inContext:localContext];
    
    self.name = dict[@"name"];
    if (![dict[@"description"] isKindOfClass:[NSNull class]]) {
        self.category_description = dict[@"description"];
    } else {
        self.category_description = @"";
    }
    if (![dict[@"image"] isKindOfClass:[NSNull class]]) {
        self.image_url = dict[@"image"];
    } else {
        self.image_url = @"";
    }
}

- (NSURL*)imageURL{
    return [self imageURLFromString:self.image_url];
}


@end
