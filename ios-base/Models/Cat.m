#import "Cat.h"

@interface Cat ()

// Private interface goes here.

@end

@implementation Cat
+ (NSArray *)allButFavorite
{
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"favorite == nil OR favorite == %@",@(NO)];
    
    return [Cat MR_findAllSortedBy:@"name" ascending:YES withPredicate:predicate];
}
+ (Cat *)favoriteCategory
{
    Cat *cat = [Cat MR_findFirstByAttribute:@"favorite"
                                  withValue:@(YES)];
    
    if (cat == nil)
    {
        NSString *kFavoriteClothDefaultTitle = @"Избранное";
        
        cat = [Cat MR_createEntity];
        cat.name = kFavoriteClothDefaultTitle;
        cat.favoriteValue = YES;
        [cat.managedObjectContext MR_saveToPersistentStoreAndWait];
    }
    
    return cat;
}

@end
