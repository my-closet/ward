// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Cat.m instead.

#import "_Cat.h"

const struct CatAttributes CatAttributes = {
	.favorite = @"favorite",
	.image_url = @"image_url",
	.name = @"name",
};

const struct CatRelationships CatRelationships = {
	.items = @"items",
};

@implementation CatID
@end

@implementation _Cat

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Cat" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Cat";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Cat" inManagedObjectContext:moc_];
}

- (CatID*)objectID {
	return (CatID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"favoriteValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"favorite"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic favorite;

- (BOOL)favoriteValue {
	NSNumber *result = [self favorite];
	return [result boolValue];
}

- (void)setFavoriteValue:(BOOL)value_ {
	[self setFavorite:[NSNumber numberWithBool:value_]];
}

- (BOOL)primitiveFavoriteValue {
	NSNumber *result = [self primitiveFavorite];
	return [result boolValue];
}

- (void)setPrimitiveFavoriteValue:(BOOL)value_ {
	[self setPrimitiveFavorite:[NSNumber numberWithBool:value_]];
}

@dynamic image_url;

@dynamic name;

@dynamic items;

- (NSMutableOrderedSet*)itemsSet {
	[self willAccessValueForKey:@"items"];

	NSMutableOrderedSet *result = (NSMutableOrderedSet*)[self mutableOrderedSetValueForKey:@"items"];

	[self didAccessValueForKey:@"items"];
	return result;
}

@end

@implementation _Cat (ItemsCoreDataGeneratedAccessors)
- (void)addItems:(NSOrderedSet*)value_ {
	[self.itemsSet unionOrderedSet:value_];
}
- (void)removeItems:(NSOrderedSet*)value_ {
	[self.itemsSet minusOrderedSet:value_];
}
- (void)addItemsObject:(Item*)value_ {
	[self.itemsSet addObject:value_];
}
- (void)removeItemsObject:(Item*)value_ {
	[self.itemsSet removeObject:value_];
}
- (void)insertObject:(Item*)value inItemsAtIndex:(NSUInteger)idx {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"items"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self items]];
    [tmpOrderedSet insertObject:value atIndex:idx];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"items"];
    [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"items"];
}
- (void)removeObjectFromItemsAtIndex:(NSUInteger)idx {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"items"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self items]];
    [tmpOrderedSet removeObjectAtIndex:idx];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"items"];
    [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"items"];
}
- (void)insertItems:(NSArray *)value atIndexes:(NSIndexSet *)indexes {
    [self willChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"items"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self items]];
    [tmpOrderedSet insertObjects:value atIndexes:indexes];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"items"];
    [self didChange:NSKeyValueChangeInsertion valuesAtIndexes:indexes forKey:@"items"];
}
- (void)removeItemsAtIndexes:(NSIndexSet *)indexes {
    [self willChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"items"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self items]];
    [tmpOrderedSet removeObjectsAtIndexes:indexes];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"items"];
    [self didChange:NSKeyValueChangeRemoval valuesAtIndexes:indexes forKey:@"items"];
}
- (void)replaceObjectInItemsAtIndex:(NSUInteger)idx withObject:(Item*)value {
    NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:idx];
    [self willChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"items"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self items]];
    [tmpOrderedSet replaceObjectAtIndex:idx withObject:value];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"items"];
    [self didChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"items"];
}
- (void)replaceItemsAtIndexes:(NSIndexSet *)indexes withItems:(NSArray *)value {
    [self willChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"items"];
    NSMutableOrderedSet *tmpOrderedSet = [NSMutableOrderedSet orderedSetWithOrderedSet:[self items]];
    [tmpOrderedSet replaceObjectsAtIndexes:indexes withObjects:value];
    [self setPrimitiveValue:tmpOrderedSet forKey:@"items"];
    [self didChange:NSKeyValueChangeReplacement valuesAtIndexes:indexes forKey:@"items"];
}
@end

