// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to ShopCategory.h instead.

#import <CoreData/CoreData.h>
#import "RootObject.h"

extern const struct ShopCategoryAttributes {
	__unsafe_unretained NSString *category_description;
	__unsafe_unretained NSString *image_url;
	__unsafe_unretained NSString *name;
} ShopCategoryAttributes;

@interface ShopCategoryID : RootObjectID {}
@end

@interface _ShopCategory : RootObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) ShopCategoryID* objectID;

@property (nonatomic, strong) NSString* category_description;

//- (BOOL)validateCategory_description:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* image_url;

//- (BOOL)validateImage_url:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* name;

//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;

@end

@interface _ShopCategory (CoreDataGeneratedPrimitiveAccessors)

- (NSString*)primitiveCategory_description;
- (void)setPrimitiveCategory_description:(NSString*)value;

- (NSString*)primitiveImage_url;
- (void)setPrimitiveImage_url:(NSString*)value;

- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;

@end
