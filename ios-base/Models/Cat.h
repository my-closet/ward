#import "_Cat.h"
#import "Item.h"
@interface Cat : _Cat {}
// Custom logic goes here.
+ (Cat*)favoriteCategory;
+ (NSArray*)allButFavorite;

@end
