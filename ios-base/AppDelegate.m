
#import "AppDelegate.h"
#import "VKSdk.h"
#import "STTwitter.h"
#import "FacebookManager.h"
#import "VKManager.h"
#import "TwitterManager.h"
#import "iRate.h"
#import <Fabric/Fabric.h>

//#import <Appsee/Appsee.h>

//#import "YMMYandexMetrica.h"
//#import <Crashlytics/Crashlytics.h>
@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
//    [Fabric with:@[CrashlyticsKit]];
    
  //  [Appsee start:@"dc3adb13493d4964bdf7784fe9ff0847"];
    
    // Override point for customization after application launch.
    [MagicalRecord setupAutoMigratingCoreDataStack];
    [MagicalRecord setShouldDeleteStoreOnModelMismatch:YES];
    
    NSURL *url = [NSPersistentStore MR_defaultPersistentStore].URL;
    NSLog(@"url:%@",url);
    NSError *error = nil;
    [url setResourceValue:[NSNumber numberWithBool:YES] forKey:NSURLIsExcludedFromBackupKey error:&error];
    
  //  [YMMYandexMetrica startWit]
    
  //  [YMMYandexMetrica activateWithApiKey:@"31638"];
    
    [AppHelper regenerateDataBaseIfNeeded];
    
    [FacebookManager sharedManager];
    [VKManager sharedManager];
    [TwitterManager sharedManager];
    
    [SVProgressHUD setBackgroundColor:RGB(255, 245, 245)];
    [SVProgressHUD setForegroundColor:[AppHelper defaultRedColor]];
    
    [iRate sharedInstance].usesUntilPrompt = 3;
    [iRate sharedInstance].daysUntilPrompt = 1;
    [iRate sharedInstance].message = @"Спасибо Вам за поддержку!";
    
    
    // 1st: find next fire date, using NSDateComponents
    
    NSDate * date = [NSDate date];
    NSDateComponents * components = [[NSCalendar currentCalendar] components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit fromDate:date];
    
    // Components will now contain information about current hour and minute, day, month and year.
    
    // Do your calculation in order to setup the right date. Note that components reflect user timezone.
    // For example, skip to the next day if current time is after 9:00:
    if (components.hour >= 9) {
        components.day = 1;
    }
    
    // Now fix the components for firing time, for example 9:00.
    components.hour = 9;
    components.minute = 0;
    
    NSDate * fireDate = [[NSCalendar currentCalendar] dateFromComponents:components];
    
    NSLog(@"Notification will fire at: %@", fireDate);
    
    
    // 2nd: Schedule local notification with repetitions:
    
    UILocalNotification * notification = [[UILocalNotification alloc] init];
    notification.fireDate = fireDate;
    notification.repeatInterval = NSMonthCalendarUnit; // Here is the trick
    notification.alertBody = @"Не забудьте сделать селфи и заранее подобрать себе подходящую оджеду, прежде чем что-то покупать.";
    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
    
//    [ [UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
//    [[UIApplication sharedApplication] registerForRemoteNotifications];
//
    
    return YES;
}

//- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
//{
//    NSMutableString *baseStringToken = [[NSMutableString alloc] initWithFormat:@"%@", deviceToken];
//    NSString *newString1 = [baseStringToken stringByReplacingOccurrencesOfString:@" " withString:@""];
//    NSString *newString2 = [newString1 stringByReplacingOccurrencesOfString:@"<" withString:@""];
//    NSString *newString3 = [newString2 stringByReplacingOccurrencesOfString:@">" withString:@""];
//    [[NSUserDefaults standardUserDefaults] setObject:newString3 forKey:@"deviceToken"];
//}
- (BOOL)application: (UIApplication *)application
            openURL: (NSURL *)url
  sourceApplication: (NSString *)sourceApplication
         annotation: (id)annotation {
    if ([[url scheme] rangeOfString:@"vk" options:NSCaseInsensitiveSearch].location != NSNotFound){
        [VKSdk processOpenURL:url fromApplication:sourceApplication];
        return YES;
    } //else if ([[url scheme] rangeOfString:@"fb" options:NSCaseInsensitiveSearch].location != NSNotFound){
        // facebook
      //  return [FBAppCall handleOpenURL:url sourceApplication:sourceApplication];
//    } else if ([[url scheme] isEqualToString:@"garderob"]) {
//        NSDictionary *d = [self parametersDictionaryFromQueryString:[url query]];
//        NSString *token = d[@"oauth_token"];
//        NSString *verifier = d[@"oauth_verifier"];
//        [[TwitterManager sharedManager] setOAuthToken:token oauthVerifier:verifier];
//        return YES;
//    }
    
    return YES;
}
- (NSDictionary *)parametersDictionaryFromQueryString:(NSString *)queryString {
    NSMutableDictionary *md = [NSMutableDictionary dictionary];
    NSArray *queryComponents = [queryString componentsSeparatedByString:@"&"];
    for(NSString *s in queryComponents) {
        NSArray *pair = [s componentsSeparatedByString:@"="];
        if([pair count] != 2) continue;
        NSString *key = pair[0];
        NSString *value = pair[1];
        md[key] = value;
    }
    return md;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


- (UIViewController*)topViewController
{
    return [self topViewControllerWithRootViewController:[UIApplication sharedApplication].delegate.window.rootViewController];
    
}


- (UIViewController*)topViewControllerWithRootViewController:(UIViewController*)rootViewController
{
    if ([rootViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController* navigationController = (UINavigationController*)rootViewController;
        return [self topViewControllerWithRootViewController:navigationController.visibleViewController];
    } else if (rootViewController.presentedViewController) {
        UIViewController* presentedViewController = rootViewController.presentedViewController;
        return [self topViewControllerWithRootViewController:presentedViewController];
    } else {
        return rootViewController;
    }
}


@end
