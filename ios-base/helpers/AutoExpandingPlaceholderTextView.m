
#import "AutoExpandingPlaceholderTextView.h"
@interface AutoExpandingPlaceholderTextView()
@property (nonatomic, strong) UILabel *placeHolderLabel;
@end

@implementation AutoExpandingPlaceholderTextView

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setup];
}

- (id)initWithFrame:(CGRect)frame
{
    if( (self = [super initWithFrame:frame]) )
    {
        [self setup];
    }
    return self;
}

- (void)setup {
    self.shouldExpand = YES;
    // Use Interface Builder User Defined Runtime Attributes to set
    // placeholder and placeholderColor in Interface Builder.
    
    if (!self.placeholder) {
        [self setPlaceholder:@""];
    }
    
    if (!self.placeholderColor) {
        [self setPlaceholderColor:[UIColor lightGrayColor]];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextViewTextDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textBeginEditing:) name:UITextViewTextDidBeginEditingNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textDidEndEditing:) name:UITextViewTextDidEndEditingNotification object:nil];
    
}

- (void)textBeginEditing:(NSNotification *)notification {
    if (notification.object == self &&
        [self.expandingDelegate respondsToSelector:@selector(textViewBeingEdited:)])
        [self.expandingDelegate textViewBeingEdited:self];
}

- (void)textChanged:(NSNotification *)notification
{
    if([[self placeholder] length] == 0)
    {
        return;
    }
    
    if([[self text] length] == 0)
    {
        [[self viewWithTag:999] setAlpha:1];
    }
    else
    {
        [[self viewWithTag:999] setAlpha:0.0];
    }
    if (notification.object == self &&
        [self.expandingDelegate respondsToSelector:@selector(textViewBeingEdited:)])
        [self.expandingDelegate textViewBeingEdited:self];
}

- (void)textDidEndEditing:(NSNotification *)notification {
    if (notification.object == self &&
        [self.expandingDelegate respondsToSelector:@selector(textViewDidEndEditing:)])
        [self.expandingDelegate textViewDidEndEditing:self];
}

- (void)setText:(NSString *)text {
    [super setText:text];
    [self textChanged:nil];
}

- (void)drawRect:(CGRect)rect
{
    if( [[self placeholder] length] > 0 )
    {
        if (_placeHolderLabel == nil )
        {
            // on ios 7 placeholder label should start from 7px
            
            _placeHolderLabel = [[UILabel alloc] initWithFrame:CGRectMake(IS_IOS_7?5:8,8,self.bounds.size.width - 16,25)];
            _placeHolderLabel.lineBreakMode = NSLineBreakByWordWrapping;
            _placeHolderLabel.numberOfLines = 0;
            _placeHolderLabel.font = self.font;
            _placeHolderLabel.backgroundColor = [UIColor clearColor];
            _placeHolderLabel.textColor = self.placeholderColor;
            _placeHolderLabel.alpha = 0;
            _placeHolderLabel.tag = 999;
            [self addSubview:_placeHolderLabel];
        }
        
        _placeHolderLabel.text = self.placeholder;
        [_placeHolderLabel sizeToFit];
        [self sendSubviewToBack:_placeHolderLabel];
    }
    
    if( [[self text] length] == 0 && [[self placeholder] length] > 0 )
    {
        [[self viewWithTag:999] setAlpha:1];
    }
    
    [super drawRect:rect];
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    
    if (_shouldExpand && !CGSizeEqualToSize(self.bounds.size, [self intrinsicContentSize])) {
        [self invalidateIntrinsicContentSize];
    }
}

- (CGSize)intrinsicContentSize
{
    if (self.shouldExpand==NO) return [super intrinsicContentSize];
    
    CGSize intrinsicContentSize = self.contentSize;
    
    if (IS_IOS_7) {
        intrinsicContentSize.width += (self.textContainerInset.left + self.textContainerInset.right ) / 2.0f;
        intrinsicContentSize.height += (self.textContainerInset.top + self.textContainerInset.bottom) / 4.0f;
    }
    
    return intrinsicContentSize;
}


- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
