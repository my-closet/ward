
#import "UIRoundedImageView.h"

@implementation UIRoundedImageView

-(void)setImage:(UIImage *)image{
    
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, NO, [[UIScreen mainScreen] scale]);
    [[UIBezierPath bezierPathWithRoundedRect:self.bounds
                                cornerRadius:(_cornerRadius > 0 ? _cornerRadius : (self.bounds.size.height/2.0f))] addClip];
    [image drawInRect:self.bounds];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [super setImage:newImage];
}

@end
