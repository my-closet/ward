
#import <Foundation/Foundation.h>

@interface NSDictionary (STExtensions)
- (NSDictionary*)dictionaryContainingIdKey;
- (NSArray*)dictionariesArrayContainingIdKey;

- (NSArray*)dictionariesArrayContainingKey:(NSString*)key;
- (NSDictionary*)dictionaryContainingKey:(NSString*)key;
@end

@interface NSArray (STExtensions)
- (NSDictionary*)dictionaryContainingIdKey;
- (NSArray*)dictionariesArrayContainingIdKey;

- (NSArray*)dictionariesArrayContainingKey:(NSString*)key;
- (NSDictionary*)dictionaryContainingKey:(NSString*)key;
@end