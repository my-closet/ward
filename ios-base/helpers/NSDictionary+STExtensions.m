
#import "NSDictionary+STExtensions.h"

@implementation NSDictionary (STExtensions)
-(NSDictionary *)dictionaryContainingIdKey{
    return [self dictionaryContainingKey:@"id"];
}
- (NSArray *)dictionariesArrayContainingIdKey{
    return [self dictionariesArrayContainingKey:@"id"];
}
- (NSArray *)dictionariesArrayContainingKey:(NSString *)key{
    if ([self allKeys].count == 0) return nil;
    
    NSArray *array = nil;
    
    for (id object in [self allValues]){
        if ([object isKindOfClass:[NSArray class]]){
            array = object;
            break;
        }
    }
    
    NSInteger numberOfTries = 10;
    
    BOOL firstObjectIsDict = [[array firstObject] isKindOfClass:[NSDictionary class]];
    
    BOOL containsKey = firstObjectIsDict && ([[array firstObject] dictionaryContainingKey:key]);
    
    NSDictionary *currentDict = self;
    NSInteger currentTry = 0;
    while (!containsKey && currentTry < numberOfTries) {
        currentTry++;
        
        NSArray *tempArray = nil;
        for (id object in [currentDict allValues]){
            if ([object isKindOfClass:[NSArray class]]){
                tempArray = object;
                break;
            }
        }
        if (tempArray){
            firstObjectIsDict = [[array firstObject] isKindOfClass:[NSDictionary class]];
            containsKey = firstObjectIsDict && ([[array firstObject] dictionaryContainingKey:key]);
        } else {
            NSDictionary *tempDict = nil;
            for (id object in [currentDict allValues]){
                if ([object isKindOfClass:[NSDictionary class]]){
                    tempDict = object;
                    break;
                }
            }
            if (tempDict)
                currentDict = tempDict;
            else
                return nil;
        }
    }
    
    
    return array;
}
- (NSDictionary *)dictionaryContainingKey:(NSString *)key{
    if ([self allKeys].count == 0) return nil;
    
    BOOL containsKey = ([self objectForKeyOrNil:key] != nil);
    NSDictionary *currentDict = self;
    
    NSInteger numberOfTries = 10;

    NSInteger currentTry = 0;
    while (!containsKey && currentTry < numberOfTries) {
        currentTry++;
        NSDictionary *tempDict = nil;
        for (id object in [currentDict allValues]){
            if ([object isKindOfClass:[NSDictionary class]]){
                tempDict = object;
                break;
            }
        }
        if (tempDict){
            currentDict = tempDict;
        } else {
            return nil;
        }
        
        containsKey = ([currentDict objectForKeyOrNil:key] != nil);
    }
    if ([currentDict objectForKeyOrNil:key])
        return currentDict;
    else
        return nil;
}
@end

@implementation NSArray (STExtensions)
-(NSDictionary *)dictionaryContainingIdKey{
    return [self dictionaryContainingKey:@"id"];
}
- (NSArray *)dictionariesArrayContainingIdKey{
    return [self dictionariesArrayContainingKey:@"id"];
}
- (NSArray *)dictionariesArrayContainingKey:(NSString *)key{
    BOOL firstObjectIsDict = [[self firstObject] isKindOfClass:[NSDictionary class]];
    BOOL containsKey = firstObjectIsDict && ([[self firstObject] dictionaryContainingKey:key]);
    if (containsKey)
        return self;
    else
        return nil;
}
- (NSDictionary *)dictionaryContainingKey:(NSString *)key{
    return nil;
}
@end
