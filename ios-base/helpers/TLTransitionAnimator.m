//
//  TLTransitionAnimator.m
//  UIViewController-Transitions-Example
//
//  Created by Ash Furrow on 2013-07-18.
//  Copyright (c) 2013 Teehan+Lax. All rights reserved.
//

#import "TLTransitionAnimator.h"

@implementation TLTransitionAnimator

- (void)animationEnded:(BOOL)transitionCompleted {
    self.presenting = NO;
}

- (NSTimeInterval)transitionDuration:(id <UIViewControllerContextTransitioning>)transitionContext {
    // Used only in non-interactive transitions, despite the documentation
    return 0.3f;
}

- (void)animateTransition:(id <UIViewControllerContextTransitioning>)transitionContext {
    
    UIView *fromView = nil;//[transitionContext viewForKey:UITransitionContextFromViewKey];
    UIView *toView = nil;//[transitionContext viewForKey:UITransitionContextToViewKey];
    
    if (fromView == nil){
        UIViewController *fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
        fromView = fromViewController.view;
    }
    if (toView == nil){
        UIViewController *toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
        toView = toViewController.view;
    }

    CGRect endFrame = [[transitionContext containerView] bounds];
    
    if (self.presenting) {
        
        CGRect startFrame = endFrame;
        startFrame.origin.y = 0;
        startFrame.origin.x = 0;
        
        fromView.frame = startFrame;
        toView.frame = startFrame;
        
        endFrame.origin.y = CGRectGetHeight([[transitionContext containerView] bounds]);
        
        // The order of these matters – determines the view hierarchy order.
        
        [transitionContext.containerView addSubview:toView];
        [transitionContext.containerView addSubview:fromView];
        
        [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
            fromView.frame = endFrame;
        } completion:^(BOOL finished) {
            [transitionContext completeTransition:YES];
        }];
    }
    else {
        CGRect startFrame = endFrame;
        startFrame.origin.y = CGRectGetHeight([[transitionContext containerView] bounds]);
        toView.frame = startFrame;
        
        [transitionContext.containerView addSubview:fromView];
        [transitionContext.containerView addSubview:toView];
        
        [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
            toView.frame = endFrame;
        } completion:^(BOOL finished) {
            [transitionContext completeTransition:YES];
            [ApplicationDelegate.window addSubview:toView];
        }];
    }
    
}



@end