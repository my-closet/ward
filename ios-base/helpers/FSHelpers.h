#import <UIKit/UIKit.h>
#import "Macros.h"
#import "NSDate+Extensions.h"
#import "UIImage+Utils.h"
#import "UIImage+ImageEffects.h"
//if you have SDWebImage added to project - you can use helper for
//uploading, caching and setting images to UIImageView
//You just need to uncomment next line
#define SDWebImage
#import "UIImageView+Utils.h"
#import "UIRoundedImageView.h"
static inline void dispatch_after_short(CGFloat delay, dispatch_block_t block){
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC)), dispatch_get_main_queue(), block);
};

#pragma mark - UIView+Utils

@interface UIView (Utils)
- (CGFloat)width;
- (CGFloat)height;
- (CGFloat)rightX;
- (CGFloat)bottomY;

- (void)setHeight:(CGFloat)height;
- (void)setWidth:(CGFloat)width;

- (CGFloat)x;
- (CGFloat)y;
- (void)setX:(CGFloat)originX;
- (void)setY:(CGFloat)originY;

- (void)moveTo:(CGPoint)position;

- (void)removeAllSubviews;

-(UIView*)addBottomLine;
-(UIView*)addTopLine;

// negative value to remove height/width
- (void)addHeight:(CGFloat)height;
- (void)addWidth:(CGFloat)width;
// negative value to move up
-(void) addOriginX:(CGFloat)originX;
-(void) addOriginY:(CGFloat)originY;
- (void)setOrigin:(CGPoint)position ;
// Layout views
- (CGSize)prefferedSize;

- (void)layoutAllSubviewsVertically;
- (void)layoutAllSubviewsVerticallyFromBottom;

-(void) addGradient;
- (BOOL)findAndResignFirstResponder;

- (NSMutableArray*)allSubViews;
- (NSMutableArray*)allLabelsButtonsAndTextFields;
- (NSMutableArray*)allLabelsButtonsTextFieldsAndTextViews;
- (NSMutableArray*)allViewsWithAccessibilityLabel:(NSString*)label;

-(void) enableCircleMask;
@end

#pragma mark - UIScrollView+Utls

@interface UIScrollView (Utils)
- (void)addContentWidth:(CGFloat)width;
- (void)addContentHeight:(CGFloat)height;
@end

#pragma mark - UILabel+Utils

@interface UILabel (Utils)
- (UILabel*)makeCopyWithFontSize:(CGFloat) fontSize;
- (CGSize)prefferedSize;
- (CGFloat)preferredWidth;
- (CGFloat)preferredHeight;

// These methods are better than stretchToPrefferedWidth, stretchToPrefferedHeight
- (void) resizeVerticallyToFit;
- (void) resizeHorizontallyToFit;

- (void) resizeVerticallyToFitWithSizeThatFitsMethod;
- (void) resizeHorizontallyToFitWithSizeThatFitsMethod;

- (void)stretchToPrefferedWidth;
- (void)stretchToPrefferedHeight;
- (void)highlightTextWithRange:(NSRange)range;
@end

#pragma mark - NSDictionary+Utils

@interface NSDictionary (Utils)

- (id)objectWithFailoverKeys:(NSArray *)keys;
- (id)objectForKeyOrDefault:(id)aKey aDefault:(id)aDefault;
- (id)objectForKeyOrEmptyString:(id)aKey;
- (id)objectForKeyOrNil:(id)aKey;
- (NSInteger)intForKeyOrDefault:(id)aKey aDefault:(NSInteger)aDefault;
- (BOOL)boolForKeyOrDefault:(id)aKey aDefault:(BOOL)aDefault;
- (id)objectForInt:(NSInteger)anInt;
- (NSString *)toCoreDataRequestString;
- (NSString *)toHttpRequestString;

@end

#pragma mark - NSMutableDictionary+Utils

@interface NSMutableDictionary (Utils)
- (void)setArrayOrEmptyString:(NSArray*)array forKey:(id)aKey;
- (void)setObjectIfNotNil:(id)anObject forKey:(id)aKey;
- (void)trimValues;

@end

#pragma mark - NSArray+Utils

@interface NSArray (WRK)
- (id) objectAtIndexOrNil:(NSInteger)index;
- (NSArray *)containNotObjects:(NSArray *)array;
- (id)findObjectWithBlock:(BOOL (^)(id obj, NSUInteger idx))block;
- (NSArray *)mapObjectsUsingBlock:(id (^)(id obj, NSUInteger idx))block;

- (NSArray*)indexPathsOfObjectsFromArray:(NSArray*)array forItemOrRow:(BOOL)itemOrRow;
@end

#pragma mark - NSSet

@interface NSSet (Utils)
- (id)findWithBlock:(BOOL (^)(id obj))block;
@end

#pragma mark - NSString+Utils

@interface NSString (Utils)
- (NSString *)URLEncodedString;
- (NSString *)URLDecodedString;
- (NSString *)LightURLEncodeString;
+ (BOOL)emailValidate:(NSString *)email;
- (CGSize)sizeForStringWithFont:(UIFont *)font constrainedToSize:(CGSize)size;
+ (NSString *)GetUUID;
/**
 * Функция возвращает окончание для множественного числа слова на основании числа и массива окончаний
 * @param  iNumber Integer Число на основе которого нужно сформировать окончание
 * @param  aEndings Array Массив слов или окончаний для чисел (1, 4, 5),
 *         например ['яблоко', 'яблока', 'яблок']
 * @return String
 */
+ (NSString *)getNumEndingForNumber:(int)iNumber endingsArray:(NSArray *)aEndings;
@end
@interface NSAttributedString (Utils)
- (CGSize)sizeForStringWithFont:(UIFont *)font constrainedToSize:(CGSize)size;
@end

#pragma mark - NSObject+Utils

@interface NSObject (Utils)

- (BOOL)isEmpty;

@end

#pragma mark - UITableView+Utils

@interface UITableView (Utils)
- (void)deselectSelectedRow;
@end


@interface UITextView (Utils)
- (CGSize) sizeWithText:(NSString*)text;
@end

@interface UIButtonWithObject : UIButton
@property (nonatomic, strong) id objectStrong;
@property (nonatomic, weak) id objectWeak;
@end

