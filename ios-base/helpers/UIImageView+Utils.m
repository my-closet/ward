#ifdef SDWebImage
#import "SDImageCache.h"
#endif
#import "FSHelpers.h"

@implementation UIImageView (Utils)

#pragma mark - Image caching

#ifdef SDWebImage
- (void)setImageWithCachingKey:(NSString *)key andPlaceholderImageName:(NSString *)placeholderName {
    SDImageCache *imageCache = [SDImageCache sharedImageCache];
    UIImage *imageFromMemory = [imageCache imageFromMemoryCacheForKey:key];
    if (!imageFromMemory) {
        [imageCache queryDiskCacheForKey:key done:^(UIImage *image, SDImageCacheType cacheType) {
            if (image)
                self.image = image;
            else
                [self sd_setImageWithURL:[NSURL URLWithString:key]
                        placeholderImage:[UIImage imageNamed:placeholderName]
                               completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                   if (image)
                                       [imageCache storeImage:image forKey:key toDisk:YES];
                                   
                               }];
        }];
    } else {
        self.image = imageFromMemory;
    }
}

- (void)setImageWithCachingKey:(NSString *)key andPlaceholderImage:(UIImage *)placeholderImage {
    SDImageCache *imageCache = [SDImageCache sharedImageCache];
    UIImage *imageFromMemory = [imageCache imageFromMemoryCacheForKey:key];
    if (!imageFromMemory) {
        [imageCache queryDiskCacheForKey:key done:^(UIImage *image, SDImageCacheType cacheType) {
            if (image)
                self.image = image;
            else
                [self sd_setImageWithURL:[NSURL URLWithString:key]
                        placeholderImage:placeholderImage
                               completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                   if (image)
                                       [imageCache storeImage:image forKey:key toDisk:YES];
                                   
                               }];
        }];
    } else {
        self.image = imageFromMemory;
    }
}
- (void)softeam_setImageWithURL:(NSURL *)url placeholderImage:(UIImage *)placeholderImage{
    
    __weak UIImageView *anAvatarView = self;
    
    [self sd_setImageWithURL:url completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (error){
            anAvatarView.image = placeholderImage;
        }
    }];
}
#endif

#pragma mark - Rounded images

- (void)sd_setCircleImageWithURL:(NSURL *)url placeholderImage:(UIImage *)placeholder options:(SDWebImageOptions)options completed:(Callback)completedBlock{
    
    __weak UIImageView *anAvatarView = self;
    
    [self sd_setImageWithURL:url placeholderImage:placeholder options:options completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (error){
            DLog(@"error avatar:%@ url:'%@'",error, imageURL);
        } else if (anAvatarView){
            UIGraphicsBeginImageContextWithOptions(anAvatarView.bounds.size, NO, [[UIScreen mainScreen] scale]);
            [[UIBezierPath bezierPathWithRoundedRect:anAvatarView.bounds
                                        cornerRadius:anAvatarView.bounds.size.height/2.0f] addClip];
            [image drawInRect:anAvatarView.bounds];
            anAvatarView.image = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            [anAvatarView setNeedsLayout];
            
            BLOCK_SAFE_RUN(completedBlock);
        }
    }];
}
- (void)circularCrop{
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, NO, [[UIScreen mainScreen] scale]);
    [[UIBezierPath bezierPathWithRoundedRect:self.bounds
                                cornerRadius:self.bounds.size.height/2.0f] addClip];
    [self.image drawInRect:self.bounds];
    self.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [self setNeedsLayout];
}
-(void) enableCircleMaskWithMaskRect:(CGRect)maskRect {
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathAddEllipseInRect(path, nil, maskRect);
    maskLayer.path = path;
    CGPathRelease(path);
    self.layer.mask = maskLayer;
}

- (void)makeImageRounded {
    self.clipsToBounds = YES;
    self.layer.cornerRadius = self.width/2;
    self.layer.borderColor = [UIColor whiteColor].CGColor;
    self.layer.borderWidth = 0.5f;
}


@end
