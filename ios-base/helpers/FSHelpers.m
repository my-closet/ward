#import "FSHelpers.h"
#import <QuartzCore/QuartzCore.h>

#pragma mark - UIView+Utils

@implementation UIView (Utils)

- (CGFloat)width {
    return self.frame.size.width;
}

- (CGFloat)height {
    return self.frame.size.height;
}

-(CGFloat)rightX{
    return self.frame.origin.x + self.frame.size.width;
}

-(CGFloat)bottomY{
    return self.frame.origin.y + self.frame.size.height;
}

- (void)setHeight:(CGFloat)height {
    CGRect frame = self.frame;
    frame.size.height = height;
    [self setFrame:frame];
}

- (void)setWidth:(CGFloat)width {
    CGRect frame = self.frame;
    frame.size.width = width;
    [self setFrame:frame];
}


- (CGFloat)x {
    return self.frame.origin.x;
}

- (CGFloat)y {
    return self.frame.origin.y;
}
-(void) setX:(CGFloat)originX{
    CGRect frame = self.frame;
    frame.origin.x = originX;
    [self setFrame:frame];
}

-(void) setY:(CGFloat)originY{
    CGRect frame = self.frame;
    frame.origin.y = originY;
    [self setFrame:frame];
}

- (void)moveTo:(CGPoint)position {
    CGRect frame = self.frame;
    frame.origin = position;
    [self setFrame:frame];
}

- (void)removeAllSubviews {
    for (UIView *view in [self subviews]) {
        [view removeFromSuperview];
    }
}

-(UIView*)addBottomLine{
    CGRect frame = {{0, self.height - ONE_PIXEL}, {self.width, ONE_PIXEL}};
    UIView *v = [[UIView alloc] initWithFrame:frame];
    v.backgroundColor = RGB(200,200, 200);
    [self addSubview:v];
//    v.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    return v;
}

-(UIView*)addTopLine{
    CGRect frame = {{0, 0}, {self.width, ONE_PIXEL}};
    UIView *v = [[UIView alloc] initWithFrame: frame];
    v.backgroundColor = RGB(200,200, 200);
    [self addSubview:v];
    return v;
}


- (void)addHeight:(CGFloat)height{
    CGRect frame = self.frame;
    frame.size.height = frame.size.height+ height;
    [self setFrame:frame];
}
- (void)addWidth:(CGFloat)width {
    CGRect frame = self.frame;
    frame.size.width = frame.size.width + width;
    [self setFrame:frame];
}

-(void) addOriginX:(CGFloat)originX{
    CGRect frame = self.frame;
    frame.origin.x = frame.origin.x + originX;
    [self setFrame:frame];
}
-(void) addOriginY:(CGFloat)originY{
    CGRect frame = self.frame;
    frame.origin.y = frame.origin.y + originY;
    [self setFrame:frame];
}

- (void)setOrigin:(CGPoint)position {
    CGRect frame = self.frame;
    frame.origin = position;
    [self setFrame:frame];
}

- (CGSize)prefferedSize {
    return self.frame.size;
}

- (void)layoutAllSubviewsVertically {
    CGFloat totalHeight = 0.0f;
    for (UIView *view in self.subviews) {
        if (![view isKindOfClass:[UIImageView class]] && !view.isHidden) {
            CGSize prefferedSize = [view prefferedSize];
            [view setHeight:prefferedSize.height];
            [view setWidth:self.width];
            [view moveTo:CGPointMake(0.0f, totalHeight)];
            
            totalHeight += prefferedSize.height;
        }
    }
    CGFloat heightToSet = totalHeight;
    [self setHeight:heightToSet];
}

- (void)layoutAllSubviewsVerticallyFromBottom {
    CGFloat totalHeight = [self height];
    for (UIView *view in self.subviews) {
        if (!view.isHidden) {
            CGSize prefferedSize = [view prefferedSize];
            [view setHeight:prefferedSize.height];
            [view setWidth:self.width];
            
            totalHeight -= prefferedSize.height;
            [view moveTo:CGPointMake(0.0f, totalHeight)];
        }
    }
}

-(void) addGradient{
    
    // Add Border
    CALayer *layer = self.layer;
    layer.cornerRadius = 10.0f;
    layer.masksToBounds = YES;
    layer.borderWidth = 1.0f;
    layer.borderColor = [UIColor colorWithWhite:0.5f alpha:0.2f].CGColor;
    
    // Add Shine
    CAGradientLayer *shineLayer = [CAGradientLayer layer];
    shineLayer.frame = layer.bounds;
    shineLayer.colors = [NSArray arrayWithObjects:
                         (id)[UIColor colorWithWhite:1.0f alpha:0.4f].CGColor,
                         (id)[UIColor colorWithWhite:1.0f alpha:0.2f].CGColor,
                         (id)[UIColor colorWithWhite:0.75f alpha:0.2f].CGColor,
                         (id)[UIColor colorWithWhite:0.4f alpha:0.2f].CGColor,
                         (id)[UIColor colorWithWhite:1.0f alpha:0.4f].CGColor,
                         nil];
    shineLayer.locations = [NSArray arrayWithObjects:
                            [NSNumber numberWithFloat:0.0f],
                            [NSNumber numberWithFloat:0.5f],
                            [NSNumber numberWithFloat:0.5f],
                            [NSNumber numberWithFloat:0.8f],
                            [NSNumber numberWithFloat:1.0f],
                            nil];
    [layer addSublayer:shineLayer];
}

- (BOOL)findAndResignFirstResponder
{
    if (self.isFirstResponder) {
        [self resignFirstResponder];
        NSLog(@"first responder was: %@",self);
        return YES;
    }
    for (UIView *subView in self.subviews) {
        if ([subView findAndResignFirstResponder])
            return YES;
    }
    return NO;
}

- (NSMutableArray*)allSubViews{
    NSMutableArray *arr= [NSMutableArray array];
    [arr addObject:self];
    for (UIView *subview in self.subviews){
        [arr addObjectsFromArray:(NSArray*)[subview allSubViews]];
    }
    return arr;
}

- (NSMutableArray*)allLabelsButtonsAndTextFields{
    NSMutableArray *arr= [NSMutableArray array];
    if ([self isKindOfClass:[UILabel class]] || [self isKindOfClass:[UIButton class]] || [self isKindOfClass:[UITextField class]])
        [arr addObject:self];
    else
        for (UIView *subview in self.subviews)    {
            [arr addObjectsFromArray:(NSArray*)[subview allLabelsButtonsAndTextFields]];
        }
    return arr;
}

- (NSMutableArray*)allLabelsButtonsTextFieldsAndTextViews{
    NSMutableArray *arr= [NSMutableArray array];
    if ([self isKindOfClass:[UILabel class]] ||
        [self isKindOfClass:[UIButton class]] ||
        [self isKindOfClass:[UITextField class]] ||
        [self isKindOfClass:[UITextView class]])
        [arr addObject:self];
    else
        for (UIView *subview in self.subviews)    {
            [arr addObjectsFromArray:(NSArray*)[subview allLabelsButtonsTextFieldsAndTextViews]];
        }
    return arr;
}
- (NSMutableArray*)allViewsWithAccessibilityLabel:(NSString*)label{
    NSMutableArray *arr= [NSMutableArray array];
    if ([[self accessibilityLabel] isEqualToString:label])
        [arr addObject:self];
    for (UIView *subview in self.subviews) {
        [arr addObjectsFromArray:(NSArray*)[subview allViewsWithAccessibilityLabel:label]];
    }
    return arr;
}
-(void) enableCircleMask{
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathAddEllipseInRect(path, nil, self.bounds);
    maskLayer.path = path;
    CGPathRelease(path);
    self.layer.mask = maskLayer;
}
@end

#pragma mark - UIScrollView+Utls

@implementation UIScrollView (Utils)
- (void)addContentWidth:(CGFloat)width {
    CGSize contentSize = self.contentSize;
    self.contentSize = CGSizeMake(contentSize.width + width, contentSize.height);
}

- (void)addContentHeight:(CGFloat)height {
    CGSize contentSize = self.contentSize;
    self.contentSize = CGSizeMake(contentSize.width, contentSize.height + height);
}
@end

#pragma mark - UILabel+Utils

@implementation UILabel (Utils)

-(UILabel*) makeCopyWithFontSize:(CGFloat) fontSize{
    UILabel *label = [[UILabel alloc] init];
    label.font = [UIFont fontWithName:self.font.familyName size:fontSize];
    label.backgroundColor = self.backgroundColor;
    label.text = self.text;
    label.frame = self.frame;
    label.textColor = self.textColor;
    label.shadowColor = self.shadowColor;
    label.shadowOffset = self.shadowOffset;
    label.textAlignment = self.textAlignment;
    return label;
}

- (CGSize)prefferedSize {
    return CGSizeMake(self.width, [self preferredHeight]);
}

- (CGFloat)preferredWidth {
    CGRect frame = CGRectMake(0, 0, 0, self.frame.size.height);
    UILabel *testLabel = [[UILabel alloc] initWithFrame:frame];
    testLabel.text = self.text;
    testLabel.font = self.font;
    testLabel.textAlignment = self.textAlignment;
    testLabel.lineBreakMode = self.lineBreakMode;
    testLabel.numberOfLines = 0;
    [testLabel sizeToFit];
    CGFloat result = testLabel.frame.size.width;
    return result;
}

- (CGFloat)preferredHeight {
    CGRect frame = CGRectMake(0, 0, self.frame.size.width, 0);
    UILabel *testLabel = [[UILabel alloc] initWithFrame:frame];
    testLabel.text = self.text;
    testLabel.font = self.font;
    testLabel.textAlignment = self.textAlignment;
    testLabel.lineBreakMode = self.lineBreakMode;
    testLabel.numberOfLines = 0;
    [testLabel sizeToFit];
    CGFloat preferredCellHeight = testLabel.frame.size.height;
    return preferredCellHeight;
}

- (void)stretchToPrefferedWidth {
    CGRect frame = self.frame;
    frame.size.width = [self preferredWidth] + 3.0f;
    self.frame = frame;
}

- (void)stretchToPrefferedHeight {
    CGRect frame = self.frame;
    frame.size.height = [self preferredHeight];
    self.frame = frame;
}
- (void) resizeVerticallyToFitWithSizeThatFitsMethod{
    CGSize size = [self sizeThatFits:CGSizeMake(self.width, CGFLOAT_MAX)];
    self.height = ceilf(size.height) + 2;
}
- (void) resizeHorizontallyToFitWithSizeThatFitsMethod{
    CGSize size = [self sizeThatFits:CGSizeMake(CGFLOAT_MAX, self.height)];
    self.width = ceilf(size.width) + 2;
}
- (void) resizeVerticallyToFit{
    
    CGSize size;
    if (self.attributedText){
        size = [self.attributedText sizeForStringWithFont:self.font
                                        constrainedToSize:CGSizeMake(self.width, CGFLOAT_MAX)];
    } else {
        size = [self.text sizeForStringWithFont:self.font
                              constrainedToSize:CGSizeMake(self.width, CGFLOAT_MAX)];
    }
    self.height = size.height;
}
- (void) resizeHorizontallyToFit{
    CGSize size;
    if (self.attributedText){
        size = [self.attributedText sizeForStringWithFont:self.font
                                        constrainedToSize:CGSizeMake(CGFLOAT_MAX, self.height)];
    } else {
        size = [self.text sizeForStringWithFont:self.font
                              constrainedToSize:CGSizeMake(CGFLOAT_MAX, self.height)];
    }
    self.width = size.width;
}

-(void)highlightTextWithRange:(NSRange)range{
    if ([self respondsToSelector:@selector(setAttributedText:)]){
        
        UIFont *boldFont = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14.0f];
        UIFont *regularFont = [UIFont fontWithName:@"HelveticaNeue" size:14.0f];
        // Create the attributes
        NSDictionary *attrs = [NSDictionary dictionaryWithObjectsAndKeys:
                               regularFont, NSFontAttributeName,
                               nil];
        NSDictionary *attrsBold = [NSDictionary dictionaryWithObjectsAndKeys:
                                   boldFont, NSFontAttributeName,
                                   nil];
        
        // Create the attributed string (text + attributes)
        NSMutableAttributedString *attributedText =
        [[NSMutableAttributedString alloc] initWithString:self.text
                                               attributes:attrs];
        [attributedText setAttributes:attrsBold range:range];
        
        // Set it in our UILabel and we are done!
        [self setAttributedText:attributedText];
    }
}

@end

#pragma mark - NSDictionary+Utils

@implementation NSDictionary (Utils)

- (id)objectWithFailoverKeys:(NSArray *)keys {
    id object = nil;
    for (id key in keys) {
        object = [self objectForKeyOrNil:key];
        if (object)
            break;
    }
    return object;
}

- (id)objectForKeyOrDefault:(id)aKey aDefault:(id)aDefault {
    id obj = nil;
    if ([self respondsToSelector:@selector(objectForKey:)]) {
        obj = [self objectForKey:aKey];
    }
    return (!obj || obj == [NSNull null]) ? aDefault : obj;
}

- (id)objectForKeyOrEmptyString:(id)aKey {
    return [self objectForKeyOrDefault:aKey aDefault:@""];
}

- (id)objectForKeyOrNil:(id)aKey {
    return [self objectForKeyOrDefault:aKey aDefault:nil];
}

- (NSInteger)intForKeyOrDefault:(id)aKey aDefault:(NSInteger)aDefault {
    id object = [self objectForKeyOrDefault:aKey aDefault:nil];
    if (object) {
        if ([object respondsToSelector:@selector(intValue)]) {
            return [object intValue];
        }
    }
    return aDefault;
}

- (BOOL)boolForKeyOrDefault:(id)aKey aDefault:(BOOL)aDefault {
    id object = [self objectForKeyOrDefault:aKey aDefault:nil];
    if (object) {
        if ([object respondsToSelector:@selector(boolValue)]) {
            return [object boolValue];
        }
    }
    return aDefault;
}

- (id)objectForInt:(NSInteger)anInt {
    return [self objectForKey:[NSNumber numberWithInteger:anInt]];
}

- (NSString *)toHttpRequestString {
    NSMutableArray *arrayOfPairs = [NSMutableArray arrayWithCapacity:[self allKeys].count];
    for (NSString *key in [self allKeys]) {
        NSString *value = [self valueForKey:key];
        NSString *pair = [NSString stringWithFormat:@"%@=%@", key, value];
        [arrayOfPairs addObject:pair];
    }
    NSString *toReturn = [arrayOfPairs componentsJoinedByString:@"&"];
    return toReturn;
}

- (NSString *)toCoreDataRequestString {
    NSString *toReturn = @"(YES == YES)";
    for (NSString *key in [self allKeys]) {
        NSString *value = [self valueForKey:key];
        NSString *pair = [NSString stringWithFormat:@" AND (%@ == %@)", key, value];
        toReturn = [toReturn stringByAppendingString:pair];
    }
    return toReturn;
}

@end

#pragma mark - NSMutableDictionary+Utils

@implementation NSMutableDictionary (Utils)
- (void)setArrayOrEmptyString:(NSArray*)array forKey:(id)aKey{
    if ([array isKindOfClass:[NSArray class]] && array.count>0) {
        [self setObject:array forKey:aKey];
    } else {
        [self setObject:@"" forKey:aKey];
    }
}

- (void)setObjectIfNotNil:(id)anObject forKey:(id)aKey {
    if (anObject) {
        [self setObject:anObject forKey:aKey];
    }
}

- (void)trimValues {
    NSArray *allKeys = [NSArray arrayWithArray:[self allKeys]];
    for (id key in allKeys) {
        id object = [self objectForKey:key];
        if ([object isKindOfClass:[NSString class]]) {
            NSString *value = (NSString *)object;
            NSString *trimmedString = [value stringByTrimmingCharactersInSet:
                                       [NSCharacterSet whitespaceAndNewlineCharacterSet]];
            [self setObject:trimmedString forKey:key];
        }
    }
}

@end

#pragma mark - NSArray+Utils

@implementation NSArray (Utils)

- (id) objectAtIndexOrNil:(NSInteger)index{
    if (index<self.count && index>=0)
        return [self objectAtIndex:index];
    return nil;
}

- (NSArray *)containNotObjects:(NSArray *)array
{
    NSMutableArray *result = [NSMutableArray new];
    for (id object in array) {
        if (![self containsObject:object]) {
            [result addObject:object];
        }
    }
    return result;
}

- (id)findObjectWithBlock:(BOOL (^)(id obj, NSUInteger idx))block {
    __block id result = nil;
    [self enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        BOOL found = block(obj, idx);
        if (found) {
            result = obj;
            *stop = YES;
        }
    }];
    return result;
}

- (NSArray *)mapObjectsUsingBlock:(id (^)(id obj, NSUInteger idx))block {
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:[self count]];
    [self enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [result addObject:block(obj, idx)];
    }];
    return result;
}

- (NSArray*)indexPathsOfObjectsFromArray:(NSArray*)array forItemOrRow:(BOOL)itemOrRow{
    NSMutableArray *targetArray = [NSMutableArray array];
    for (id object in array){
        NSInteger index = [self indexOfObject:object];
        if (itemOrRow)
            [targetArray addObject:[NSIndexPath indexPathForItem:index inSection:0]];
        else
            [targetArray addObject:[NSIndexPath indexPathForRow:index inSection:0]];
    }
    return targetArray;
}

@end

#pragma mark - NSSet

@implementation NSSet (Utils)

- (id)findWithBlock:(BOOL (^)(id obj))block
{
	NSParameterAssert(block != nil);
    
	return [[self objectsPassingTest:^BOOL(id obj, BOOL *stop) {
		if (block(obj)) {
			*stop = YES;
			return YES;
		}
        
		return NO;
	}] anyObject];
}

@end

#pragma mark - NSString+Utils

@implementation NSAttributedString (Utils)

- (CGSize)sizeForStringWithFont:(UIFont *)font constrainedToSize:(CGSize)size {
    
    CGSize boundingSize = size;
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineBreakMode:NSLineBreakByWordWrapping];
    
    CGRect contentRect = [self boundingRectWithSize:boundingSize
                                            options:(NSStringDrawingUsesLineFragmentOrigin)
                                            context:NULL];
    CGSize contentSize = CGSizeMake(ceilf(contentRect.size.width), ceilf(contentRect.size.height));
    
    return contentSize;
}

@end

@implementation NSString (Utils)

- (NSString *)URLEncodedString {
	NSString * encodedString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef)self,
                                                                                                     NULL, (CFStringRef)@"!*'();@&=+$,?%#[]",
                                                                                                     kCFStringEncodingUTF8));
	return encodedString;
}

- (NSString *)URLDecodedString {
    return [self stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}

- (NSString *)LightURLEncodeString {
    NSMutableString *tempStr = [NSMutableString stringWithString:self];
    [tempStr replaceOccurrencesOfString:@" " withString:@"+" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [tempStr length])];
    return [[NSString stringWithFormat:@"%@",tempStr] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}

+ (BOOL)emailValidate:(NSString *)email {
    NSString *emailRegEx =
    @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
    @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    
    NSPredicate *regExPredicate =
    [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    return [regExPredicate evaluateWithObject:email];
}

- (CGSize)sizeForStringWithFont:(UIFont *)font constrainedToSize:(CGSize)size {
    
    CGSize boundingSize = size;
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineBreakMode:NSLineBreakByWordWrapping];
    
    NSDictionary *attributes = @{ NSFontAttributeName: font, NSParagraphStyleAttributeName : paragraphStyle };
    
    CGRect contentRect = [self boundingRectWithSize:boundingSize
                                            options:(NSStringDrawingUsesLineFragmentOrigin)
                                         attributes:attributes
                                            context:NULL];
    CGSize contentSize = CGSizeMake(ceilf(contentRect.size.width), ceilf(contentRect.size.height));
    
    return contentSize;
}

+ (NSString *)GetUUID
{
    CFUUIDRef theUUID = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, theUUID);
    CFRelease(theUUID);
    return (__bridge NSString *)string;
}

+ (NSString *)getNumEndingForNumber:(int)iNumber endingsArray:(NSArray *)aEndings {
    NSString *sEnding = nil;
    int i = 0;
    iNumber = iNumber % 100;
    if (iNumber >= 11 && iNumber <= 19) {
        sEnding = aEndings[2];
    } else {
        i = iNumber % 10;
        switch (i) {
            case 1:
                sEnding = aEndings[0];
                break;
            case 2:
            case 3:
            case 4:
                sEnding = aEndings[1];
                break;
            default:
                sEnding = aEndings[2];
                break;
        }
    }
    return sEnding;
}

@end

#pragma mark - NSObject+Utils

@implementation NSObject (Utils)

- (BOOL)isEmpty {
    return self == nil
    || ([self respondsToSelector:@selector(length)]
        && [(NSData *)self length] == 0)
    || ([self respondsToSelector:@selector(count)]
        && [(NSArray *)self count] == 0)
    || ([self respondsToSelector:@selector(isEqualToString:)]
        && [(NSString *)self isEqualToString:@""]);
}

@end

#pragma mark - UiTableView+Utils

@implementation UITableView (Utils)

- (void)deselectSelectedRow {
    if (self.indexPathForSelectedRow) {
        [self deselectRowAtIndexPath:self.indexPathForSelectedRow animated:YES];
    }
}

@end

@implementation UITextView (Utils)

- (CGSize) sizeWithText:(NSString*)text{
    CGRect frame = self.bounds;
    
    UIEdgeInsets textContainerInsets = self.textContainerInset;
    UIEdgeInsets contentInsets = self.contentInset;
    
    CGFloat leftRightPadding = textContainerInsets.left + textContainerInsets.right + self.textContainer.lineFragmentPadding * 2 + contentInsets.left + contentInsets.right;
    CGFloat topBottomPadding = textContainerInsets.top + textContainerInsets.bottom + contentInsets.top + contentInsets.bottom;
    
    frame.size.width -= leftRightPadding;
    frame.size.height -= topBottomPadding;
    
    if ([text hasSuffix:@"\n"])
    {
        text = [NSString stringWithFormat:@"%@-", text];
    }
    
    CGSize size = [text sizeForStringWithFont:self.font
                            constrainedToSize:CGSizeMake(CGRectGetWidth(frame), MAXFLOAT)];
    
    size.height += topBottomPadding;
    
    return size;
}

@end


@implementation UIButtonWithObject
@end
