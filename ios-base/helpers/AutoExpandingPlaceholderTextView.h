
#import <UIKit/UIKit.h>

@class AutoExpandingPlaceholderTextView;

@protocol AutoExpandingPlaceholderTextViewDelegate <NSObject>

- (void)textViewBeingEdited:(AutoExpandingPlaceholderTextView *)textView;
- (void)textViewDidEndEditing:(AutoExpandingPlaceholderTextView *)textView;

@end

@interface AutoExpandingPlaceholderTextView : UITextView

@property (nonatomic, strong) NSString *placeholder;
@property (nonatomic, retain) UIColor *placeholderColor;
@property (nonatomic, weak) IBOutlet id<AutoExpandingPlaceholderTextViewDelegate> expandingDelegate;

// YES by default
@property (nonatomic, assign) BOOL shouldExpand;
@end
