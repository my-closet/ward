
#import <UIKit/UIKit.h>

@interface UIRoundedImageView : UIImageView
@property (nonatomic, assign) CGFloat cornerRadius; // full circle by default
@end
