


#ifdef MR_ENABLE_ACTIVE_RECORD_LOGGING

#import "MagicalRecord.h"

typedef void (^MRSofteamSaveCompletionHandler)(NSArray *objectsArray, NSError *error);
typedef NSArray* (^MRSofteamArrayBlock)(NSManagedObjectContext *localContext);

@interface MagicalRecord (FSHelpers)
+ (void) saveWithArrayBlock:(MRSofteamArrayBlock)block completion:(MRSofteamSaveCompletionHandler)completion;
@end

#endif