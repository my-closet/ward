
#import "MagicalRecord+FSHelpers.h"

#ifdef MR_ENABLE_ACTIVE_RECORD_LOGGING

@implementation MagicalRecord (FSHelpers)
+ (void) saveWithArrayBlock:(NSArray*(^)(NSManagedObjectContext *localContext))block completion:(MRSofteamSaveCompletionHandler)completion;
{
    
    NSManagedObjectContext *mainContext  = [NSManagedObjectContext MR_rootSavingContext];
    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextWithParent:mainContext];
    
    NSMutableArray *objects = [NSMutableArray array];
    
    [localContext performBlock:^{
        
        if (block) {
            NSArray *array = block(localContext);
            [objects addObjectsFromArray:array];
        }
        
        if ([[localContext insertedObjects] count])
        {
            NSError *error = nil;
            BOOL success = [localContext obtainPermanentIDsForObjects:objects error:&error];
            if (!success)
            {
                [MagicalRecord handleErrors:error];
            }
        }
        
        [localContext MR_saveWithOptions:MRSaveParentContexts completion:^(BOOL success, NSError *error) {
            
            NSMutableArray *objectsArray = [NSMutableArray array];
            for (NSManagedObject *anObject in objects){
                
                NSManagedObject *object = (NSManagedObject*)[[NSManagedObjectContext MR_defaultContext] objectWithID:anObject.objectID];
                
                [objectsArray addObject:object];
            }
            
#ifdef SIMULATE_LONG_RESPONSE_TIME
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(SIMULATE_LONG_RESPONSE_TIME * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                completion(objectsArray, nil);
            });
            return;
#endif
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(objectsArray, nil);
            });
            
        }];
    }];
}
@end

#endif